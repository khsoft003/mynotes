



## ssm+maven单元测试



1. pom.xml导入测试包

```xml
<properties>
        <!-- Spring版本号 -->
        <spring.version>4.3.8.RELEASE</spring.version>
</properties>

<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-test</artifactId>
    <version>${spring.version}</version>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-tx</artifactId>
    <version>${spring.version}</version>
</dependency>

```



2. 创建测试类

```java
@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = { "classpath:applicationContext-service.xml" })
public class MyTest {
 
    @Autowired
    private UserService userService;
 
    @Test
    public void test01() {
        User user= this.userService.getUserById(1);
    	System.out.println("userName=" + user.getName());
    }
 
}
```





