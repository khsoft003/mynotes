## Java项目架构的发展历程



### 1 三层架构+MVC

​	代码分层，便于团队开发

### 2 Spring框架（IOC、AOP）

​	简化了三层架构中代码太多的问题

### 3 SpringBoot

​	简化了Spring框架配置太多的问题，特点：约定大于配置

### 4 SpringCloud 微服务架构

​	项目从All in One - -> 模块化

### 5 Server Mesh 服务网格：istio







## 微服务架构要解决的4个核心问题

1. 服务太多，客户端如何访问？
2. 服务太多，服务之间如何通信？
3. 服务太多，如何治理？
4. 服务异常结束，如何处理？





## 微服务架构解决4个核心问题

- API网关
- Http通信 / RPC调用
- 服务注册与发现
- 熔断机制（服务熔断与服务降级）





## 微服务架构解决方案

1. Spring Cloud NetFlix	一站式解决方案

   - API网关：zuul组件
   - Http通信：Feign
   - 服务注册与发现：Eureka
   - 熔断机制：Hystrix

   

2. Apache  Dubbo Zookeeper  半自动，需要整合其他

   - API网关：无

   - RPC调用：Dubbo

   - 服务注册与发现：Zookeeper

   - 熔断机制：借助Hystrix

     

3. Spring Cloud Alibaba   一站式解决方案





## 现代传统网站架构图



![image-20210412110159735](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412110159735.png)





## Dubbo 与 SpringCloud对比

![image-20210412110355193](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412110355193.png)