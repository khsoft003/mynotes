## 什么是 ECMAScript

ECMAScript 是由 Ecma 国际通过 ECMA-262 标准化的脚本程序设计语言；





## ES6 兼容性

查看网址：http://kangax.github.io/compat-table/es6





## ES6 新特性

### 1、let 关键字

1. 不允许重复声明；
2. 块儿级作用域（局部变量）；
3. 不存在变量提升；
4. 不影响作用域链；



### 2、const 关键字

声明常量；

1. 声明必须赋初始值；
2. 标识符一般为大写（习惯）；
3. 不允许重复声明；
4. 值不允许修改；
5. 块儿级作用域（局部变量）；



### 3、变量和对象的解构赋值

​	简化变量声明

```javascript
// ES6 允许按照一定模式，从数组和对象中提取值，对变量进行赋值，这被称为解构赋值；
// 1、数组的解构赋值
const F4 = ["大哥","二哥","三哥","四哥"];
let [a,b,c,d] = F4;
// 这就相当于我们声明4个变量a,b,c,d，其值分别对应"大哥","二哥","三哥","四哥"
console.log(a + b + c + d); // 大哥二哥三哥四哥

// 2、对象的解构赋值
const F3 = {
	name : "大哥",
	age : 22,
	sex : "男",
	xiaopin : function(){ // 常用
        console.log("我会演小品！");
	}
}
let {name,age,sex,xiaopin} = F3; // 注意解构对象这里用的是{}
console.log(name + age + sex + xiaopin); // 大哥22男
xiaopin(); // 此方法可以正常调用
```







### 4、模板字符串

声明自带格式的字符串；

```javascript
// 声明字符串的方法：单引号（''）、双引号（""）、反引号（``）
// 声明
let string = `我也一个字符串哦！`;
console.log(string);
// 特性
// 1、字符串中可以出现换行符
let str =`<ul>
            <li>大哥</li>
            <li>二哥</li>
            <li>三哥</li>
            <li>四哥</li>
        </ul>`;
console.log(str);

// 2、可以使用 ${xxx} 形式引用变量
let s = "大哥";
let out = `${s}是我最大的榜样！`;
console.log(out);
```





### 5、简化对象和函数写法

简化对象和函数写法；

```javascript
// ES6允许在对象的大括号内直接写入变量和函数作为对象的属性和方法
// 变量和函数
let name = "訾博";
let change = function(){
	console.log("活着就是为了改变世界！");
} 

//创建对象
const school = {
    // 完整写法
    // name:name,
    // change:change
    // 简化写法
    name,
    change,
    // 声明方法的简化
    say(){
    	console.log("言行一致！");
    }
} 
school.change();
school.say();
```







### 6、箭头函数

简化函数写法；

#### 箭头函数的注意点：

1. 如果形参只有一个，则小括号可以省略；
2. 函数体如果只有一条语句，则花括号可以省略，函数的返回值为该条语句的执行结果；
3. 箭头函数 this 指向声明时所在作用域下 this 的值；
4. 箭头函数不能作为构造函数实例化；
5. 不能使用 arguments；



```javascript
// ------ 无参数 ------
// 传统写法：
var say = function(){
	console.log("hello！");
} 
// ES写法：无参数
let say = () => console.log("hello！");



// ------ 一个参数 ------
// 传统写法：
var hello = function(name){
	return "hello " + name;
}
// ES6写法：
let hello = name => "hello " + name;


// ------ 多个参数 ------
// 传统写法：
var sum = function(a,b,c){
	return a + b + c;
} 
// ES6写法：
let he = (a,b,c) => a + b + c;

```







#### 特性：

1. 箭头函数的this是静态的，始终指向函数声明时所在作用域下的this的值；
2. 不能作为构造实例化对象；
3. 不能使用 arguments 变量；

```javascript
// 特性
// 1、箭头函数的this是静态的，始终指向函数声明时所在作用域下的this的值

window.name = "訾博";

// 传统函数
function getName(){
	console.log("getName：" + this.name);
} 
// 箭头函数
getName1 = () => console.log("getName1：" + this.name);

// 直接调用
getName(); //输出："訾博"
getName1();//输出："訾博"
// ---------------------------------------

const school = {
	name : "北京大学",
} 

// 使用call调用
getName.call(school); //输出："北京大学"
getName1.call(school);//输出："訾博"
// 结论：箭头函数的this是静态的，始终指向函数声明时所在作用域下的this的值
```



```javascript
// 2、不能作为构造实例化对象
// let Persion = (name,age) => {
// this.name = name;
// this.age = age;
// }
// let me = new Persion("訾博",24);
// console.log(me);
// 报错：Uncaught TypeError: Persion is not a constructor
```



```javascript
// 3、不能使用 arguments 变量
// let fn = () => console.log(arguments);
// fn(1,2,3);
// 报错：Uncaught ReferenceError: arguments is not defined
```





### 7、ES6中函数参数的默认值

给函数的参数设置默认值；

```javascript
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>
			函数参数默认值
		</title>
	</head>
	<body>
		<script>
			//ES6 允许给函数参数赋值初始值
			//1. 形参初始值 具有默认值的参数, 一般位置要靠后(潜规则)
			function add(a,b,c=10) {
				return a + b + c;
			}
			let result = add(1,2);
			console.log(result); // 13
			//2. 与解构赋值结合
			// 注意这里参数是一个对象
			function connect({host="127.0.0.1", username,password, port}){
				console.log(host);
				console.log(username);
				console.log(password);
				console.log(port);
			}
			connect({
                host: 'atguigu.com',
                username: 'root',
                password: 'root',
                port: 3306
            })
		</script>
	</body>

</html>
```





### 8、rest参数

拿到实参；ES6 引入 rest 参数，用于获取函数的实参，用来代替 arguments；

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>rest参数</title>
</head>
<body>
<script>
    // ES6 引入 rest 参数，用于获取函数的实参，用来代替 arguments；
    // ES5获取实参的方式
    function data(){
    	console.log(arguments);
    }
    data("大哥","二哥","三哥","四哥");
    // ES6的rest参数...args，rest参数必须放在最后面
    function data(...args){
    	console.log(args); // fliter some every map
    }
    data("大哥","二哥","三哥","四哥");

</script>
</body>
</html>
```





### 9、扩展运算符

将一个数组转为用逗号分隔的参数序列；

... 扩展运算符能将数组转换为逗号分隔的参数序列；

```javascript
// ... 扩展运算符能将数组转换为逗号分隔的参数序列
//声明一个数组 ...
const tfboys = ['易烊千玺', '王源', '王俊凯'];
// => '易烊千玺','王源','王俊凯'
// 声明一个函数
function chunwan() {
	console.log(arguments);
}
chunwan(...tfboys); // chunwan('易烊千玺','王源','王俊凯')

```





### 10、Symbol

表示独一无二的值；它是JavaScript 语言的第七种数据类 型，是一种类似于字符串的数据类型；

#### Symbol 特点： 

1. Symbol 的值是唯一的，用来解决命名冲突的问题；
2. Symbol 值不能与其他数据进行运算； 
3. Symbol 定义的对象属性不能使用for…in循环遍历 ，但是可以使用Reflect.ownKeys 来获取对象的 所有键名；



#### 基本使用：

```javascript
//创建Symbol
let s = Symbol();
// console.log(s, typeof s);
let s2 = Symbol('尚硅谷');
let s3 = Symbol('尚硅谷');
console.log(s2==s3); // false
//Symbol.for 创建
let s4 = Symbol.for('尚硅谷');
let s5 = Symbol.for('尚硅谷');
console.log(s4==s5); // true
//不能与其他数据进行运算
// let result = s + 100;
// let result = s > 100;
// let result = s + s;
// USONB you are so niubility
// u undefined
// s string symbol
// o object
// n null number
// b boolean

```



#### Symbol创建对象属性：  

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Symbol创建对象属性</title>
</head>

<body>
    <script>
        // 向对象中添加方法 up down
        let game = {
            name: '俄罗斯方块',
            up: function () { },
            down: function () { }
        };

        // 我们要往game对象里面添加方法，但是怕game对象已经存在
        // 同名方法，所以我们这时使用到了Symbol
        // 方式一
        // 声明一个对象
        let methods = {
            up: Symbol(),
            down: Symbol()
        };
        game[methods.up] = function () {
            console.log("我可以改变形状");
        }
        game[methods.down] = function () {
            console.log("我可以快速下降!!");
        }
        console.log(game);
        // 方式二
        let youxi = {
            name: "狼人杀",
            [Symbol('say')]: function () {
                console.log("我可以发言")
            },
            [Symbol('zibao')]: function () {
                console.log('我可以自爆');
            }
        }
        console.log(youxi);
        // 如何调用方法？？？ 讲师没讲，这是弹幕说的方法
        let say = Symbol('say');
        let youxi1 = {
            name: "狼人杀",
            [say]: function () {
                console.log("我可以发言")
            },
            [Symbol('zibao')]: function () {
                console.log('我可以自爆');
            }
        }
        youxi1[say]();
    </script>
</body>

</html>
```





#### Symbol内置值：

概述：
除了定义自己使用的 Symbol 值以外，ES6 还提供了 11 个内置的 Symbol 值，指向语言内部使用的方
法。可以称这些方法为魔术方法，因为它们会在特定的场景下自动执行；  

（待续。。。）



### 11、迭代器

用来遍历集合、数组等；

#### 概述：

遍历器（Iterator）就是一种机制。它是一种接口，为各种不同的数据结构提供统一的访问机制。任何数
据结构只要部署 Iterator 接口，就可以完成遍历操作；



#### 特性：

ES6 创造了一种新的遍历命令 for...of 循环，Iterator 接口主要供 for...of 消费；
原生具备 iterator 接口的数据(可用 for of 遍历)：

- Array；
- Arguments；
- Set；
- Map；
- String；
- TypedArray；
- NodeList；

#### 工作原理：

1. 创建一个指针对象，指向当前数据结构的起始位置；
2. 第一次调用对象的 next 方法，指针自动指向数据结构的第一个成员；
3. 接下来不断调用 next 方法，指针一直往后移动，直到指向最后一个成员；
4. 每调用 next 方法返回一个包含 value 和 done 属性的对象；



**注：需要自定义遍历数据的时候，要想到迭代器；**  



#### 迭代器自定义遍历对象：  

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>迭代器自定义遍历数据</title>
</head>

<body>
    <script>
        // 声明一个对象
        const banji = {
            name: "终极一班",
            stus: [
                'xiaoming',
                'xiaoning',
                'xiaotian',
                'knight'
            ],
            [Symbol.iterator]() {
                // 索引变量
                let index = 0;
                // 保存this
                let _this = this;
                return {
                    next: function () {
                        if (index < _this.stus.length) {
                            const result = {
                                value: _this.stus[index],
                                done: false
                            };
                            // 下标自增
                            index++;
                            // 返回结果
                            return result;
                        } else {
                            return {
                                value: undefined,
                                done: true
                            };
                        }
                    }
                };
            }
        }
        // 遍历这个对象
        for (let v of banji) {
            console.log(v);
        }
    </script>
</body>

</html>
```

###### 运行结果：

![image-20220221102836216](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220221102836216.png)



### 12、生成器

#### 概述：

生成器函数是 ES6 提供的一种异步编程解决方案，语法行为与传统函数完全不同；  



#### 基本使用：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>生成器</title>
</head>
<body>
    <script>
        // 生成器其实就是一个特殊的函数
        // 异步编程 纯回调函数 node fs ajax mongodb
        // yield：函数代码的分隔符
        function* gen() {
            console.log(111);
            yield '一只没有耳朵';
            console.log(222);
            yield '一只没有尾部';
            console.log(333);
            yield '真奇怪';
            console.log(444);
        }
        let iterator = gen();
        console.log(iterator.next());
        console.log(iterator.next());
        console.log(iterator.next());
        console.log(iterator.next());
        console.log("遍历：");
        //遍历
        for (let v of gen()) {
            console.log(v);
        }
    </script>
</body>
</html>
```



###### 运行结果：

![image-20220221103428721](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220221103428721.png)



#### 生成器函数的参数传递:  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>生成器函数的参数传递</title>
</head>
<body>
    <script>
        function* gen(arg) {
            console.log(arg);
            let one = yield 111;
            console.log(one);
            let two = yield 222;
            console.log(two);
            let three = yield 333;
            console.log(three);
        };
        let iterator = gen("AAA");
        console.log(iterator.next()); // 会执行yield 111;
        // next()方法是可以传入参数的，传入的参数作为第一条(上一条)语句yield 111的返回结果
        console.log(iterator.next("BBB")); // 会执行yield 222;
        console.log(iterator.next("CCC")); // 会执行yield 333;
        console.log(iterator.next("DDD")); // 继续往后走，未定义;
    </script>
</body>
</html>
```





###### 运行结果：

![image-20220221103843034](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220221103843034.png)



#### 生成器函数实例1：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>生成器函数实例1</title>
</head>
<body>
    <script>
        // 异步编程 文件操作 网络操作（ajax，request） 数据库操作
        // 需求：1s后控制台输出111 再过2s后控制台输出222 再过3s后控制台输出333
        // 一种做法：回调地狱
        // setTimeout(() => {
        //     console.log(111);
        //     setTimeout(() => {
        //         console.log(222);
        //         setTimeout(() => {
        //             console.log(333);
        //         }, 3000)
        //     }, 2000)
        // }, 1000)
        // 另一种做法
        function one() {
            setTimeout(() => {
                console.log(111);
                iterator.next();
            }, 1000)
        }
        function two() {
            setTimeout(() => {
                console.log(222);
                iterator.next();
            }, 1000)
        }
        function three() {
            setTimeout(() => {
                console.log(333);
                iterator.next();
            }, 1000)
        }
        function* gen() {
            yield one();
            yield two();
            yield three();
        }
        // 调用生成器函数
        let iterator = gen();
        iterator.next();
    </script>
</body>
</html>
```

###### 运行结果：

![image-20220221104331686](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220221104331686.png)



#### 生成器函数实例2：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>生成器函数实例2</title>
</head>
<body>
    <script>
        // 模拟获取: 用户数据 订单数据 商品数据
        function getUsers() {
            setTimeout(() => {
                let data = "用户数据";
                // 第二次调用next，传入参数，作为第一个的返回值
                iterator.next(data); // 这里将data传入
            }, 1000);
        } 
        function getOrders() {
            setTimeout(() => {
                let data = "订单数据";
                iterator.next(data); // 这里将data传入
            }, 1000);
        } 
        function getGoods() {
            setTimeout(() => {
                let data = "商品数据";
                iterator.next(data); // 这里将data传入
            }, 1000);
        } 
        function* gen() {
            let users = yield getUsers();
            console.log(users);
            let orders = yield getOrders();
            console.log(orders);
            let goods = yield getGoods();
            console.log(goods); // 这种操作有点秀啊！
        } 
        let iterator = gen();
        iterator.next();
    </script>
</body>
</html>
```



###### 运行结果：

![image-20220221104954114](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220221104954114.png)



### 13、Promise

#### 概述：

Promise 是 ES6 引入的**异步编程的新解决方案**。语法上 Promise 是**一个构造函数**，用来封装异步操作
并可以获取其成功或失败的结果；

1. Promise 构造函数: Promise (excutor) {}；
2. Promise.prototype.then 方法；
3. Promise.prototype.catch 方法；  



#### 基本使用：

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Promise</title>
</head>

<body>
    <script>
        // 实例化 Promise 对象
        // Promise 对象三种状态：初始化、成功、失败
        const p = new Promise(function (resolve, reject) {
            setTimeout(function () {
                // 成功，调用resolve，这个Promise 对象的状态就会变成成功
                let data = "数据"; 
                resolve(data);

                // 失败
                // let err = "失败了！";
                // reject(err);
            }, 1000);
        });
        // 成功
        // 调用 Promise 对象的then方法，两个参数为函数
        p.then(function (value) { // 成功
            console.log(value);
        }, function (season) { // 失败
            console.log(season);
        });
    </script>
</body>

</html>
```



#### Promise封装读取文件：  

###### 一般写法：  

```javascript
// 1、引入 fs 模块
const fs = require("fs");
// 2、调用方法，读取文件
fs.readFile("resources/text.txt", (err, data) => {
    // 如果失败则抛出错误
    if (err) throw err;
    // 如果没有出错，则输出内容
    console.log(data.toString());
});
```

###### Promise封装：  

```javascript
// 1、引入 fs 模块
const fs = require("fs");
// 2、调用方法，读取文件
const p = new Promise(function (resolve, data) {
    fs.readFile("resources/text.txt", (err, data) => {
        // 判断如果失败
        if (err) reject(err);
        // 如果成功
        resolve(data);
    });
});
p.then(function (value) {
    console.log(value.toString());
}, function (reason) {
    console.log(reason); // 读取失败
})
```

###### 运行结果：

![image-20220221111701740](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220221111701740.png)





#### Promise封装Ajax请求：

###### 原生请求：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Promise封装Ajax请求</title>
</head>
<body>
    <script>
        // 请求地址：https://api.apiopen.top/getJoke
        // 原生请求
        // 1、创建对象
        const xhr = new XMLHttpRequest();
        // 2、初始化
        xhr.open("GET", "https://api.apiopen.top/getJoke");
        // 3、发送
        xhr.send();
        // 4、绑定事件，处理响应结果
        xhr.onreadystatechange = function () {
            // 判断状态
            if (xhr.readyState == 4) {
                // 判断响应状态码 200-299
                if (xhr.status >= 200 && xhr.status <= 299) {
                    // 成功
                    console.log(xhr.response);
                } else {
                    // 失败
                    console.error(xhr.status);
                }
            }
        }
    </script>
</body>
</html>
```



#### Promise封装Ajax请求：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Promise封装Ajax请求</title>
</head>
<body>
    <script>
        // 请求地址：https://api.apiopen.top/getJoke
        const p = new Promise(function (resolve, reason) {
            // 原生请求
            // 1、创建对象
            const xhr = new XMLHttpRequest();
            // 2、初始化
            xhr.open("GET", "https://api.apiopen.top/getJoke");
            // 3、发送
            xhr.send();
            // 4、绑定事件，处理响应结果
            xhr.onreadystatechange = function () {
                // 判断状态
                if (xhr.readyState == 4) {
                    // 判断响应状态码 200-299
                    if (xhr.status >= 200 && xhr.status <= 299) {
                        // 成功
                        resolve(xhr.response);
                    } else {
                        // 失败
                        reason(xhr.status);
                    }
                }
            }
        });
        p.then(function (value) {
            console.log(value.toString());
        }, function (reason) {
            console.log(reason); // 读取失败
        })
    </script>
</body>
</html>
```



#### Promise.prototype.then：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Promise.prototype.then</title>
</head>
<body>
    <script>
        // 创建 Promise 对象
        const p = new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve("用户数据");
            }, 1000);
        });
        // 调用then方法，then方法的返回结果是promise对象，
        // 对象的状态有回调函数的结果决定;
        const result = p.then(value => {
            console.log(value);
            // 1、如果回调函数中返回的结果是 非promise 类型的数据，
            // 状态为成功，返回值为对象的成功值resolved
            // [[PromiseStatus]]:"resolved"
            // [[PromiseValue]]:123
            // return 123;
            // 2、如果...是promise类型的数据
            // 此Promise对象的状态决定上面Promise对象p的状态
            // return new Promise((resolve,reject)=>{
            // // resolve("ok"); // resolved
            // reject("ok"); // rejected
            // });
            // 3、抛出错误
            // throw new Error("失败啦！");
            // 状态：rejected
            // value：失败啦！
        }, reason => {
            console.error(reason);
        })
        // 链式调用
        // then里面两个函数参数，可以只写一个
        p.then(value=>{},reason=>{}).then(value=>{},reason=>{});
        console.log(result);
    </script>
</body>
</html>
```



#### Promise解决回调地狱问题

###### “回调地狱”方式写法：  

```js
// 1、引入 fs 模块
const fs = require("fs");
// 2、调用方法，读取文件
fs.readFile("resources/text.txt", (err, data1) => {
    fs.readFile("resources/test1.txt", (err, data2) => {
        fs.readFile("resources/test2.txt", (err, data3) => {
            let result = data1 + data2 + data3;
            console.log(result);
        });
    });
})
```

###### Promise实现：  

```javascript
// 1、引入 fs 模块
const fs = require("fs");
// 2、使用Promise实现
const p = new Promise((resolve, reject) => {
    fs.readFile("resources/text.txt", (err, data) => {
        resolve(data);
    });
});
p.then(value => {
    return new Promise((resolve, reject) => {
        fs.readFile("resources/text1.txt", (err, data) => {
            resolve([value, data]);
        });
    })
}).then(value => {
    return new Promise((resolve, reject) => {
        fs.readFile("resources/text2.txt", (err, data) => {
            // 存入数组
            value.push(data);
            resolve(value);
        });
    })
}).then(value => {
    console.log(value.join("\r\n"));
})
```





#### Promise对象catch方法：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Promise对象catch方法</title>
</head>
<body>
    <script>
        // Promise对象catch方法
        const p = new Promise((resolve, reject) => {
            setTimeout(() => {
                // 设置p对象的状态为失败，并设置失败的值
                reject("失败啦~！");
            }, 1000);
        })
        // p.then(value=>{
        // console.log(value);
        // },reason=>{
        // console.warn(reason);
        // });
        p.catch(reason => {
            console.warn(reason);
        });
    </script>
</body>
</html>
```



### 14、Set集合

#### 概述：

ES6 提供了新的数据结构 Set（集合）。它类似于数组，但成员的值都是唯一的，集合实现了 iterator
接口，所以可以使用『扩展运算符』和『for…of…』进行遍历，集合的属性和方法：

1. size 返回集合的元素个数；
2. add 增加一个新元素，返回当前集合；
3. delete 删除元素，返回 boolean 值；
4. has 检测集合中是否包含某个元素，返回 boolean 值；
5. lear 清空集合，返回 undefined；  



#### 基本使用：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Set集合</title>
</head>
<body>
    <script>
        // Set集合
        let s = new Set();
        console.log(s, typeof s);
        let s1 = new Set(["大哥", "二哥", "三哥", "四哥", "三哥"]);
        console.log(s1); // 自动去重
        // 1. size 返回集合的元素个数；
        console.log(s1.size);
        // 2. add 增加一个新元素，返回当前集合；
        s1.add("大姐");
        console.log(s1);
        // 3. delete 删除元素，返回 boolean 值；
        let result = s1.delete("三哥");
        console.log(result);
        console.log(s1);
        // 4. has 检测集合中是否包含某个元素，返回 boolean 值；
        let r1 = s1.has("二姐");
        console.log(r1);
        // 5. clear 清空集合，返回 undefined；
        // s1.clear();
        // console.log(s1);
    </script>
</body>
</html>
```

![image-20220222134040161](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222134040161.png)

#### Set集合实践：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Set集合实践</title>
</head>
<body>
    <script>
        // Set集合实践
        let arr = [1, 2, 3, 4, 5, 4, 3, 2, 1];
        // 数组去重
        let s1 = new Set(arr);
        console.log(s1);
        // 交集
        let arr2 = [3, 4, 5, 6, 5, 4, 3];
        // 看来我需要学学数组的一些方法
        let result = [...new Set(arr)].filter(item => new
            Set(arr2).has(item));
        console.log(result);
        // 并集
        // ... 为扩展运算符，将数组转化为逗号分隔的序列
        let union = [...new Set([...arr, ...arr2])];
        console.log(union);
        // 差集：比如集合1和集合2求差集，就是1里面有的，2里面没的
        let result1 = [...new Set(arr)].filter(item => !(new
            Set(arr2).has(item)));
        console.log(result1);
    </script>
</body>
</html>
```

![image-20220222134319053](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222134319053.png)





### 15、Map集合

#### 概述：

ES6 提供了 Map 数据结构。它类似于对象，也是键值对的集合。但是“键”的范围不限于字符串，各种类
型的值（包括对象）都可以当作键。Map 也实现了iterator 接口，所以可以使用『扩展运算符』和
『for…of…』进行遍历；  



#### Map 的属性和方法：  

1. size 返回 Map 的元素个数；
2. set 增加一个新元素，返回当前 Map；
3. get 返回键名对象的键值；
4. has 检测 Map 中是否包含某个元素，返回 boolean 值；
5. clear 清空集合，返回 undefined；  



#### 简单使用：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Map集合</title>
</head>
<body>
    <script>
        // Map集合
        // 创建一个空 map
        let m = new Map();
        // 创建一个非空 map
        let m2 = new Map([
            ['name', '尚硅谷'],
            ['slogon', '不断提高行业标准']
        ]);
        // 1. size 返回 Map 的元素个数；
        console.log(m2.size);
        // 2. set 增加一个新元素，返回当前 Map；
        m.set("皇帝", "大哥");
        m.set("丞相", "二哥");
        console.log(m);
        // 3. get 返回键名对象的键值；
        console.log(m.get("皇帝"));
        // 4. has 检测 Map 中是否包含某个元素，返回 boolean 值；
        console.log(m.has("皇帝"));
        // 5. clear 清空集合，返回 undefined；
        // m.clear();
        // console.log(m);
    </script>
</body>
</html>
```



![image-20220222134747927](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222134747927.png)





### 16、class类

#### 概述：

ES6 提供了更接近传统语言的写法，引入了 Class（类）这个概念，作为对象的模板。通过 class 关键
字，可以定义类。基本上，ES6 的 class 可以看作只是一个语法糖，它的绝大部分功能，ES5 都可以做
到，新的 class 写法只是让对象原型的写法更加清晰、更像面向对象编程的语法而已；



#### 知识点：

1. class 声明类；

2. constructor 定义构造函数初始化；

3. extends 继承父类；

4. super 调用父级构造方法；

5. static 定义静态方法和属性；

6. 父类方法可以重写；

   

#### class初体验：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>class类</title>
</head>
<body>
    <script>
        // 手机 ES5写法
        // function Phone(brand,price){
        // this.brand = brand;
        // this.price = price;
        // }
        // // 添加方法
        // Phone.prototype.call = function(){
        // console.log("我可以打电话！");
        // }
        // // 实例化对象
        // let HuaWei = new Phone("华为",5999);
        // HuaWei.call();
        // console.log(HuaWei);
        // ES6写法
        class Phone {
            // 构造方法，名字是固定的
            constructor(brand, price) {
                this.brand = brand;
                this.price = price;
            }
            // 打电话，方法必须使用该方式写
            call() {
                console.log("我可以打电话！");
            }
        }
        let HuaWei = new Phone("华为", 5999);
        HuaWei.call();
        console.log(HuaWei);
    </script>
</body>
</html>
```

![image-20220222135353378](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222135353378.png)





#### class静态成员：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>class静态成员</title>
</head>
<body>
    <script>
        // class静态成员
        // ES5写法
        // function Phone() { }
        // Phone.name = "手机";
        // Phone.change = function () {
        //     console.log("我可以改变世界！");
        // }
        // let nokia = new Phone();
        // console.log(nokia.name); // undefined
        // // // nokia.change();
        // // // 报错：Uncaught TypeError: nokia.change is not a function
        // Phone.prototype.color = "黑色";
        // console.log(nokia.color); // 黑色
        // console.log(Phone.name);
        // Phone.change();
        // // 注意：实例对象和函数对象的属性是不相通的

        
        // ES6写法
        class Phone {
            // 静态属性
            static name = "手机";
            static change() {
                console.log("我可以改变世界！");
            }
        }
        let nokia = new Phone();
        console.log(nokia.name);
        console.log(Phone.name);
        Phone.change();
    </script>
</body>
</html>
```





![image-20220222140338622](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222140338622.png)





#### ES5构造函数实现继承：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ES5构造函数继承</title>
</head>
<body>
    <script>
        // ES5构造函数继承
        // 手机
        function Phone(brand, price) {
            this.brand = brand;
            this.price = price;
        }
        Phone.prototype.call = function () {
            console.log("我可以打电话！");
        }
        // 智能手机
        function SmartPhone(brand, price, color, size) {
            Phone.call(this, brand, price);
            this.color = color;
            this.size = size;
        }
        // 设置子级构造函数的原型
        SmartPhone.prototype = new Phone;
        SmartPhone.prototype.constructor = SmartPhone;
        // 声明子类的方法
        SmartPhone.prototype.photo = function () {
            console.log("我可以拍照！");
        }
        SmartPhone.prototype.game = function () {
            console.log("我可以玩游戏！");
        }
        const chuizi = new SmartPhone("锤子", 2499, "黑色", "5.5inch");
        console.log(chuizi);
        chuizi.call();
        chuizi.photo();
        chuizi.game();
    </script>
</body>
</html>
```

![image-20220222141714166](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222141714166.png)



#### ES6class类继承：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ES6class类继承</title>
</head>
<body>
    <script>
        // ES6class类继承
        class Phone {
            constructor(brand, price) {
                this.brand = brand;
                this.price = price;
            } 
            call() {
                console.log("我可以打电话！");
            }
        } 
        
        class SmartPhone extends Phone {
            // 构造函数
            constructor(brand, price, color, size) {
                super(brand, price); // 调用父类构造函数
                this.color = color;
                this.size = size;
            } 
            photo() {
                console.log("我可以拍照！");
            } 
            game() {
                console.log("我可以玩游戏！");
            }
        } 
        const chuizi = new SmartPhone("小米", 1999, "黑色", "5.15inch");
        console.log(chuizi);
        chuizi.call();
        chuizi.photo();
        chuizi.game();
    </script>
</body>
</html>
```

![image-20220222141958912](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222141958912.png)



#### 子类对父类方法重写：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ES6class类继承</title>
</head>
<body>
    <script>
        // ES6class类继承
        class Phone {
            constructor(brand, price) {
                this.brand = brand;
                this.price = price;
            } 
            call() {
                console.log("我可以打电话！");
            }
        } 
        
        class SmartPhone extends Phone {
            // 构造函数
            constructor(brand, price, color, size) {
                super(brand, price); // 调用父类构造函数
                this.color = color;
                this.size = size;
            } 
            // 子类对父类方法重写
            // 直接写，直接覆盖
            // 注意：子类无法调用父类同名方法
            call() {
                console.log("我可以进行视频通话！");
            } 
            photo() {
                console.log("我可以拍照！");
            } 
            game() {
                console.log("我可以玩游戏！");
            }
        } 
        const chuizi = new SmartPhone("小米", 1999, "黑色", "5.15inch");
        console.log(chuizi);
        chuizi.call();
        chuizi.photo();
        chuizi.game();
    </script>
</body>
</html>
```

![image-20220222142353256](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222142353256.png)





#### class中的getter和setter设置：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>class中的getter和setter设置</title>
</head>
<body>
    <script>
        // class中的getter和setter设置
        class Phone {
            get price() {
                console.log("价格属性被读取了！");
                return 123;// 返回值
            }
            set price(value) {
                console.log("价格属性被修改为："+value);
            }
        }
        // 实例化对象
        let s = new Phone();
        console.log(s.price); // 返回值
        s.price = 2999;
    </script>
</body>
</html>
```

![image-20220222142638669](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222142638669.png)





### 17、数值扩展

增加一些数值相关的方法等；

#### Number.EPSILON：

Number.EPSILON 是 JavaScript 表示的最小精度；
EPSILON 属性的值接近于 2.2204460492503130808472633361816E-16；

#### 二进制和八进制：

ES6 提供了二进制和八进制数值的新的写法，分别用前缀 0b 和 0o 表示；

#### Number.isFinite() 与 Number.isNaN() ：

Number.isFinite() 用来检查一个数值是否为有限的；
Number.isNaN() 用来检查一个值是否为 NaN；

#### Number.parseInt() 与 Number.parseFloat()：

ES6 将全局方法 parseInt 和 parseFloat，移植到 Number 对象上面，使用不变；

#### Math.trunc：

用于去除一个数的小数部分，返回整数部分；

#### Number.isInteger：

Number.isInteger() 用来判断一个数值是否为整数；  



```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>数值扩展</title>
</head>
<body>
    <script>
        // 数值扩展
        // 0. Number.EPSILON 是 JavaScript 表示的最小精度
        // EPSILON 属性的值接近于 2.2204460492503130808472633361816E-16
        // function equal(a, b){
        // return Math.abs(a-b) < Number.EPSILON;
        // }
        console.log("0、Number.EPSILON 是 JavaScript 表示的最小精度");
        // 箭头函数简化写法
        equal = (a, b) => Math.abs(a - b) < Number.EPSILON;
        console.log(0.1 + 0.2);
        console.log(0.1 + 0.2 === 0.3); // false
        console.log(equal(0.1 + 0.2, 0.3)); // true
        // 1. 二进制和八进制
        console.log("1、二进制和八进制");
        let b = 0b1010;
        let o = 0o777;
        let d = 100;
        let x = 0xff;
        console.log(x);
        // 2. Number.isFinite 检测一个数值是否为有限数
        console.log("2、Number.isFinite 检测一个数值是否为有限数");
        console.log(Number.isFinite(100));
        console.log(Number.isFinite(100 / 0));
        console.log(Number.isFinite(Infinity));
        // 3. Number.isNaN 检测一个数值是否为 NaN
        console.log("3. Number.isNaN 检测一个数值是否为 NaN");
        console.log(Number.isNaN(123));
        // 4. Number.parseInt Number.parseFloat字符串转整数
        console.log("4. Number.parseInt Number.parseFloat字符串转整数");
        console.log(Number.parseInt('5211314love'));
        console.log(Number.parseFloat('3.1415926神奇'));
        // 5. Number.isInteger 判断一个数是否为整数
        console.log("5. Number.isInteger 判断一个数是否为整数");
        console.log(Number.isInteger(5));
        console.log(Number.isInteger(2.5));
        // 6. Math.trunc 将数字的小数部分抹掉
        console.log("6. Math.trunc 将数字的小数部分抹掉 ");
        console.log(Math.trunc(3.5));
        // 7. Math.sign 判断一个数到底为正数 负数 还是零
        console.log("7. Math.sign 判断一个数到底为正数 负数 还是零");
        console.log(Math.sign(100));
        console.log(Math.sign(0));
        console.log(Math.sign(-20000));
    </script>
</body>
</html>
```

![image-20220222143223105](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222143223105.png)





### 18、对象扩展

#### 概述：

ES6 新增了一些 Object 对象的方法：

1. Object.is 比较两个值是否严格相等，与『===』行为基本一致（+0 与 NaN）；
2. Object.assign 对象的合并，将源对象的所有可枚举属性，复制到目标对象；
3. proto、setPrototypeOf、 setPrototypeOf 可以直接设置对象的原型；  



#### 代码实现及相关说明：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>对象扩展</title>
</head>
<body>
    <script>
        // 对象扩展
        // 1. Object.is 比较两个值是否严格相等，与『===』行为基本一致（+0 与 NaN）；
        console.log(Object.is(120, 120)); // ===
        // 注意下面的区别
        console.log(Object.is(NaN, NaN));
        console.log(NaN === NaN);
        // NaN与任何数值做===比较都是false，跟他自己也如此！

        // 2. Object.assign 对象的合并，将源对象的所有可枚举属性，复制到目标对象；
        const config1 = {
            host: "localhost",
            port: 3306,
            name: "root",
            pass: "root",
            test: "test" // 唯一存在
        } 
        const config2 = {
            host: "http://zibo.com",
            port: 300300600,
            name: "root4444",
            pass: "root4444",
            test2: "test2"
        } 
        // 如果前边有后边没有会添加，如果前后都有，后面的会覆盖前面的
        console.log(Object.assign(config1, config2));

        // 3. __proto__、setPrototypeOf、 getPrototypeOf 可以直接设置对象的原型；
        const school = {
            name: "尚硅谷"
        } 
        const cities = {
            xiaoqu: ['北京', '上海', '深圳']
        } 
        // 并不建议这么做
        Object.setPrototypeOf(school, cities);
        console.log(Object.getPrototypeOf(school));
        console.log(school);
    </script>
</body>
</html>
```

![image-20220222143824129](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222143824129.png)







### 19、模块化

#### 概述：

模块化是指将一个大的程序文件，拆分成许多小的文件，然后将小文件组合起来；



#### 模块化的好处：

1. 防止命名冲突；
2. 代码复用；
3. 高维护性；



#### 模块化规范产品：

ES6 之前的模块化规范有：

1. CommonJS => NodeJS、Browserify；
2. AMD => requireJS；
3. CMD => seaJS；



#### ES6 模块化语法：

模块功能主要由两个命令构成：export 和 import；

- export 命令用于规定模块的对外接口（导出模块）；
- import 命令用于输入其他模块提供的功能（导入模块）；  





#### 简单使用：  

###### m.js（导出模块）：  

```javascript
export let school = "尚硅谷";
export function teach(){
	console.log("我们可以教你开发技术！");
}
```



###### 模块化.html（导入和使用模块）：  

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>模块化</title>
</head>
<body>
	<script type="module">
		// 引入m.js模块内容
        import * as m from "./js/m.js";
        console.log(m);
        console.log(m.school);
        m.teach();
	</script>
</body>
</html>
```

![image-20220222144340918](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222144340918.png)





#### ES6暴露数据语法汇总：  

###### m.js（逐个导出模块）：  

```javascript
// 分别暴露（导出）
export let school = "尚硅谷";
export function teach(){
	console.log("我们可以教你开发技术！");
}
```



###### n.js（统一导出模块）：  

```javascript
// 统一暴露（导出）
let school = "尚硅谷";
function findJob(){
	console.log("我们可以帮你找到好工作！");
} 
export {school,findJob}
```



###### o.js（默认导出模块）：  

```javascript
// 默认暴露（导出）
export default{
    school : "尚硅谷",
    change : function(){
    	console.log("我们可以帮你改变人生！");
    }
}
```



###### 模块化.html（引入和使用模块）：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>模块化</title>
</head>
<body>
    <script type="module">
        // 引入m.js模块内容
        import * as m from "./js/m.js";
        console.log(m);
        console.log(m.school);
        m.teach();
        // 引入n.js模块内容
        import * as n from "./js/n.js";
        console.log(n);
        console.log(n.school);
        n.findJob();
        // 引入o.js模块内容
        import * as o from "./js/o.js";
        console.log(o);
        // 注意这里调用方法的时候需要加上default
        console.log(o.default.school);
        o.default.change();
    </script>
</body>
</html>
```

![image-20220222144706311](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222144706311.png)





#### ES6导入模块语法汇总：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>模块化</title>
</head>
<body>
    <script type="module">
        // 通用方式
        // 引入m.js模块内容
        import * as m from "./js/m.js";
        console.log(m);
        console.log(m.school);
        m.teach();
        // 引入n.js模块内容
        import * as n from "./js/n.js";
        console.log(n);
        console.log(n.school);
        n.findJob();
        // 引入o.js模块内容
        import * as o from "./js/o.js";
        console.log(o);
        // 注意这里调用方法的时候需要加上default
        console.log(o.default.school);
        o.default.change();
        // 解构赋值形式
        import {school,teach} from "./js/m.js";
        // 重名的可以使用别名
        import {school as xuexiao,findJob} from "./js/n.js";
        // 导入默认导出的模块，必须使用别名
        import {default as one} from "./js/o.js";
        // 直接可以使用
        console.log(school);
        teach();
        console.log(xuexiao);
        console.log(one);
        console.log(one.school);
        one.change();
        // 简便形式，只支持默认导出
        import oh from "./js/o.js";
        console.log(oh);
        console.log(oh.school);
        oh.change();
    </script>
</body>
</html>
```

![image-20220222144949131](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222144949131.png)





#### 使用模块化的另一种方式：  

###### 将js语法整合到一个文件app.js：  

```javascript
// 通用方式
// 引入m.js模块内容
import * as m from "./m.js";
console.log(m);
console.log(m.school);
m.teach();
```

###### 使用模块化的另一种方式.html：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>使用模块化的另一种方式</title>
</head>
<body>
	<script src="./js/app.js" type="module"></script>
</body>
</html>
```

![image-20220222145140535](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222145140535.png)









### 20、Babel对ES6模块化代码转换

#### Babel概述：

- Babel 是一个 JavaScript 编译器；
- Babel 能够将新的ES规范语法转换成ES5的语法；
- 因为不是所有的浏览器都支持最新的ES规范，所以，一般项目中都需要使用Babel进行转换；
- 步骤：使用Babel转换JS代码——打包成一个文件——使用时引入即可；  





#### 步骤：

第一步：安装工具babel-cli（命令行工具） babel-preset-env（ES转换工具） browserify（打包工具，
项目中使用的是webpack）；

第二步：初始化项目  

```shell
npm init -y
```

第三步：安装  

```shell
npm i babel-cli babel-preset-env browserify
```

第四步：使用babel转换  

```shell
npx babel js（js目录） -d dist/js（转化后的js目录） --presets=babel-preset-env
```

第五步：打包  

```shell
npx browserify dist/js/app.js -o dist/bundle.js
```

第六步：在使用时引入bundle.js  

```html
<script src="./js/bundle.js" type="module"></script>
```





#### 转换前后对比：  

###### 转换前 :

```javascript
//分别暴露
export let school = '尚硅谷';
export function teach() {
	console.log("我们可以教给你开发技能");
}
```

###### 转换后：  

```javascript
"use strict";
Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.teach = teach;
//分别暴露
var school = exports.school = '尚硅谷';
function teach() {
	console.log("我们可以教给你开发技能");
}
```



### 21、ES6模块化引入NPM包

像导入模块一样导入npm包；

#### 演示：

第一步：安装jquery：  

```shell
npm i jquery
```

第二步：在app.js使用jquery  

```javascript
//入口文件
//修改背景颜色为粉色
import $ from 'jquery';// 相当于const $ = require("jquery");
$('body').css('background','pink');
```









## ES7 新特性

### 0、功能概述

##### 1、Array.prototype.includes

判断数组中是否包含某元素，语法：arr.includes(元素值)；

##### 2、指数操作符

幂运算的简化写法，例如：2的10次方：2**10；  





### 1、Array.prototype.includes

#### 概述：

Includes 方法用来检测数组中是否包含某个元素，返回布尔类型值；
判断数组中是否包含某元素，语法：arr.includes(元素值)；  





#### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>includes</title>
</head>
<body>
    <script>
        // includes
        let arr = [1,2,3,4,5];
        console.log(arr.includes(1));
    </script>
</body>
</html>
```

![image-20220222173220069](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222173220069.png)





### 2、指数操作符

#### 概述：

在 ES7 中引入指数运算符「\*\*」，用来实现幂运算，功能与 Math.pow 结果相同；
幂运算的简化写法，例如：2的10次方：2\*\*10；  



#### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>指数操作符</title>
</head>
<body>
    <script>
        // 指数操作符
        console.log(Math.pow(2,10))
        console.log(2**10);
    </script>
</body>
</html>
```

![image-20220222173540607](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222173540607.png)







## ES8 新特性

### 0、功能概述

##### 1、async 和 await

简化异步函数的写法；  

##### 2、对象方法扩展

对象方法扩展；



### 1、async 和 await

#### 概述：

async 和 await 两种语法结合可以让异步代码看起来像同步代码一样；
简化异步函数的写法；





#### async 函数：

###### 概述：

1. async 函数的返回值为 promise 对象；
2. promise 对象的结果由 async 函数执行的返回值决定；  



###### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>async函数</title>
</head>
<body>
    <script>
        // async函数：异步函数
        async function fn(){
            // return 123; // 返回普通数据
            // 若报错，则返回的Promise对象也是错误的
            // throw new Error("出错啦！");
            // 若返回的是Promise对象，那么返回的结果就是Promise对象的结果
            return new Promise((resolve,reject)=>{
                resolve("成功啦！");
                // reject("失败啦！");
            })
        } 

        const result = fn();
        // console.log(result); // 返回的结果是一个Promise对象
        // 调用then方法
        result.then(value => {
            console.log(value);
        },reason => {
            console.warn(reason);
        });
    </script>
</body>
</html>
```





#### await 表达式：

###### 概述：

1. await 必须写在 async 函数中；
2. await 右侧的表达式一般为 promise 对象；
3. await 返回的是 promise 成功的值；
4. await 的 promise 失败了, 就会抛出异常, 需要通过 try...catch 捕获处理；  



###### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>await表达式</title>
</head>
<body>
    <script>
    // async函数 + await表达式：异步函数
    // 创建Prmise对象
    const p = new Promise((resolve,reject)=>{
    	resolve("成功啦！");
    })
    
    async function fn(){
        // await 返回的是 promise 成功的值
        let result = await p;
        console.log(result); // 成功啦！
    } 
        
    fn();
    </script>
</body>
</html>
```





#### async 和 await 读取文件案例：  

```javascript
// 导入模块
const fs = require("fs");
// 读取
function readText() {
    return new Promise((resolve, reject) => {
        fs.readFile("./resources/text.txt", (err, data) => {
            //如果失败
            if (err) reject(err);
            //如果成功
            resolve(data);
        })
    })
}
function readTest1() {
    return new Promise((resolve, reject) => {
        fs.readFile("./resources/text1.txt", (err, data) => {
            //如果失败
            if (err) reject(err);
            //如果成功
            resolve(data);
        })
    })
} 
function readTest2() {
    return new Promise((resolve, reject) => {
        fs.readFile("./resources/text2.txt", (err, data) => {
            //如果失败
            if (err) reject(err);
            //如果成功
            resolve(data);
        })
    })
} 
//声明一个 async 函数
async function main() {
    //获取为学内容
    let t0 = await readText();
    //获取插秧诗内容
    let t1 = await readTest1();
    // 获取观书有感
    let t2 = await readTest2();
    console.log(t0.toString());
    console.log(t1.toString());
    console.log(t2.toString());
} 

main();
```

![image-20220222175218003](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222175218003.png)





#### async 和 await 结合发送ajax请求：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>async 和 await 结合发送ajax请求</title>
</head>
<body>
    <script>
        // async 和 await 结合发送ajax请求
        function sendAjax(url) {
            return new Promise((resolve, reject) => {
                // 1、创建对象
                const x = new XMLHttpRequest();
                // 2、初始化
                x.open("GET", url);
                // 3、发送
                x.send();
                // 4、事件绑定
                x.onreadystatechange = function () {
                    if (x.readyState == 4) {
                        if (x.status >= 200 && x.status <= 299) {
                            // 成功
                            resolve(x.response);
                        } else {
                            // 失败
                            reject(x.status);
                        }
                    }
                }
            });
        } 
        // 测试
        // const result = sendAjax("https://api.apiopen.top/getJoke");
        // result.then(value=>{
        // console.log(value);
        // },reason=>{
        // console.warn(reason);
        // })
        // 使用async和await
        async function main() {
            let result = await sendAjax("https://api.apiopen.top/getJoke");
            console.log(result);
        }
        main();
    </script>
</body>
</html>
```

![image-20220222180004915](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222180004915.png)



### 2、对象方法扩展  

1. Object.values()方法：返回一个给定对象的所有可枚举属性值的数组；
2. Object.entries()方法：返回一个给定对象自身可遍历属性 [key,value] 的数组；
3. Object.getOwnPropertyDescriptors()该方法：返回指定对象所有自身属性的描述对象；  



```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>对象方法扩展</title>
</head>
<body>
    <script>
        // 对象方法扩展
        let school = {
            name: "訾博",
            age: 24,
            sex: "男"
        }
        // 获取对象所有的键
        console.log(Object.keys(school));
        // 获取对象所有的值
        console.log(Object.values(school));
        // 获取对象的entries
        console.log(Object.entries(school));
        // 创建map
        const map = new Map(Object.entries(school));
        console.log(map);
        console.log(map.get("name"));
        // 返回指定对象所有自身属性的描述对象
        console.log(Object.getOwnPropertyDescriptors(school));
        // 参考内容：
        // const obj = Object.create(null, {
        //     name: {
        //         // 设置值
        //         value: "訾博",
        //         // 属性特性
        //         writable: true,
        //         configuration: true,
        //         enumerable: true
        //     }
        // });
    </script>
</body>
</html>
```

![image-20220223110645149](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223110645149.png)



## ES9 新特性

### 0、功能概述

##### 1、Rest 参数与 spread 扩展运算符  

在对象中使Rest参数与spread扩展运算符；

##### 2、正则扩展

简化和增强正则匹配；  





### 1、Rest 参数与 spread 扩展运算符

#### 概述：

Rest 参数与 spread 扩展运算符在 ES6 中已经引入，不过 ES6 中只针对于数组，在 ES9 中为对象提供了
像数组一样的 rest 参数和扩展运算符；  



#### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Rest参数与spread扩展运算符</title>
</head>
<body>
    <script>
        // Rest参数与spread扩展运算符
        // Rest 参数与 spread 扩展运算符在 ES6 中已经引入，
        // 不过 ES6 中只针对于数组，在 ES9 中为对象提供了像
        // 数组一样的 rest 参数和扩展运算符；
        //rest 参数
        function connect({
            host,
            port,
            ...user
        }) 
        {
            console.log(host);
            console.log(port);
            console.log(user);
        }
        connect({
            host: '127.0.0.1',
            port: 3306,
            username: 'root',
            password: 'root',
            type: 'master'
        });
        //对象合并
        const skillOne = {
            q: '天音波'
        }
        const skillTwo = {
            w: '金钟罩'
        }
        const skillThree = {
            e: '天雷破'
        }
        const skillFour = {
            r: '猛龙摆尾',
            // 自己测试，可用
            z: '胡说八道'
        }
        const mangseng = {
            ...skillOne,
            ...skillTwo,
            ...skillThree,
            ...skillFour
        };
        console.log(mangseng)
        // ...skillOne => q: '天音波', w: '金钟罩'
    </script>
</body>
</html>
```

![image-20220223111441719](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223111441719.png)



### 2、正则扩展：命名捕获分组

#### 概述：

ES9 允许命名捕获组使用符号『?』,这样获取捕获结果可读性更强；  



#### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>正则扩展：命名捕获分组</title>
</head>
<body>
    <script>
        // 正则扩展：命名捕获分组
        // 声明一个字符串
        let str = '<a href="http://www.baidu.com">訾博</a>';
        // 需求：提取url和标签内文本
        // 之前的写法
        const reg = /<a href="(.*)">(.*)<\/a>/;
        // 执行
        const result = reg.exec(str);
        console.log(result);
        // 结果是一个数组，第一个元素是所匹配的所有字符串
        // 第二个元素是第一个(.*)匹配到的字符串
        // 第三个元素是第二个(.*)匹配到的字符串
        // 我们将此称之为捕获
        console.log(result[1]);
        console.log(result[2]);
        // 命名捕获分组
        const reg1 = /<a href="(?<url>.*)">(?<text>.*)<\/a>/;
        const result1 = reg1.exec(str);
        console.log(result1);
        // 这里的结果多了一个groups
        // groups:
        // text:"訾博"
        // url:"http://www.baidu.com"
        console.log(result1.groups.url);
        console.log(result1.groups.text);
    </script>
</body>
</html>
```

![image-20220223111836435](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223111836435.png)



### 3、正则扩展：反向断言

#### 概述：

ES9 支持反向断言，通过对匹配结果前面的内容进行判断，对匹配进行筛选；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>正则扩展：反向断言</title>
</head>
<body>
    <script>
        // 正则扩展：反向断言
        // 字符串
        let str = "JS5201314你知道么555啦啦啦";
        // 需求：我们只想匹配到555
        // 正向断言
        const reg = /\d+(?=啦)/; // 前面是数字后面是啦
        const result = reg.exec(str);
        console.log(result);
        // 反向断言
        const reg1 = /(?<=么)\d+/; // 后面是数字前面是么
        const result1 = reg.exec(str);
        console.log(result1);
    </script>
</body>
</html>
```

![image-20220223112016354](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223112016354.png)



### 4、正则扩展：dotAll 模式  

#### 概述：

正则表达式中点.匹配除回车外的任何单字符，标记『s』改变这种行为，允许行终止符出现；  



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>正则扩展：dotAll 模式</title>
</head>
<body>
    <script>
        // 正则扩展：dotAll 模式
        // dot就是. 元字符，表示除换行符之外的任意单个字符
        let str = `
                    <ul>
                    <li>
                    <a>肖生克的救赎</a>
                    <p>上映日期: 1994-09-10</p>
                    </li>
                    <li>
                    <a>阿甘正传</a>
                    <p>上映日期: 1994-07-06</p>
                    </li>
                    </ul>
                    `;
        // 需求：我们想要将其中的电影名称和对应上映时间提取出来，存到对象
        // 之前的写法
        // const reg = /<li>\s+<a>(.*?)<\/a>\s+<p>(.*?)<\/p>/;
        // dotAll 模式
        const reg = /<li>.*?<a>(.*?)<\/a>.*?<p>(.*?)<\/p>/gs;
        // const result = reg.exec(str);
        // console.log(result);
        let result;
        let data = [];
        while (result = reg.exec(str)) {
            console.log(result);
            data.push({ title: result[1], time: result[2] });
        } 
        console.log(data);
    </script>
</body>
</html>
```

![image-20220223112257290](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223112257290.png)







## ES10 新特性

### 0、功能概述

1. Object.fromEntries

   将二维数组或者map转换成对象；

2. trimStart 和 trimEnd

   去除字符串前后的空白字符；

3. Array.prototype.flat 与 flatMap

   将多维数组降维；

4. Symbol.prototype.description

   获取Symbol的字符串描述；





### 1、Object.fromEntries

#### 概述：

将二维数组或者map转换成对象；

之前学的Object.entries是将对象转换成二维数组；  





#### 代码实现：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Object.fromEntries</title>
</head>
<body>
    <script>
        // Object.fromEntries：将二维数组或者map转换成对象
        // 之前学的Object.entries是将对象转换成二维数组
        // 此方法接收的是一个二维数组，或者是一个map集合
        // 二维数组
        const result = Object.fromEntries([
            ["name","訾博"],
            ["age",24],
        ]);
        console.log(result);
        const m = new Map();
        m.set("name","张三");
        m.set("age",23);
        const result1 = Object.fromEntries(m);
        console.log(result1);
    </script>
</body>
</html>
```

![image-20220223112840849](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223112840849.png)



### 2、trimStart 和 trimEnd  

#### 概述：

去掉字符串前后的空白字符；





#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>trimStart 和 trimEnd</title>
</head>
<body>
    <script>
    // trimStart 和 trimEnd
    let str = " zibo ";
    console.log(str.trimLeft());
    console.log(str.trimRight());
    console.log(str.trimStart());
    console.log(str.trimEnd());
    </script>
</body>
</html>
```

![image-20220223114652352](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223114652352.png)



### 3、Array.prototype.flat 与 flatMap

#### 概述：

将多维数组转换成低维数组；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Array.prototype.flat 与 flatMap</title>
</head>
<body>
    <script>
        // Array.prototype.flat 与 flatMap
        // flat
        // 将多维数组转换成低维数组
        // 将二维数组转换成一维数组
        const arr = [1, 2, 3, [4, 5], 6, 7];
        console.log(arr.flat());
        // 将三维数组转换成二维数组
        const arr2 = [1, 2, 3, [4, 5, [6, 7]], 8, 9];
        console.log(arr2.flat());
        // 将三维数组转换成一维数组
        console.log(arr2.flat(2));
        // flatMap
        const arr3 = [1, 2, 3, 4, 5];
        const result0 = arr3.map(item => item * 10);
        console.log(result0);
        const result = arr3.map(item => [item * 10]);
        console.log(result);
        const result1 = arr3.flatMap(item => [item * 10]);
        console.log(result1);
    </script>
</body>
</html>
```

![image-20220223114946077](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223114946077.png)

### 4、Symbol.prototype.description

#### 概述：

获取Symbol的描述字符串；  



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Symbol.prototype.description</title>
</head>
<body>
    <script>
    // Symbol.prototype.description
    // 获取Symbol的描述字符串
    // 创建Symbol
    let s = Symbol("訾博");
    console.log(s.description)
    </script>
</body>
</html>
```

![image-20220223115109033](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223115109033.png)





## ES11 新特性

### 0、功能概述

1. String.prototype.matchAll
   用来得到正则批量匹配的结果；
2. 类的私有属性
   私有属性外部不可访问直接；
3. Promise.allSettled
   获取多个promise执行的结果集；
4. 可选链操作符
   简化对象存在的判断逻辑；
5. 动态 import 导入
   动态导入模块，什么时候使用什么时候导入；
6. BigInt
   大整型；
7. globalThis 对象
   始终指向全局对象window；  



### 1、String.prototype.matchAll

#### 概述：

用来得到正则批量匹配的结果；

#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>String.prototype.matchAll</title>
</head>
<body>
    <script>
        // String.prototype.matchAll
        // 用来得到正则批量匹配的结果
        let str = `
                <ul>
                    <li>
                        <a>肖生克的救赎</a>
                        <p>上映日期: 1994-09-10</p>
                    </li>
                    <li>
                        <a>阿甘正传</a>
                        <p>上映日期: 1994-07-06</p>
                    </li>
                </ul>
                `;
        // 正则
        const reg = /<li>.*?<a>(.*?)<\/a>.*?<p>(.*?)<\/p>/sg;
        const result = str.matchAll(reg);
        // 返回的是可迭代对象，可用扩展运算符展开
        // console.log(...result);
        // 使用for...of...遍历
        for (let v of result) {
            console.log(v);
        }
    </script>
</body>
</html>
```

![image-20220223115649101](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223115649101.png)



### 2、类的私有属性

#### 概述：

私有属性外部不可访问直接；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>类的私有属性</title>
</head>
<body>
    <script>
        // 类的私有属性
        class Person {
            // 公有属性
            name;
            // 私有属性
            #age;
            #weight;
            // 构造方法
            constructor(name, age, weight) {
                this.name = name;
                this.#age = age;
                this.#weight = weight;
            } 
            intro() {
                console.log(this.name);
                console.log(this.#age);
                console.log(this.#weight);
            }
        }
        // 实例化
        const girl = new Person("小兰", 18, "90kg");
        console.log(girl);
        // 公有属性的访问
        console.log(girl.name);
        // 私有属性的访问
        console.log(girl.age); // undefined
        // 报错Private field '#age' must be declared in an enclosing class
        // console.log(girl.#age);
        girl.intro();
    </script>
</body>
</html>
```

![image-20220223120001141](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223120001141.png)





### 3、Promise.allSettled

#### 概述：

获取多个promise执行的结果集；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Promise.allSettled</title>
</head>
<body>
    <script>
        // Promise.allSettled
        // 获取多个promise执行的结果集
        // 声明两个promise对象
        const p1 = new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve("商品数据——1");
            }, 1000);
        });
        const p2 = new Promise((resolve, reject) => {
            setTimeout(() => {
                reject("失败啦");
            }, 1000);
        });
        // 调用Promise.allSettled方法
        const result = Promise.allSettled([p1, p2]);
        console.log(result);
        const result1 = Promise.all([p1, p2]); // 注意区别
        console.log(result1);
    </script>
</body>
</html>
```



![image-20220223120708253](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223120708253.png)





### 4、可选链操作符

#### 概述：

如果存在则往下走，省略对对象是否传入的层层判断；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>可选链操作符</title>
</head>
<body>
    <script>
        // 可选链操作符
        // ?.
        function main(config) {
            // 传统写法
            // const dbHost = config && config.db && config.db.host;
            // 可选链操作符写法
            const dbHost = config?.db?.host;
            console.log(dbHost);
        }
        main({
            db: {
                host: "192.168.1.100",
                username: "root"
            },
            cache: {
                host: "192.168.1.200",
                username: "admin"
            }
        });
    </script>
</body>
</html>
```

![image-20220223121235412](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223121235412.png)







### 5、动态 import 导入

#### 概述：

动态导入模块，什么时候使用时候导入；



#### 代码实现：

###### hello.js：  

```javascript
export function hello(){
	alert('Hello');
}
```

###### app.js

```javascript
// import * as m1 from "./hello.js"; // 传统静态导入
//获取元素
const btn = document.getElementById('btn');
btn.onclick = function(){
    import('./hello.js').then(module => {
    	module.hello();
    });
}
```

###### 动态import加载.html：  

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>动态 import </title>
</head>
<body>
    <button id="btn">点击</button>
    <script src="app.js" type="module"></script>
</body>
</html>
```

![image-20220223130743505](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223130743505.png)



### 6、BigInt

#### 概述：

更大的整数；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>BigInt</title>
</head>
<body>
    <script>
        // BigInt
        // 大整型
        let n = 100n;
        console.log(n, typeof (n));
        // 函数：普通整型转大整型
        let m = 123;
        console.log(BigInt(m));
        // 用于更大数值的运算
        let max = Number.MAX_SAFE_INTEGER;
        console.log(max);
        console.log(max + 1);
        console.log(max + 2); // 出错了
        console.log(BigInt(max));
        console.log(BigInt(max) + BigInt(1));
        console.log(BigInt(max) + BigInt(2));
    </script>
</body>
</html>
```



![image-20220223130950954](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223130950954.png)





### 7、globalThis 对象

#### 概述：

始终指向全局对象window；



#### 代码实现：  

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>globalThis 对象</title>
</head>
<body>
    <script>
        // globalThis 对象 : 始终指向全局对象window
        console.log(globalThis);
    </script>
</body>
</html>
```

![image-20220223131129169](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220223131129169.png)



