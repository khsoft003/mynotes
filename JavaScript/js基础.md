

## 一些重要的话



- 在JavaScript中，一切皆对象。

- 在JavaScript中，函数被称为一等公民。

  - 在JavaScript中，函数是一种特殊的数据类型。

  - 函数可以被当做参数传递，被当做值来返回。

  - 函数自身的作用域在声明的地方，而不是调用的地方

  - **闭包**：

    ```javascript
    var a = 8;
    function f1(){
        var a = 1;
        function f2(){
            console.log(a++);
        }
        // 由于f1的运行结果是返回f2，又由于作用域链及函数自身作用域的问题，
        // 导致f1运行结果后，不能销毁变量，因此形成闭包。
        return f2;
    }
    
    var f = f1();
    f();//:1
    f();//:2
    ```

    闭包优点：通过闭包，在全局空间操作了局部空间里面的变量

    闭包缺点：闭包会一直保存着作用域，又不会被垃圾回收机制销毁，就会造成内存的大量消耗。谨慎合理的利用闭包。

- 两链一包：两链指的是作用域链和原型链，而这个一包呢，指的就是闭包。









## [null与undefined的区别](https://www.cnblogs.com/calvin-dong/p/11217880.html)

#### 1、首先看一个判断题：null和undefined 是否相等

  ```javascript
  console.log(null==undefined)//true
  console.log(null===undefined)//false
  ```

观察可以发现：null和undefined 两者相等，但是当两者做全等比较时，两者又不等。

原因：

- null： Null类型，代表“空值”，代表一个空对象指针，使用typeof运算得到 “object”，所以你可以认为它是一个特殊的对象值。
- undefined： Undefined类型，当一个声明了一个变量未初始化时，得到的就是undefined。


实际上，undefined值是派生自null值的，ECMAScript标准规定对二者进行相等性测试要返回true

#### 2、那到底什么时候是null,什么时候是undefined呢？

#####   null表示"没有对象"，即该处不应该有值。典型用法是：

​	（1） 作为函数的参数，表示该函数的参数不是对象。

​	（2） 作为对象原型链的终点。

#####  undefined表示"缺少值"，就是此处应该有一个值，但是还没有定义。典型用法是：

​	（1）变量被声明了，但没有赋值时，就等于undefined。

​	（2) 调用函数时，应该提供的参数没有提供，该参数等于undefined。

​	（3）对象没有赋值的属性，该属性的值为undefined。

​	（4）函数没有返回值时，默认返回undefined。