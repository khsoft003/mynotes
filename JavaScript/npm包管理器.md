## npm：Node Package Manager

- npm官网：https://www.npmjs.com
- npm： 即node包管理器，是Nodejs默认的，以JavaScript编写的软件包管理器
- 在官网搜索一个包的名字，可以查看安装此包的具体命令
- 安装Nodejs会默认安装好npm



### npm镜像的设置与查看

#### 设置成淘宝镜像方式一：

###### 将npm设置成淘宝镜像

```sh
npm config set registry https://registry.npm.taobao.org --global
npm config set disturl https://npm.taobao.org/dist --global
```



###### 查看镜像的配置结果

```sh
npm config get registry 
npm config get disturl 
```



###### 使用nrm工具切换淘宝源

```sh
npx nrm use taobao
```



###### 切换回官方源

```sh
npx nrm use npm
```



#### 设置成淘宝镜像方式二：

你可以使用淘宝定制的cnpm(gzip压缩支持)命令行工具代替默认的npm:

```sh
npminstall -g cnpm --registry-https://registry.npm.taobao.org
```





#### 设置当前地址(恢复到默认地址)

npm config set registry https://registry.npmjs.org/





### 初始化项目

```sh
npm init --yes
npm init -y
```

生成一个 package.json，类似下面：

```json
{
  "name": "test",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}

```





### 安装一个包

```sh
npm install packageName@3.2.1 -g  # 全局安装具体版本为3.2.1的package
npm install packageName # 安装packageName包
```

- 包会被安装到node_modules文件夹下。
- 如果没有package-lock.json文件的话，也会同时生成一个package-lock.json
- 同时会在package.json文件的dependencies中添加此包的配置



#### 安装运行时依赖

**安装jquery包**

```sh
npm install jquery
```

安装后的package.json文件如下，注意dependencies的变化：

```json
{
  "name": "test",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "jquery": "^3.6.0"
  }
}
```



#### 安装开发时依赖

**安装webpack**

```sh
npm install webpack --save-dev
```

安装后的package.json文件如下，注意devDependencies的变化：

```json
{
  "name": "test",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "jquery": "^3.6.0"
  },
  "devDependencies": {
    "webpack": "^5.65.0"
  }
}
```









### 安装所有包

```sh
npm install 
```

会把package.json的dependencies中所有的依赖项全部安装



### 更新一个包

```sh
npm update packageName
```





### 卸载一个包

```sh
npm uninstall packageName
```





### npm常用操作

```sh
npm -V  # 通过查看版本, 看npm是否安装成功
npm install <Module Name>     # 使用 npm命令安装模块
npm install <Module Name> -g  # 可以直接在命令 行里使用
npm list -g   # 查看所有全局安装的模块
npm list vue  # 查看某个模块的版本号
npm -g install npm@5.9.1 # (@后跟版本号) 这样我们就可以更新npm版本
npm install --save moduleName   # --save在package文件的dependencies节点写入依赖。
npm install --save-dev moduleName # -save-dev 在package文件的devDependencies节点写入依赖
npm update <package>     # 更新当前面目录node_modules目录中的模块至最新版本
npm update <package> -g  # 更新全局中的模块至最新版本
npm cache clear   # 可以清空npm本地缓存
```

- dependencies:运行时的依赖，发布后，即生产环境下还需要用的模块
- devDependencies:开发时的依赖。里面的模块是开发时用的,发布时用不到它,比如项目中使用的gulp，压缩css、js的模块。这些模块在我们的项目部署后是不需要的





### package.json属性说明

- name -包名。

- version -包的版本号。

- description -包的描述。

- homepage -包的官网url。

- author -包的作者姓名。

- contributors -包的其他贡献者姓名。

- dependencies -依赖包列表。如果依赖包没有安装, npm会自动将依赖包安装在node_ module目录下。

- repository -包代码存放的地方的类型,可以是git或svn, git 可在Github上。

- main - main字段指定了程序的主入口文件，require(" moduleName')就会加载这个文件。这个字段的默认值是模块根目录下面的index.js。

- keytvords -关键字

- scripts 可执行脚本，例如下面的脚本运行命令为：npm run test  

  ```json
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1"
    }
  ```

  

package.json文件中版本号的说明，安装的时候代表不同的含义:

> -  "5.0.3” 表示安装指定的5.0.3版本
> - "~5.0.3"表示安装5.0.X中最新的版本
> - "^5.0.3"表示安装5.X.X中最新的版本





### ES6兼容性解决

- 兼容表：http://kangax.github.io/compat-table/es6/
- IE10+、Chrome、FireFox、移动端、NodeJS都支持
- 兼容低版本浏览器
  - 在线转换（babel，jsx）
  - 提前编译（babel）





## yarn包管理器

### yarn的安装

1. 下载node.js, 使用npm安装

```sh
npm install -9 yarn
yarn --version # 查看版本
```

2. 安装node.js,"下载yarn的安装程序:
   	提供一个.msi文件， 在运行时将引导您在Windows. 上安装Yarn

3. Yarn淘宝源安装，分别复制粘贴以下代码行到黑窗口运行即可

   ```sh
   yarn config set registry https://registry.npm.taobao.org -g
   yarn config set sass_ binary _site http://cdn.npm.taobao.org/dist/node-sass -g
   ```

   

### yarn的基本使用

```sh
yarn init    # 初始化项目同npm init,执行输入信息后，会生成package.json文件
yarn install # 安装package.json里所有包，并将包及它的所有依赖项保存进yarn.lock
yarn install --flat  # 安装-个包的单- -版本
yarn install --force # 强制重新下载所有包
yarn install --production    # 只安装dependencies里的包
yarn install --no-lockfile   # 不读取或生成yarn.lock
yarn install --pure-lockfile # 不生成yarn.lock
yarn add [package]           # 在当前的项目中添加一个依赖包,会自动更新到package.json和yarn.lock文件中
yarn add [package]@[version] # 安装指定版本,这里指的是主要版本,如果需要精确到小版本，使用-E参数
yarn add [package]@[tag]     # 安装某个tag (比如beta,next或者latest)
yarn add --dev/-D      # 加到devDependencies
yarn add --peer/-P     # 加到peerDependencies
yarn add --optional/-O # 加到optionalDependencies

# 默认安装包的主要版本里的最新版本，下面两个命令可以指定版本:
yarn add --exact/-E # 安装包的精确版本。例如yarn add foo@1.2.3会接受1.9.1版，但是yarn add foo@1.2.3 --exact只会接受1.2.3版
yarn add --tilde/-T # 安装包的次要版本里的最新版。 例如yarn add foo@1.2.3 --tilde会接受1.2.9,但不接受1.3.0
yarn publish     # 发布包
yarn remove <packageName>  # 移除一个包, 会自动更新package.json和yarn.lock
yarn upgrade    # 更新一个依赖,用于更新包到基于规范范围的最新版本
yarn run        # 运行脚本, 用来执行在package.json中scripts属性下定义的脚本
yarn info <packageName>    # 显示某个包的信息,可以用来查看某个模块的最新版本信息

#缓存yarn cache
yarn cache list   #列出已缓存的每个包
yarn cache dir    #返回全局缓存位置
yarn cache clean  #清除缓存

```

