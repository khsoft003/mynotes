## 为什么使用WebPack

- 解决js文件引用的前后顺序问题

- 解决js变量作用域问题

- 解决代码拆分问题

- 让浏览器支持模块

  

## WebPack竞品

- Parcel : 号称零配置，开箱即用
- rollup.js：只处理打包js，可用于开发类库
- Vite：基于esmodule，Vue3推荐，未来可期



## WebPack安装

```sh
npm install webpack --save-dev
npm install webpack-cli --save-dev
```



## WebPack命令

```sh
webpack	# 打包
webpack --stats detailed	# 打包详细信息
webpack -v  # 查看版本号
webpack --help  # 帮助
webpack --entry ./src/index.js  # 指定入口js文件
webpack --mode	production  # 生产环境模式
```





## webpack.config.js

```javascript
const path = require('path')
module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.resovle(__dirname, './dist'),
        filename: "bundle.js"
    },
    mode:'none'
};
```



## WebPack插件

![image-20220402115716140](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220402115716140.png)