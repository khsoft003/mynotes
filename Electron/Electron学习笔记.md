## 安装

前提：已经安装[node](https://so.csdn.net/so/search?q=node&spm=1001.2101.3001.7020)，并且是10以上的版本

```shell
cnpm install electron --save-dev //仅安装在当前项目
cnpm install -g electron // 全局安装
npx electron -v // 查看版本
npx electron // 打开Electron界面
npx electron . // 运行该程序
```



## 打包成exe文件

> cnpm install electron-packager --save-dev //安装electron-packager
> “packager”: “electron-packager ./ HelloWorld --all --out ./outApp --overwrite --icon=./app/img/icon/icon.ico” // 在package.json添加这个脚本
> 在项目中新建outAPP文件夹。
> cnpm run-script packager // 打包



## 其它资源

官方网站
https://www.electronjs.org/





## Demo

### Demo1：Hello World

效果：打开一个普通应用程序的界面，主界面只有Hello World

```javascript
var electron = require('electron');

var app = electron.app // 引用app
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({width: 800, height: 800})
    mainWindow.loadFile('index.html') // 加载html界面，也就是渲染进程
    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})
```



### Demo2：读取xiaojiejie.txt文件的内容

html

```html
<button id="btn">小姐姐请进来</button>
<div id="mybaby"></div>
<script src="./render/index.js"></script>
```

index.js


```javascript
var fs = require('fs')
window.onload = function(){
    var btn = this.document.querySelector("#btn");
    var mybaby = this.document.querySelector("#mybaby");
    btn.onclick = function(){
        fs.readFile('xiaojiejie.txt', (err,data)=>{
            mybaby.innerHTML = data;
        })
    }
}
```

main.js

```javascript
var electron = require('electron');

var app = electron.app // 引用app
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        webPreferences: {nodeIntegration: true} // 让node下的所有东西都可以在渲染进程中使用
    })
    mainWindow.loadFile('index.html') // 加载html界面，也就是渲染进程
    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})
```





### Demo3：点击打开新窗口

demo.html

```html
<button id="btn">
    打开新的窗口
</button>
<script src="./render/demo.js"></script>
```

yellow.html

```html
<body style="background-color: yellow;">
    <h1>我是黄色页面</h1>
</body>
```

demo.js

```javascript
const btn = this.document.querySelector('#btn')
const BrowserWindow = require('electron').remote.BrowserWindow

window.onload = function(){
    btn.onclick = ()=>{
        newWin = new BrowserWindow({
            width: 500,
            height: 500
        })
        newWin.loadFile('yellow.html')
        newWin.on('close',()=>{
            newWin = null
        })
    }
}
```

main.js

```javascript
var electron = require('electron');

var app = electron.app // 引用app
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        webPreferences: {nodeIntegration: true} // 让node下的所有东西都可以在渲染进程中使用
    })
    mainWindow.loadFile('demo.html') // 加载html界面，也就是渲染进程
    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})
```





### Demo4：顶部菜单栏

demo.html

```html
<button id="btn">
    打开新的窗口
</button>
<script src="./render/demo.js"></script>
```

menu.js

```javascript
const {Menu, BrowserWindow} = require('electron');
var template = [
    {
        label: '凤来仪洗浴会所',
        submenu: [
            {
                label: '精品SPA',
                accelerator: 'ctrl + n', // 给该菜单添加快捷键
                click:()=>{
                    var win = new BrowserWindow({
                        width: 500,
                        height: 500,
                        webPreferences: {nodeIntegration: true}
                    })
                    win.loadFile('yellow.html')
                    win.on('close', ()=>{
                        win = null;
                    })
                }
            },
            {label: '泰式按摩'}
        ]
    },
    {
        label: '大浪淘沙洗浴中心',
        submenu:[
            {label: '牛奶玫瑰浴'},
            {label: '爱情拍拍手'}
        ]
    }
]

var m = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(m)
```

main.js

```javascript
var electron = require('electron');

var app = electron.app // 引用app
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        webPreferences: {nodeIntegration: true} // 让node下的所有东西都可以在渲染进程中使用
    })
    require('./main/menu.js')
    mainWindow.loadFile('demo.html') // 加载html界面，也就是渲染进程
    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})
```









### Demo5：右键菜单

demo.html

```html
<button id="btn">
    打开新的窗口
</button>
<script src="./render/demo.js"></script>
```

demo.js

```javascript
// const { require } = require('globalthis/implementation')

const btn = this.document.querySelector('#btn')
const BrowserWindow = require('electron').remote.BrowserWindow

window.onload = function(){
    btn.onclick = ()=>{
        newWin = new BrowserWindow({
            width: 500,
            height: 500
        })
        newWin.loadFile('yellow.html')
        newWin.on('close',()=>{
            newWin = null
        })
    }
}

const {remote} = require('electron')

var rightTemplate = [
    {label: '粘贴', accelerator: 'ctrl+c'},
    {label: '复制', accelerator: 'ctrl+v'}
]

var m = remote.Menu.buildFromTemplate(rightTemplate)

window.addEventListener('contextmenu',function(e){
    // alert(111)
    e.preventDefault()
    m.popup({window:remote.getCurrentWindow()})
})
```





### Demo6：在浏览器中打开超链接

demo2.html

```html
<h1>
    <a id="aHref" href="https://jspang.com">技术胖的博客</a>
</h1>
<script src="./render/demo2.js"></script>
```

demo2.js

```javascript
var {shell} = require('electron')
var aHref = document.querySelector('#aHref')

aHref.onclick = function(e){
    e.preventDefault()
    var href = this.getAttribute('href')
    shell.openExternal(href)
}
```

main.js

```javascript
var electron = require('electron');

var app = electron.app // 引用app
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        webPreferences: {nodeIntegration: true} // 让node下的所有东西都可以在渲染进程中使用
    })
    mainWindow.webContents.openDevTools()
    require('./main/menu.js')
    mainWindow.loadFile('demo2.html') // 加载html界面，也就是渲染进程
    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})
```





### Demo7：内嵌一个网页

main.js

```javascript
var electron = require('electron');
const { console } = require('globalthis/implementation');

var app = electron.app // 引用app
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        webPreferences: {nodeIntegration: true} // 让node下的所有东西都可以在渲染进程中使用
    })
    mainWindow.webContents.openDevTools()
    require('./main/menu.js')
    mainWindow.loadFile('demo2.html') // 加载html界面，也就是渲染进程

    // BrowserView
    var BrowserView = electron.BrowserView
    var view = new BrowserView()
    mainWindow.setBrowserView(view)
    view.setBounds({x:0, y:120, width:1000, height:680})
    view.webContents.loadURL('https://jspang.com')

    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})
```





### Demo8：子窗口——在新窗口打开此链接

demo2.html

```html
<h1>
    <a id="aHref" href="https://jspang.com">技术胖的博客</a>
</h1>
<button id="mybtn">打开子窗口</button>
<script src="./render/demo2.js"></script>
```

demo2.js

```javascript
var {shell} = require('electron')
var aHref = document.querySelector('#aHref')

aHref.onclick = function(e){
    e.preventDefault()
    var href = this.getAttribute('href')
    shell.openExternal(href)
}

var mybtn = document.querySelector('#mybtn')
mybtn.onclick = function(e){
    window.open('https://jspang.com')
}
```







### Demo9：子窗口向父窗口传递信息

demo2.html

```html
<h1>
    <a id="aHref" href="https://jspang.com">技术胖的博客</a>
</h1>
<button id="mybtn">打开子窗口</button>
<!--接收子窗口传递过来的信息-->
<div id="mytext"></div>
<script src="./render/demo2.js"></script>
```

demo2.js

```javascript
var {shell} = require('electron')
// const { window, document } = require('globalthis/implementation')
var aHref = document.querySelector('#aHref')

aHref.onclick = function(e){
    e.preventDefault()
    var href = this.getAttribute('href')
    shell.openExternal(href)
}

var mybtn = document.querySelector('#mybtn')
mybtn.onclick = function(e){
    window.open('./popup_page.html')
}

window.addEventListener('message', (msg)=>{
    let mytext = document.querySelector('#mytext')
    mytext.innerHTML = JSON.stringify(msg.data)
})
```

popup_page.html

```html
<h2>我是弹出子窗口</h2>
    <button id="popbtn">向父窗口传递信息</button>
    <script>
        var popbtn = this.document.querySelector('#popbtn')
        popbtn.onclick = function(e){
            // alert('111')
            window.opener.postMessage('我是子窗口传递过来的信息')
        }
    </script>
```





### Demo10：打开一张图片并且在应用中显示出来

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <button id="openBtn">打开美女图片</button>
    <img id="images" src="" style="width: 100%;" />
</body>
<script>
    const {dialog} = require('electron').remote
    var openBtn = document.getElementById('openBtn')
    openBtn.onclick = function(){
        dialog.showOpenDialog({
            title: '请选择你喜欢的小姐姐照片',
            defaultPath: './xiaojiejie/a1.jpg',
            filters: [{name: 'img', extensions:['jpg']}],
            buttonLabel: '打开小姐姐'
        }).then(result=>{
            let image = document.getElementById('images')
            image.setAttribute("src", result.filePaths[0])
        }).catch(err=>{
            consle.log(err)
        })
    }
</script>
</html>
```





### Demo11：保存文件

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <button id="openBtn">打开美女图片</button>
    <button id="saveBtn">保存文件</button>
    <img id="images" src="" style="width: 100%;" />
</body>
<script>
    const {dialog} = require('electron').remote
    const fs = require('fs')
    var openBtn = document.getElementById('openBtn')
    openBtn.onclick = function(){
        dialog.showOpenDialog({
            title: '请选择你喜欢的小姐姐照片',
            defaultPath: './xiaojiejie/a1.jpg',
            filters: [{name: 'img', extensions:['jpg']}],
            buttonLabel: '打开小姐姐'
        }).then(result=>{
            let image = document.getElementById('images')
            image.setAttribute("src", result.filePaths[0])
        }).catch(err=>{
            consle.log(err)
        })
    }

    var saveBtn = document.getElementById('saveBtn')
    saveBtn.onclick = function(){
        dialog.showSaveDialog({
            title: '保存文件'
        }).then(result=>{
            console.log(result)
            fs.writeFileSync(result.filePath, 'jspang.com')
        }).catch(err=>{
            console.log(err)
        })
    }
</script>
</html>
```





### Demo12：消息对话框

```html
<button id="messageBtn">弹出对话框</button>
```

```javascript
var messageBtn = document.getElementById('messageBtn')
messageBtn.onclick = function(){
    dialog.showMessageBox({
        type: 'warning',
        title: '去不去由你',
        message: '是不是要跟胖哥去大宝剑',
        buttons: ['我要去', '不去了']
    }).then(result=>{
        console.log(result)
    })
}
```





### Demo13：断网提醒

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>JSPang.com 断网提醒测试</h2>
</body>
<script>
    // online offline
    window.addEventListener('online', function(){
        alert('官人，我来了，我们继续哦')
    })
    window.addEventListener('offline', function(){
        alert('小女子先行离开一会儿，请稍等')
    })
</script>
</html>
```





### Demo14：消息通知

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <button id="notifyBtn">通知消息</button>
</body>
<script>
    var notifyBtn = document.getElementById('notifyBtn')
    var option = {
        title: '小二，来订单了，出来接客了！',
        body: '有大官人翻你牌子了'
    }
    notifyBtn.onclick = function(){
        new window.Notification(option.title, option)
    }
</script>
</html>
```





### Demo15：全局快捷键的注册和取消

main.js

```javascript
var electron = require('electron');
const { console } = require('globalthis/implementation');

var app = electron.app // 引用app
var globalShortcut = electron.globalShortcut // 全局快捷键
var BrowserWindow = electron.BrowserWindow // 窗口引用

var mainWindow = null // 声明要打开的主窗口

app.on('ready', ()=>{
    mainWindow = new BrowserWindow({
        width: 800,
        height: 800,
        webPreferences: {nodeIntegration: true} // 让node下的所有东西都可以在渲染进程中使用
    })

    // 全局快捷键打开一个网页
    globalShortcut.register('ctrl+e',()=>{
        mainWindow.loadURL('https://jspang.com')
    })

    // 自动判断绑定全局快捷键是否成功
    let isRegister = globalShortcut.isRegistered('ctrl+e')?'Register Success' : 'Register Fail'
    // 注意：下面这句话会在vscode的终端中输出
    console.log('---------------->' + isRegister)

    mainWindow.webContents.openDevTools()
    require('./main/menu.js')
    mainWindow.loadFile('demo5.html') // 加载html界面，也就是渲染进程

    // BrowserView
    // var BrowserView = electron.BrowserView
    // var view = new BrowserView()
    // mainWindow.setBrowserView(view)
    // view.setBounds({x:0, y:120, width:1000, height:680})
    // view.webContents.loadURL('https://jspang.com')

    mainWindow.on('close', ()=>{
        mainWindow = null
    })
})

app.on('will-quit', function(){
    // 注销全局快捷键
    globalShortcut.unregister('ctrl+e')
    globalShortcut.unregisterAll()
})
```





### Demo16：复制到剪贴板

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>
        激活码：<span id="code">fafafqfafgggdgda</span>
        <button id="btn">复制激活码</button>
    </div>
</body>
<script>
    const {clipboard} = require('electron')
    const code = document.getElementById('code')
    const btn = document.getElementById('btn')
    btn.onclick = function(){
        clipboard.writeText(code.innerHTML)
        alert('复制成功')
    }
</script>
</html>
```

