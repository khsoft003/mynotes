### 介绍
> 个人笔记
>

这是一个个人笔记项目。

访问地址：http://jibiji.cc/



### 友情链接

[在旭的网站](http://jibiji.cc:8000)



### 网址收藏



| 分类       | 网站                                                         |
| ---------- | ------------------------------------------------------------ |
| Linux      | [Linux命令大全](https://www.linuxcool.com/)    [中标麒麟rpm](http://download.cs2c.com.cn/neokylin/desktop/everything/7.2/ls_64/os/RPMS/)    [Linux命令参数解释](https://tldr.ostera.io/)   [explainshell](https://www.explainshell.com) |
| JavaScript | [jQuery中文文档](https://www.html.cn/jqapi-1.9/)    [test-cors](http://test-cors.org/)   [W3C JS标准](https://www.w3cschool.cn/javascript_guide/)    [npm官网](https://www.npmjs.com/)    [ES6兼容性](http://kangax.github.io/compat-table/es6/)   [yarn官网](https://yarnpkg.com/)   [Nodejs](http://nodejs.cn/) |
| 开发工具   | [Qt下载](https://download.qt.io/archive/qt/)                 |
| 在线工具   | [JS在线](https://www.w3cschool.cn/tryrun/runcode?lang=javascript)   [ProcessOn](https://www.processon.com/)   [AsciiFlow](https://asciiflow.com/#/)   [原PostWomen](https://hoppscotch.io/cn)  [SearchCode](https://searchcode.com/) [Rubular](https://rubular.com/)   [爱资料工具箱](https://www.toolnb.com/)    [菜鸟工具](http://c.runoob.com/)   [我的IP](http://www.whatismyip.com.tw/)   [在线工具](https://tool.lu/)    [excalidraw](https://excalidraw.com/)   [脚本之家](http://tools.jb51.net/)   [Regex101](https://regex101.com/)   [流程图](https://app.diagrams.net/)   [端口扫描](http://coolaf.com/tool/port)     [deepl翻译](https://www.deepl.com/translator)   [JSON编辑器](http://jsoneditoronline.org/#right=local.xoyele&left=local.fatodu)   [轻松传](https://easychuan.cn/) |
| 文档教程   | [devdocs](https://devdocs.io)    [正则表达式手册](https://tool.oschina.net/uploads/apidocs/jquery/regexp.html)   [C语言技术网](http://freecplus.net/index.html)    [AndroidStudio使用艺术](https://as.quanke.name/)      [Android开发者文档](https://developer.android.google.cn/docs)     [书栈网](https://www.bookstack.cn/)   [Docker 从入门到实践](https://vuepress.mirror.docker-practice.com/)   [回车课堂](https://ke.boxuegu.com/)   [W3C的Docker教程](https://www.w3cschool.cn/reqsgr/3bue8ozt.html)    [Spring官方文档](https://www.springcloud.cc/spring-reference.html)   [SpringCloud](https://www.springcloud.cc/spring-cloud-dalston.html)  [在线API](https://www.matools.com/api)   [C语言网](https://www.dotcpp.com/)  [C++参考手册](https://zh.cppreference.com/w/%E9%A6%96%E9%A1%B5) [CodingDict](https://www.codingdict.com/tutorials)  [易百教程](https://www.yiibai.com) |
| 其他       | [阿里云云效Maven](https://developer.aliyun.com/mvn/guide)   [编程自学之路](https://www.r2coding.com) |

