## 自定义类需要继承QObject



官方文档

​	QObject是Qt对象模型的核心。该模型的中心特性是一种非常强大的无缝对象通信机制，称为信号和插槽。

​	请注意，对于实现信号、插槽或属性的任何对象，Q_OBJECT宏都是必需的。您还需要在源文件上运行元对象编译器。我们强烈建议在QObject的所有子类中使用此宏，无论它们是否实际使用信号、插槽和属性，因为如果不这样做，可能会导致某些函数显示出奇怪的行为。

**原文**

>The QObject class is the base class of all Qt objects.
>QObject is the heart of the Qt Object Model. The central feature in this model is a very powerful mechanism for seamless object communication called signals and slots. You can connect a signal to a slot with connect() and destroy the connection with disconnect(). To avoid never ending notification loops you can temporarily block signals with blockSignals(). The protected functions connectNotify() and disconnectNotify() make it possible to track connections.
>QObjects organize themselves in object trees. When you create a QObject with another object as parent, the object will automatically add itself to the parent’s children() list. The parent takes ownership of the object; i.e., it will automatically delete its children in its destructor. You can look for an object by name and optionally type using findChild() or findChildren().
>Every object has an objectName() and its class name can be found via the corresponding metaObject() (see QMetaObject::className()). You can determine whether the object’s class inherits another class in the QObject inheritance hierarchy by using the inherits() function.
>When an object is deleted, it emits a destroyed() signal. You can catch this signal to avoid dangling references to QObjects.
>QObjects can receive events through event() and filter the events of other objects. See installEventFilter() and eventFilter() for details. A convenience handler, childEvent(), can be reimplemented to catch child events.
>Last but not least, QObject provides the basic timer support in Qt; see QTimer for high-level support for timers.
>Notice that the Q_OBJECT macro is mandatory for any object that implements signals, slots or properties. You also need to run the Meta Object Compiler on the source file. We strongly recommend the use of this macro in all subclasses of QObject regardless of whether or not they actually use signals, slots and properties, since failure to do so may lead certain functions to exhibit strange behavior.
>All Qt widgets inherit QObject. The convenience function isWidgetType() returns whether an object is actually a widget. It is much faster than qobject_cast<QWidget *>(obj) or obj->inherits(“QWidget”).
>Some QObject functions, e.g. children(), return a QObjectList. QObjectList is a typedef for QList<QObject *>.

**译文**

>QObject类是所有Qt对象的基类。
>
>QObject是Qt对象模型的核心。该模型的中心特性是一种非常强大的无缝对象通信机制，称为信号和插槽。可以使用connect（）将信号连接到插槽，并使用disconnect（）销毁连接。为了避免永不结束的通知循环，可以使用block signals（）临时阻止信号。受保护的函数connectNotify（）和disconnectNotify（）使跟踪连接成为可能。
>
>QObjects在对象树中组织自己。当您使用另一个对象作为父对象创建QObject时，该对象将自动将自己添加到父对象的children（）列表中。父对象拥有该对象的所有权；即，它将自动删除其析构函数中的子对象。可以使用findChild（）或findChildren（）按名称和可选类型查找对象。
>
>每个对象都有一个object name（），其类名可以通过相应的metaObject（）找到（请参见QMetaObject：：className（））。可以使用inherits（）函数确定对象的类是否继承QObject继承层次结构中的另一个类。
>
>当一个对象被删除时，它会发出一个destroyed（）信号。您可以捕捉此信号，以避免挂起对QObjects的引用。
>
>QObjects可以通过event（）接收事件并过滤其他对象的事件。有关详细信息，请参阅installEventFilter（）和eventFilter（）。可以重新实现便利处理程序childEvent（）来捕获子事件。
>
>最后但并非最不重要的一点是，QObject在Qt中提供了基本的计时器支持；有关计时器的高级支持，请参见QTimer。
>
>请注意，对于实现信号、插槽或属性的任何对象，Q_OBJECT宏都是必需的。您还需要在源文件上运行元对象编译器。我们强烈建议在QObject的所有子类中使用此宏，无论它们是否实际使用信号、插槽和属性，因为如果不这样做，可能会导致某些函数显示出奇怪的行为。
>
>所有Qt小部件都继承QObject。便利函数isWidgetType（）返回对象是否实际是小部件。它比qobject_cast<QWidget*>（obj）或obj->inherits（“QWidget”）快得多。
>
>一些QObject函数，例如children（），返回一个QObjectList。QObjectList是QList<QObject*>的typedef。


