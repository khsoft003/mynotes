



## 1. 创建第一个Qt程序

​	

### 	1.1 注意事项

​			项目名称及路径中不能有中文及空格。





### 	1.2 三个窗口类

​			QWidget、QMainWindow、QDialog





### 	1.3 第一个项目

#### 				1.3.1 main函数

~~~c++
    #include "widget.h"
    #include <QApplication>

    int main(int argc, char *argv[])
    {
        QApplication a(argc, argv);//应用程序对象，有且仅有一个
        Widget w;	//实例化窗口对象
        w.show();	//显示窗口
        return a.exec();	//让应用程序进入消息循环，代码阻塞到当前行
    }
~~~











#### 	1.3.2 myWidget.h

![image-20210205210918363](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210205210918363.png)







#### 	1.3.2 .pro文件

~~~c++
QT       += core gui   Qt包含的模块

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets  //大于4版本以上 包含 widget模块

TARGET = 01_FirstProject  //目标   生成的.exe程序的名称
TEMPLATE = app       	  //模板   应用程序模板  Application  


SOURCES += main.cpp\      //源文件
        mywidget.cpp

HEADERS  += mywidget.h    //头文件
	    
~~~



​	





#### 	1.3.3 命名规范

- ​	类名：首字母大写，单词和单词之间首字母大写
- ​	函数名：首字母小写，单词和单词之间首字母大写





#### 	1.3.4  快捷键

![image-20210205212029279](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210205212029279.png)









## Qt的基本模块

​		在Qt4版本中没有Qt Widgets模块，Qt5版本中，从Qt GUI模块中拆分出来Qt Widgets模块



![Qt5基本模块](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/Qt5基本模块.jpg)







## Qt中的对象树

​	当创建的对象在堆区的时候，如果指定的父亲是QObject派生下来的类或者QObject子类派生下来的类，那么可以不用管理释放的操作，因为对象会被放到对象树种。

​	一定程度上简化了内存回收机制。

![Qt中的对象树](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/Qt中的对象树.jpg)



## Qt中的坐标系

​	以左上角为原点（0,0），x向右增加，y向下增加。

![image-20210206094124761](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206094124761.png)



![image-20210415162427170](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210415162427170.png)



QWidget类中的坐标系统成员函数


x()：左上角的坐标（屏幕左上角是远点（0,0））

y()：左上角的坐标（屏幕左上角是远点（0,0））

width()：客户区的宽度

height()：客户区的高度

geometry.x()：不包括标题栏、边框的客户区

geometry.y()：不包括标题栏、边框的客户区

geometry.width()：客户区的宽度

geometry.height()：客户区的高度

frameGeometry.x()：左上角的坐标

frameGeometry.y()：左上角的坐标

frameGeometry.width()：窗口真正的宽度（包括边框和标题栏）

frameGeometry.height()：窗口真正的高度（包括边框和标题栏）
————————————————
版权声明：本文为CSDN博主「qq_24127015」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/qq_24127015/article/details/95115966



## 添加资源文件

1. 将图片拷贝到项目目录下
2. 右键项目-》添加新文件-》Qt-》Qt recourse File -》给资源文件命名
3. res生成 res.qrc
4. 右键 res.qrc -》open in editor
5. 添加前缀，添加文件
6. 使用 ":+前缀名+文件名"



## Qt的事件处理的5种级别

1. 重写控件的事件处理函数：如重写keyPressEvent()，mousePressEvent()和paintEvent()，这是最常用的事件处理方法，我们已经看到过很多这样的例子了。
2. 重写QObject::event()，在事件到达事件处理函数时处理它。在需要改变Tab键的惯用法时这样做。也可以处理那些没有特定事件处理函数的比较少见的事件类型（例如，QEvent::HoverEnter）。我们重写event()时，必须要调用基类的event()，由基类处理我们不需要处理的那些情况。
3. 给QObject对象安装事件过滤器：对象用installEventFilter()后，所有达到目标控件的事件都首先到达监视对象的eventFilter()函数。如果一个对象有多个事件过滤器，过滤器按顺序激活，先到达最近安装的监视对象，最后到达最先安装的监视对象。
4. 给QApplication安装事件过滤器，如果qApp（唯一的QApplication对象）安装了事件过滤器，程序中所有对象的事件都要送到eventFilter()函数中。这个方法在调试的时候非常有用，在处理非活动状态控件的鼠标事件时这个方法也很常用。
5. 继承QApplication，重写notify()。Qt调用QApplication::nofity()来发送事件。重写这个函数是在其他事件过滤器处理事件前得到所有事件的唯一方法。通常事件过滤器是最有用的，因为在同一时间，可以有任意数量的事件过滤器，但是notify()函数只有一个。

许多事件类型，包括鼠标，键盘事件，是能够传播的。如果事件在到达目标对象的途中或者由目标对象处理掉，事件处理的过程会重新开始，不同的是这时的目标对象是原目标对象的父控件。这样从父控件再到父控件，知道有控件处理这个事件或者到达了最顶级的那个控件。

下图显示了一个键盘事件在一个对话框中从子控件到父控件的传播过程。当用户敲击一个键盘，时间首先发送到有焦点的控件上（这个例子中是QCheckBox）。如果QCheckBox没有处理这个事件，Qt把事件发送到QGroupBox中，如果仍然没有处理，则最后发送到QDialog中。

![image-20210219172644984](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210219172644984.png)







## Qt中的鼠标事件



### enterEvent

```c++
void Demo::enterEvent(QEvent * e)
{
    qDebug() << "鼠标进入";
}
```



### leaveEvent 

```c++
void Demo::leaveEvent(QEvent * e)
{
    qDebug() << "鼠标离开";
}
```



### mousePressEvent

```c++
void Demo::mousePressEvent(QMouseEvent * ev)
{
    if(ev->button() == Qt::LeftButton){
         QString str = QString("鼠标按下：x=%1 y=%2 globalX=%3 globalY=%4").arg(e->x()).arg(e->y())
        			.arg(e->globalX()).arg(e->globalY());
    	qDebug() << str;   
    }
}
```



### mouseReleaseEvent

```c++
void Demo::mouseReleaseEvent(QMouseEvent * ev)
{
    if(ev->button() == Qt::LeftButton){
        QString str = QString("鼠标释放：x=%1 y=%2 globalX=%3 globalY=%4").arg(e->x()).arg(e->y())
                        .arg(e->globalX()).arg(e->globalY());
        qDebug() << str;
    }
}
```



### mouseMoveEvent

```c++
void Demo::mouseMoveEvent(QMouseEvent * ev)
{
    if(ev->buttons() & Qt::LeftButton){
        QString str = QString("鼠标移动：x=%1 y=%2 globalX=%3 globalY=%4").arg(e->x()).arg(e->y())
                        .arg(e->globalX()).arg(e->globalY());
        qDebug() << str;
    }
}
```







### 设置鼠标追踪

mouseMoveEvent默认在没有鼠标按键按下的情况下是不执行的，如果希望没有鼠标按键按下的情况下也执行，需要**设置鼠标追踪**

```c++
setMouseTracking(true);
```





### 设置鼠标穿透

##### 当前窗口及子控件均不响应鼠标事件

```c++
setAttribute(Qt::WA_TransparentForMouseEvents, true);
```



##### 当前窗口透明区域不响应鼠标事件

```c++
setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
setAttribute(Qt::WA_TranslucentBackground, true);
```



##### 自定义当前窗口区域响应鼠标事件

```c++
void QWidget::setMask(const QRegion &region)
```

注意：如果设置的区域很复杂，效果可能会很慢。

自行传递当前窗口的鼠标事件：

```c++
void mouseMoveEvent(QMouseEvent *event)
{
    transMouseEvents(event);
}

void mousePressEvent(QMouseEvent *event)
{
    transMouseEvents(event);
}

void mouseReleaseEvent(QMouseEvent *event)
{
    transMouseEvents(event);
}

void mouseDoubleClickEvent(QMouseEvent *event)
{
    transMouseEvents(event);
}

void transMouseEvents(QMouseEvent *event)
{
    if (this->parentWidget()) {    // 这里仅演示向父类窗口传递鼠标事件
	this->setAttribute(Qt::WA_TransparentForMouseEvents, true);

	QPoint pt = this->mapTo(this->parentWidget(), event->pos());
	QWidget *wid = this->parentWidget()->childAt(pt);
	if (wid) {
	    pt = wid->mapFrom(this->parentWidget(), pt);
	    QMouseEvent *mEvent = new QMouseEvent(event->type(), pt, event->button(), event->buttons(), event->modifiers());
	    QApplication::postEvent(wid, mEvent);
	}

	this->setAttribute(Qt::WA_TransparentForMouseEvents, false);
    }
}
```











## 信号和槽



~~~c++
	MyPushButton * myBtn = new MyPushButton;

	// 参数1：信号发送者	参数2：发送的信号（函数地址）	参数3：信号的接受者	参数4： 处理的槽函数
	connect(myBtn, &MyPushButton::clicked, this, &myWidget::close);
	connect(myBtn, &QPushButton::clicked, this, &QWidget::close);//调用父类函数，效果相同
~~~

![Qt信号和槽 ](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/Qt_xinhaohecao.png)











#### 注意事项

- ​		一个信号连接多个槽函数
- ​		多个信号可以连接同一个槽函数
- ​		**信号和槽函数的参数必须类型一一对应**
- ​		**信号参数个数可以多于槽函数的参数个数**







#### 	自定义信号

- ​		写到signals下；
- ​		返回值是void，只需要声明，不需要实现；
- ​		可以有参数，可以重载。

![image-20210206133439901](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206133439901.png)



#### 	自定义槽

- ​		自定义槽函数，早期Qt必须写到public slots下；
- ​		Qt5是可以写到public或全局下的；
- ​		可以有参数，可以发生重载；

![image-20210206133624002](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206133624002.png)



#### 	连接信号和槽

- ​		使用connect函数连接信号和槽
- ​		使用emit 关键字发送信号

![image-20210206134234714](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206134234714.png)



#### 信号和槽发生重载







##### 	信号重载

![image-20210206141458081](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206141458081.png)







##### 	槽函数重载

![image-20210206141606777](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206141606777.png)









##### 	用函数指针连接

![image-20210206141257445](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206141257445.png)



##### 	信号连接信号

![image-20210206145854628](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206145854628.png)



##### 	断开信号

​	![image-20210206150040396](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206150040396.png)









##### 	Qt4版本的连接方式

~~~c++
	connect(zt, SIGNAL(hungry()), st, SLOT(tret()));
~~~

- ​		优点：参数直观

- ​		缺点：编译时类型不做检测，参数不匹配时发现不了错误。

  ​		![image-20210206151538061](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206151538061.png)

  ​			上面代码编译不报错，运行时出错，如下图所示

![image-20210206151441086](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206151441086.png)





##### Lambda表达式

​	**注意：**早期Qt版本必须在pro文件中添加：CONFIG  += c++11

​		![image-20210206152353958](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206152353958.png)







###### 	值传递(推荐)

~~~c++
	[=](){
        
    };

	[myBtn](){ 
        myBtn->setText("123");
    };

~~~







###### 	引用传递

	    [&](){
	
	    };
	    
	    [&myBtn](){
			myBtn->setText("123");
	    };


###### 	返回值

​		->返回值类型

![image-20210206153820989](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210206153820989.png)





#### connect信号连接的几种写法

​	Qt 中的信号和槽应该是最熟悉不过的了，连接信号和槽的关键字 connect有五种连接类型，今天不是介绍这五种连接类型，而是简单的总结一下 connect 的几种新旧写法，其实在新版本中几种写法都能适用，看个人习惯吧。

##### 第一种

首先来看看老版本的 connect 写法，比较复杂些，需要将信号和槽进行明确的指定，包括形参。 
看一个示例：

为方便演示，先自定义一个 Button，然后定义两个重载的信号

```c++
class MyButton : public QWidget
{
    Q_OBJECT
public:
    explicit MyButton(QWidget *parent = nullptr);

signals:
    void sigClicked();
    void sigClicked(bool check);
};
```

那么在用这个 Button 的时候连接这两个信号，按照旧版本的写法，应该是这样：

```c++
connect(m_pBtn,SIGNAL(sigClicked()),this,SLOT(onClicked()));
connect(m_pBtn,SIGNAL(sigClicked(bool)),this,SLOT(onClicked(bool)));
```

这种写法比较麻烦，常常在用的时候缺少括号，不过该写法很明确，一眼就能看出来是将哪个信号连接到哪个槽。



##### 第二种

接着上面的示例，在 Qt5.0以后推出一种新的写法，如下：

```c++
connect(m_pBtn,&MyButton::sigClicked,this,&Widget::onClicked);
```

这种写法看起来很简洁，但是存在一些坑需要注意，这句写法如果用在上面的示例中，会报错下面的错误：

```bash
error: no matching member function for call to 'connect'  connect(m_pBtn,&MyButton::sigClicked,this,&Widget::onClicked);
    ^~~~~~~
```

这是因为我们自定义的 Button 中存在两个重载信号，然后用这种 connect 的方式会无法识别到底想要连接哪个信号。所以，如果信号是重载的话，需要用下面的写法来替换：

```c++
connect(m_pBtn, static_cast<void (MyButton::*)(bool)>(&MyButton::sigClicked), this, &Widget::onClicked);
```

问题又来了，如果我的onClicked槽也是重载的话，还是会报同样的错误。因为编译器不知道你想要真正连接哪个槽。所以这里建议，如果信号重载，可以用上面的方法来写，如果槽重载…还是用第一种方法来 connect 吧，比较保险，虽然比较麻烦点。





##### 第三种

最后来看一种最新的写法，忘记是在 Qt 的哪个版本推出的了，主要针对重载信号的连接做了调整，会更简单些： 
同样是上面的示例：

```c++
connect(m_pBtn, QOverload<bool>::of(&MyButton::sigClicked),this,&Widget::onClicked);
```

很显然这种写法相对于第二种会比较简单些，但依然不能连接到重载的槽函数，如果连接重载槽函数，还是会报之前的错误。





##### Lambda 函数写法

lambda函数的方式，如果槽函数中的内容比较简单的话，没必要再去单独定义一个槽来连接， 直接用Lambda 函数会更简单。 
来看一下示例：

```c++
connect(m_pBtn, QOverload<bool>::of(&MyButton::sigClicked),
          [=](bool check){
        /* do something.. */ 
    });
```

```c++
connect(m_pBtn, static_cast<void (MyButton::*)(bool)>(&MyButton::sigClicked), this, [=](bool check){
        //do something
    });
```





