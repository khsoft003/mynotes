## 界面控制



### 窗口设置

#### 图标

~~~c++
	//设置图标
    this->setWindowIcon(QPixmap(":/res/Coin0001.png"));
~~~



#### 标题

~~~c++
	//设置标题
    this->setWindowTitle("选择关卡");	
~~~



#### 固定窗口大小

~~~c++
	//设置窗口固定大小
    this->setFixedSize(320,588);
~~~



#### 设置窗口置顶

窗口在整个桌面中置顶：

```c++
Qt::WindowFlags m_flags = windowFlags();
setWindowFlags(m_flags | Qt::WindowStaysOnTopHint); // 在show()函数之前设置
```

窗口在程序窗口中置顶：

```c++
	this->setAttribute(Qt::WA_ShowModal, true);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
```







#### 设置窗口透明度

```c++
this->setWindowOpacity(0.3); //0~1，0：透明；1：不透明
```











### 工具栏

1. 创建工具栏

   ~~~c++
   	QToolBar * toolBar = new QToolBar(this);
       addToolBar(Qt::TopToolBarArea, toolBar);//设置工具栏到顶部
   ~~~

   

2. 设置工具栏

   ~~~c++
   	toolBar->setAllowedAreas(Qt::TopToolBarArea); 
       toolBar->setFloatable(false); //禁止浮动
   	toolBar->setMovable(false); //禁止移动
   ~~~

   

3. 给工具栏添加按钮

   ~~~c++
   	QAction * firstPageAction = new QAction("首页", this);
       toolBar->addAction(firstPageAction);
   ~~~

   

4. 给按钮添加执行代码

   ~~~c++
   	connect(firstPageAction, &QAction::triggered, this, [=](){
           qDebug() << " do something...";
       });
   ~~~

5. 工具栏添加分割线

   ~~~c++
   	toolBar->addSeparator();
   ~~~



​	6.工具栏添加控件

~~~c++
	QPushButton * btn = new QPushButton("aa", this);
	tooBar->addWidget(btn);
~~~



 7. 工具项添加图标

    ~~~c++
    firstPageAction->setIcon(QIcon("D:/image/first.png"));
    secondPageAction->setIcon(QIcon(":/image/first.png"));//使用资源文件中的图片
    ~~~

 8. 隐藏工具栏

    ```c
    ui->mainToolBar->setVisible(false);
    ```

    





### 菜单栏

 1. 创建菜单栏(**只能创建一个**)

    ~~~c++
    QMenuBar * bar = menuBar();
    setMenubar(bar);//将菜单添加到窗口
    ~~~

    

 2. 创建菜单

    ~~~c++
    QMenu * fileMenu = bar->addMenu("文件");
    QMenu * editMenu = bar->addMenu("编辑");
    ~~~

    

 3. 创建菜单项

    ~~~c++
    fileMenu->addAction("新建");
    fileMenu->addAction("打开");
    ~~~

    

 4. 添加分隔线

    ~~~c++
    fileMenu->addSeparator();
    ~~~




### 状态栏

 1. 创建状态栏

    ~~~c++
    QStatusBar * stBar = statusBar();
    setStatusBar(stBar);
    ~~~



 2. 添加控件

    ~~~c++
    QLabel * label = new QLabel("提示信息", this);
    stBar->addWidget(label);
    QLabel * label2 = new QLabel("右侧提示信息", this);
    stBar->addPermanentWidget(label2);
    ~~~

    

### 铆接部件

1. 创建铆接部件

   ~~~c++
   QDockWidget * dockWidget = new QDockWidget("浮动", this);
   addDockWidget(Qt::BottomDockWidgetArea, dockWidget);
   ~~~

2. 设置后期停靠

~~~c++
	dockWidget->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
~~~







### 中心部件

 1. 设置中心部件

    ~~~c++
    QTextEdit * edit = new QTextEdit(this);
    setCentralWidget(edit);
    ~~~



### 对话框

#### 创建对话框

 1. 创建模态对话框

    ~~~c++
    	//模态对话框 阻塞
    	QDialog dlg(this);
    	dlg.resize(200,100);
    	dlg.exec();
    	qDebug() << "模态对话框弹出了";
    ~~~

    



 2. 创建非模态对话框

    ~~~c++
	//非模态对话框
    	QDialog * dlg2 = new QDialog(this);
    	dlg2->resize(200,100);
    	dlg2->show();
    	dlg2->setAttribute(Qt::WA_DeleteOnClose); //设置关闭对话框后释放资源
    	qDebug() << "非模态对话框弹出了";
    ~~~
    
    ​	**Qt::WA_DeleteOnClose** ，窗口关闭时释放资源





#### QDialog exec()并获取结果值

> 要点：不要自己使用setResult(),而是使用[QDialog::accept()](http://qt-project.org/doc/qt-4.8/qdialog.html#accept)和[QDialog::reject()](http://qt-project.org/doc/qt-4.8/qdialog.html#reject).

需要在显示对话框之前添加此代码(假设它在对话框方法中)

```c++
QObject::connect(acceptButton, SIGNAL(clicked()), this, SLOT(accept()));
QObject::connect(rejectButton, SIGNAL(clicked()), this, SLOT(reject()));
```

在你的调用者对象中

```c++
void someInitFunctionOrConstructor(){
   QObject::connect(mydialog, SIGNAL(finished (int)), this, SLOT(dialogIsFinished(int)));
}

void dialogIsFinished(int result){ //this is a slot
   if(result == QDialog::Accepted){
       //do something
       return;
   }
   //do another thing
}

```







#### 标准对话框

##### 	MessageBox

![image-20210217173229169](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210217173229169.png)



![image-20210217173305825](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210217173305825.png)





~~~c++
	//消息对话框

	//错误对话框
	QMessageBox::critical(this, "critical", "错误");
	//信息对话框
	QMessageBox::information(this, "info", "信息");
	//提问对话框
	if(QMessageBox::Save == QMessageBox::question(this, "ques", "提问", QMessageBox::Save | QMessageBox::Cancel)){
        qDebug()<<"选择的是保存";
    }
	//警告对话框
	QMessageBox::warning(this, "warning", "警告");
~~~









##### QColorDialog

​	颜色对话框

~~~c++
	QColor color = QColorDialog::getColor(QColor(255,0,0));
	qDebug() << "r="<<color.red() << " g=" << color.green() << " b=" << color.blue();
~~~





##### QFileDialog

​	文件对话框

~~~c++
	QFileDialog::getOpenFileName(this, "打开文件", "/home/yan");
	QFileDialog::getOpenFileName(this, "打开文件", "/home/yan", "(*.txt)");
	QString str = QFileDialog::getOpenFileName(this, "打开文件", "/home/yan");
	qDebug() << str;
~~~



##### QFontDialog

​	字体对话框

~~~c++
	bool flag;
	QFont font = alog::getFont(&flag, QFont("华文彩云", 36));
	qDebug() << "字体：" << font.family << " 字号："<< font.pointSize() << " 粗体：" << font.bold(); <<" 斜体：" << font.italic();
~~~











## 事件

​	事件分发机制：

![image-20210222204928493](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210222204928493.png)

### event 拦截事件

​	事件原型：bool event(QEvent * e);

~~~c++
bool myLabel::event(QEvent *e)
{
    //如果是鼠标按下 ，在event事件分发中做拦截操作
    if(e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent * ev  = static_cast<QMouseEvent *>(e);
        QString str = QString( "Event函数中：：鼠标按下了 x = %1   y = %2  globalX = %3 globalY = %4 " ).arg(ev->x()).arg(ev->y()).arg(ev->globalX()).arg(ev->globalY());
        qDebug() << str;

        return true; //true代表用户自己处理这个事件，不向下分发
    }

    //其他事件 交给父类处理  默认处理
    return QLabel::event(e);
}
~~~





### eventFilter事件过滤器

​	通过事件过滤器可以在程序分发到event事件之前在做一次高级的拦截。



![image-20210223125252078](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210223125252078.png)



两个步骤：

 1. 给控件安装事件过滤器

    ~~~c++
    Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
    {
        ui->setupUi(this);
        //给label 安装事件过滤器
        // 步骤1  安装事件过滤器
        ui->label->installEventFilter(this);
    }
    ~~~

    

 2. 重新eventFilter事件

    ~~~c++
     // 步骤2  重写 eventfilter事件
    bool Widget::eventFilter(QObject * obj , QEvent * e)
    {
        if(obj == ui->label)
        {
            if(e->type() == QEvent::MouseButtonPress)
            {
                QMouseEvent * ev  = static_cast<QMouseEvent *>(e);
                QString str = QString( "事件过滤器中：：鼠标按下了 x = %1   y = %2  globalX = %3 globalY = %4 " ).arg(ev->x()).arg(ev->y()).arg(ev->globalX()).arg(ev->globalY());
                qDebug() << str;
                return true; //true代表用户自己处理这个事件，不向下分发
            }
        }
    
        //其他默认处理
        return QWidget::eventFilter(obj,e);
    }
    ~~~

    



###    paintEvent

~~~c++
void PDFWidget::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.translate(-mHorizontalScrollbar->value(), -mVerticalScrollbar->value());
    QBrush brush(Qt::gray);
    painter.setBrush(brush);
    painter.drawRect(0,0,this->width(), this->height());

    QPixmap pixmap = QPixmap::fromImage(*mImage);
    QPixmap fitpixmap = pixmap.scaled(mImage->width()*m_Ratio, mImage->height()*m_Ratio, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    int x = 10;
    int y = 20;
    painter.drawPixmap(QPoint(x,y),fitpixmap);

}
~~~

手动调用绘图事件：

~~~c++
	//如果要手动调用绘图事件 用update更新
	this->update();
~~~



###    resizeEvent

~~~c++
void PDFWidget::resizeEvent(QResizeEvent *e)
{
    //
}
~~~



###    showEvent

~~~c++
void PDFWidget::showEvent(QShowEvent *event)
{
    //
}
~~~



###    keyPressEvent

~~~c++
void PDFWidget::keyPressEvent(QKeyEvent * event)
{
    switch (event->key())
    {
    case Qt::Key_Plus:
    {

    }
        break;
    case Qt::Key_Minus:
    {

    }
        break;
    default:
        break;
    }
}
~~~



###    wheelEvent

~~~c++
void PDFWidget::wheelEvent(QWheelEvent *event)
{

    int numDegrees = event->delta() / 8;//滚动的角度，*8就是鼠标滚动的距离
    int numSteps = numDegrees / 15;//滚动的步数，*15就是鼠标滚动的角度

    if( numSteps < 0){
        
    }else{
        
    }

    event->accept();      //接收该事件
}
~~~



###    mouseMoveEvent

~~~c++
void PDFWidget::mouseMoveEvent(QMouseEvent* event)
{
    QPoint mousepos = event->pos();

    //在坐标（0 ~ width，0 ~ height）范围内改变鼠标形状
    if(mousepos.rx() > 0
       && mousepos.rx() < this->width()
       && mousepos.ry() > 0
       && mousepos.ry() < this->height())
    {
        this->setCursor(Qt::OpenHandCursor);
    }
    else
    {
        this->setCursor(Qt::ArrowCursor);      //范围之外变回原来形状
    }
}
~~~

判断鼠标左键是否按下：

~~~c++
// QMouseEvent* e;
if (e->type() == QEvent::MouseMove && (e->buttons() & Qt::LeftButton )) 
{
    // TODO
}
 
// 或者
if (e->type() == QEvent::MouseMove && (e->buttons() == Qt::LeftButton )) 
{
    // TODO
}
 
// 如果要判断鼠标多个键按下
// 示例： 鼠标移动的同时， 鼠标左键和中键被按下
if (e->type() == QEvent::MouseMove && (e->buttons() == (Qt::LeftButton | Qt::MiddleButton ))) 
{
    // TODO
}
~~~

**设置鼠标追踪**：无需鼠标按键按下，就可以捕获鼠标移动事件，缺点：有点耗费资源

~~~c++
	setMouseTracking(true);
~~~





###    mousePressEvent

~~~c++
void PDFWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        int x = event->x();
        int y = event->y();
    }
}
~~~



###    mouseReleaseEvent

~~~c++
void PDFWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {

    }
}
~~~



### timerEvent

~~~c++
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    //启动定时器
    startTimer(1000); //参数1  间隔  单位 毫秒
}
void Widget::timerEvent(QTimerEvent * ev)
{
    //label  每隔1秒 +1
    static int num = 1;
    ui->label->setText(  QString::number(num++));
}

~~~

启动两个定时器：

~~~c++
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //启动定时器,startTimer的返回值是唯一标识
    id1 = startTimer(1000); //参数1  间隔  单位 毫秒
    id2 = startTimer(2000);
}
void Widget::timerEvent(QTimerEvent * ev)
{
    if(ev->timerId() == id1)
    {
        static int num = 1;
        //label2 每隔1秒+1
        ui->label_2->setText(  QString::number(num++));
    }
    if(ev->timerId() == id2)
    {
    //label3  每隔2秒 +1
    static int num2 = 1;
    ui->label_3->setText(  QString::number(num2++));
    }
}
~~~

推荐使用QTimer。







### 自定义事件

#### 1、自定义事件步骤

有时为了开发需要，我们希望自定义事件来完成某种目的。实现自定义事件的步骤如下：

- 继承QEvent。
- 定义事件类型（取值在QEvent::User和QEvent::MaxUser之间，建议使用registerEventType()函数自动创建一个全局唯一一个的事件类型，防止重复）
- 使用sendEvent()或postEvent()发送事件。
- 需要在自定义类中重写QObject::event()方法处理自定义事件。
  

#### 2、自定义事件类

自定义的事件类代码如下：

头文件代码：

```c++
// -------------------------- 自定义事件 --------------------------------
class CustomerEvent : public QEvent
{
public:
    CustomerEvent(QString valueString = "");
    ~CustomerEvent();

    static Type eventType();
    QString getValueString(void);

private:
    static Type m_EventType;
    QString m_String;
};
```



源文件代码：

```c++
QEvent::Type CustomerEvent::m_EventType = QEvent::None;
CustomerEvent::CustomerEvent(QString valueString) : QEvent(eventType())
{
    m_String = valueString;
}

CustomerEvent::~CustomerEvent()
{

}

QEvent::Type CustomerEvent::eventType()
{
    // 创建事件Type
    if (m_EventType == QEvent::None)
        m_EventType = (QEvent::Type)QEvent::registerEventType();

    return m_EventType;
}

QString CustomerEvent::getValueString(void)
{
    return m_String;
}
```

这里使用了**CustomerEvent::eventType()**函数为事件创建了类型。

#### 3、发送事件

发送事件有两种方法，一种是使用sendEvent()函数，一种是使用postEvent()函数。

##### （1）sendEvent方法

> sendEvent()方法是阻塞的，它发送个对象事件，等待接受对象处理结束后返回。函数原型如下： 
> static bool QCoreApplication::sendEvent(QObject *receiver, QEvent *event); 
> 参数： receiver为接受事件的对象， event为传递的事件指针 
> 返回值：若事件被处理返回true， 否则返回false。 
> 说明： 这个函数需要手动去释放事件的内存。

##### （2）postEvent方法

> postEvent()为异步的方法，它向事件队列中登记一个指定接收者的事件，然后返回。函数原型如下： 
> static void QCoreApplication::postEvent(QObject *receiver, QEvent *event, int priority = Qt::NormalEventPriority); 
> 参数：receiver为接受事件的对象， event为传递的事件指针， priority为优先级 
> 说明：这个函数不需要手动去释放事件的内存，有Qt框架自动释放。

使用PostEvent和SendEvent的例子如下： 
头文件

```c++
class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

protected:
    virtual bool event(QEvent *event) override;
};
```

这里定义了一个Widget, 其中event()函数后面处理自定义事件时会用到。

源文件

```c++
Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    // 创建按钮并布局
    QPushButton *button = new QPushButton("CreateEvent", this);
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(button);
    this->setGeometry(100, 100, 800, 600);

    // 建立连接, 发送信号
    QObject::connect(button, &QPushButton::clicked, [=](void)->void{
        // 使用PostEvent方式发送
        CustomerEvent *customerEvent = new CustomerEvent("PostCustomerEvent");
        QCoreApplication::postEvent(this, customerEvent);

        // 使用SendEvent方式发送
        CustomerEvent *sendCustomerEvent = new CustomerEvent("SendCustomerEvent");
        bool result = QCoreApplication::sendEvent(this, sendCustomerEvent);
        qDebug() << "The Dispose Result Is " << result;
        delete sendCustomerEvent;
    });
}

Widget::~Widget()
{

}
```

这里创建了一个按钮，当按钮按下时发送自定义的事件。

#### 4、事件处理

通过重写QObject::event()的方式处理自定义事件。代码如下：

```c++
bool Widget::event(QEvent *event)
{
    if (event->type() == CustomerEvent::eventType())
    {
        CustomerEvent *customerEvent = dynamic_cast<CustomerEvent*>(event);
        qDebug() << customerEvent->getValueString();
        return true;
    }
    return QWidget::event(event);
}
```

函数的返回true表示已经处理了此事件，否则返回false。

整体程序的运行结果为： 
“SendCustomerEvent” 
The Dispose Result Is true 
“PostCustomerEvent” 
可见，函数先处理了sendEvent()函数的事件， 等事件循环运行时分发给当前对象后处理。
————————————————
版权声明：本文为CSDN博主「douzhq」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/douzhq/article/details/80209601



### on_pushButton_clicked()的用法

1、在UI文件里加入按钮，objectName 设置为pushButtonA


2、在引用的头文件（比如MainWindow.h）里加入按钮曹的声明： 

```c++
public slots: 
    void on_pushButtonA_clicked(); 
```

3、在调用处使用，（比如MainWindow.cpp）。 

```void MainWindow::on_pushButtonConnect_clicked() 
void MainWindow::on_pushButtonA_clicked() 
{ 
    qDebug() << "pushButtonA clicked"; 
}
```







## 内置对象



### QWidget

###### 	设置样式

~~~c++
	this->setStyleSheet("background-color: rgb(200,200,200)");
~~~






###### 	设置窗口大小

~~~c++
	resize(1000, 800);
~~~






###### 	设置固定窗口大小

~~~c++
	setFixedSize(1000, 800);
~~~











### 绘图设备

​	**绘图设备是指继承QPainterDevice的子类。**Qt一共提供了四个这样的类，分别是QPixmap、QBitmap、QImage和 QPicture。

-  QPixmap专门为图像在屏幕上的显示做了优化
-  QBitmap是QPixmap的一个子类，它的色深限定为1，可以使用 QPixmap的isQBitmap()函数来确定这个QPixmap是不是一个QBitmap。
-  QImage专门为图像的像素级访问做了优化。 
-  QPicture则可以记录和重现QPainter的各条命令。

#### QImage

~~~c++
    QImage * img = new QImage;
    img->load(":/images/1.png");
~~~

​	访问和修改图片中的像素：

~~~c++
    QPainter painter(this);

    //利用QImage 对像素进行修改
    QImage img;
    img.load(":/Image/Luffy.png");

    //修改像素点
    for(int i = 50 ;i < 100 ; i++)
    {
        for(int j = 50 ; j < 100;j++)
        {
            QRgb value = qRgb(255,0,0);
            img.setPixel(i,j,value);
        }
    }

    painter.drawImage(0,0,img);
~~~





#### QPixmap

​	加载图片

~~~c++
    QPixmap pixmap = new QPixmap;
    pixmap.load("/home/tom/1.png");
~~~

​	绘制图片保存到磁盘：

~~~c++
    //Pixmap绘图设备 专门为平台做了显示的优化
    QPixmap pix(300,300);
    //填充颜色
    pix.fill(Qt::white);

    //声明画家
    QPainter painter(&pix);
    painter.setPen(QPen(Qt::green));
    painter.drawEllipse(QPoint(150,150) , 100,100);
    //保存
    pix.save("E:\\pix.png");
~~~







#### QPicture

​	保存绘制的内容：

~~~c++
    //QPicture 绘图设备  可以记录和重现 绘图指令
    QPicture pic;
    QPainter painter;
    painter.begin(&pic);  //开始往pic上画
    painter.setPen(QPen(Qt::cyan));
    painter.drawEllipse(QPoint(150,150) , 100,100);
    painter.end(); //结束画画

    //保存到磁盘
    pic.save("E:\\pic.zt");
~~~

​	加载绘制的内容：

~~~c++
    QPainter painter(this);
    //重现QPicture的绘图指令
    QPicture pic;
    pic.load("E:\\pic.zt");
    painter.drawPicture(0,0,pic);
~~~





### QAction

~~~c++
	QToolBar * toolBar = new QToolBar(this);
    addToolBar(Qt::TopToolBarArea, toolBar);
    QAction * firstPageAction = new QAction("首页", this);
    toolBar->addAction(firstPageAction);
~~~





### QScrollBar

动态创建的QScrollBar只有**在showEvent中获取到的高宽**才是准确的

~~~c++
    QScrollBar * mHorizontalScrollbar = new QScrollBar(this);
    mHorizontalScrollbar->setOrientation(Qt::Horizontal);//设置横向
    connect(mHorizontalScrollbar, &QScrollBar::valueChanged, this, [=](int value){
        //
    });
~~~



### QLabel

~~~c++
	QLabel* mLabel = new QLabel(this);
    mLabel->setGeometry(mLeft,mTop,mWidth, mHeight);

    QImage *img=new QImage; 
    img->load(":/images/1.png"); 
    mLabel->setPixmap(QPixmap::fromImage(*img));
    delete(img);
~~~

设置多行显示：

```c++
QLabel lbl("long long string"); 
lbl.setWordWrap(true); 
```





### QTime

~~~c++
    QTime time;
    // do something ..

    // 计算程序执行时间，打印出来
    qDebug() << time.elapsed();
~~~

转QString

```c++
QTime::currentTime().toString("hh:mm:ss");
```







### QTimer

只执行一次：

~~~c++
	QTimer::singleShot(100,this,[=](){
        qDebug() << "只执行一次";
    });
~~~



每隔一段时间执行一次：

~~~c++
	QTimer * timer = new QTimer(this);
    //启动定时器
    timer->start(500);

    connect(timer,&QTimer::timeout,[=](){
        static int num = 1;
        //label4 每隔0.5秒+1
        ui->label_4->setText(QString::number(num++));
    });
~~~



停止定时器：

~~~c++
	//点击暂停按钮 实现停止定时器
    connect(ui->btn,&QPushButton::clicked,[=](){
        timer->stop();
    });

~~~







### QPainter

​		Qt 的绘图系统允许使用相同的 API 在屏幕和其它打印设备上进行绘制。整个绘图系统基于QPainter，QPainterDevice和QPaintEngine三个类。

- **QPainter**用来执行绘制的操作；
- **QPaintDevice**是一个二维空间的抽象，这个二维空间允许QPainter在其上面进行绘制，也就是QPainter工作的空间；
- **QPaintEngine**提供了画笔（QPainter）在不同的设备上进行绘制的统一的接口。

![img](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/clip_image001.png)

~~~c++
    void PaintedWidget::paintEvent(QPaintEvent *)
    {
        //    //设置画笔
        //    QPen pen(QColor(255,0,0));
        //    //设置画笔宽度
        //    pen.setWidth(3);
        //    //设置画笔风格
        //    pen.setStyle(Qt::DotLine);
        //    //让画家 使用这个笔
        //    painter.setPen(pen);
        
        QPainter painter(this);
        painter.drawLine(80, 100, 650, 500);
        painter.setPen(Qt::red);
        painter.drawRect(10, 10, 100, 400);
        painter.setPen(QPen(Qt::green, 5));
        painter.setBrush(Qt::blue);
        painter.drawEllipse(50, 150, 400, 200);
        
        painter.drawText(QRect(10,200,150,50) , "好好学习，天天向上");
        
            //////////////////////////////高级设置 ///////////////////////////////

        //    QPainter painter(this);
        //    painter.drawEllipse(QPoint(100,50) , 50,50);
        //    //设置 抗锯齿能力  效率较低
        //    painter.setRenderHint(QPainter::Antialiasing);
        //    painter.drawEllipse(QPoint(200,50) , 50,50);

        //    //移动画家
        //    painter.translate(100,0);

        //    //保存画家状态
        //    painter.save();

        //    painter.drawRect(QRect(20,20,50,50));

        //    painter.translate(100,0);

        //    //还原画家保存状态
        //    painter.restore();

        //    painter.drawRect(QRect(20,20,50,50));

        /////////////////////////////////利用画家 画资源图片 ///////////////////
         QPainter painter(this);
         QPixmap pix = QPixmap(":/Image/Luffy.png");

        //如果超出屏幕 从0开始
         if(posX >= this->width())
         {
             posX = -pix.width();
         }

         painter.drawPixmap(posX,0,pix);
    
    }
~~~







### QPrinter

​	Qt5中将Qprinter、QPrintDialog等类归入到了printsupport模块中，需要在工程文件（.pro文件）中加入QT+=printsupport，否则编译会出错。



##### 打印图片

```c++
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPrinter>
#include <QPrintDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QPainter>


void MainWindow::on_pushButton_clicked()
{
    qDebug() << "1112";

    QPixmap pixmap;
    pixmap.load(":/images/1.png");

    QPrinter printer;
    printer.setPageSize(QPrinter::A4);

    QPrintDialog dlg(&printer, this);
    // 如果在对话框中按下了打印按钮，则执行打印操作
    if (dlg.exec() == QDialog::Accepted)
    {
        QPainter painter;
        painter.begin(&printer);

        QRect rect = painter.viewport();
        int x = rect.width() / pixmap.width();
        int y = rect.height() / pixmap.height();

        painter.scale(x, y);
        painter.drawPixmap(0, 0 , pixmap);
        painter.end();
    }

}
```



##### 打印文本

```c++
void MainWindow::doPrint()
{
    // 创建打印机对象
    QPrinter printer;
    // 创建打印对话框
    QString printerName = printer.printerName();
    if( printerName.size() == 0)
        return;
    QPrintDialog dlg(&printer, this);
    //如果编辑器中有选中区域，则打印选中区域
    if (ui->textEdit->textCursor().hasSelection())
        dlg.addEnabledOption(QAbstractPrintDialog::PrintSelection);
    // 如果在对话框中按下了打印按钮，则执行打印操作
    if (dlg.exec() == QDialog::Accepted)
    {
       ui->textEdit->print(&printer);
    }
}
```













### QFont

~~~c++
	fitWidthAction->setFont(QFont("宋体",12));
    realSizeAction->setFont(QFont("宋体",12,QFont::Bold));
~~~





### QVector和QtAlgorithms

~~~c++
#include <QCoreApplication>
#include <QVector>
#include <QtAlgorithms>
#include <QDebug>


void myPrint(QVector<int> &v)
{
    for(QVector<int>::iterator it=v.begin(); it!=v.end();it++){
        qDebug() << *it ;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVector<int> v;
    v.push_back(50);
    v.push_back(20);
    v.push_back(30);
    v.push_back(40);

    qSort(v.begin(),v.end());

    myPrint(v);

    return a.exec();
}

~~~





### QSound

~~~c++
QSound *startSound = new QSound(":/res/TapButtonSound.wav",this);
startSound->play(); //播放音效
~~~





### QProcess 

#### 堵塞运行

```c++
QProcess process;
process.execute( "sub.exe", params ); 
process.waitForFinished();
```





#### 异步运行

```c++
QProcess process;
process.start( "sub.exe", params ); 
```







#### 设置临时环境变量

```sh
QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
//env.insert("LD_LIBRARY_PATH", QCoreApplication::applicationDirPath());此代码不起作用，改用qputenv函数
qputenv("LD_LIBRARY_PATH", QCoreApplication::applicationDirPath().toStdString().c_str());
QProcess process;
process.setProcessEnvironment(env);
process.execute(QCoreApplication::applicationDirPath() + QDir::separator() + "zspdf", QStringList(data));
```











### 文件操作



#### QFile

​	打开并读取文本文件：

~~~c++
        QString path = QFileDialog::getOpenFileName(this,"打开文件","C:\\Users\\zhangtao\\Desktop");
        //将路径放入到lineEdit中
        ui->lineEdit->setText(path);

        //编码格式类
        //QTextCodec * codec = QTextCodec::codecForName("gbk");

        //读取内容 放入到 textEdit中
        // QFile默认支持的格式是 utf-8
        QFile file(path); //参数就是读取文件的路径
        //设置打开方式
        file.open(QIODevice::ReadOnly);

        //QByteArray array = file.readAll();

        QByteArray array;
        while( !file.atEnd())
        {
            array += file.readLine(); //按行读
        }

        //将读取到的数据 放入textEdit中
        ui->textEdit->setText(array);
        //ui->textEdit->setText( codec->toUnicode(array)  );

        //对文件对象进行关闭
        file.close();
~~~



​	写入文本文件：

~~~c++
		//进行写文件
        file.open(QIODevice::Append); //用追加方式进行写
        file.write("啊啊啊啊啊");
        file.close();
~~~



判断文件是否存在

```c++
QString fileName = "/root/1.txt";
QFile::exists(fileName);
```



删除文件

```c++
QString fileName = "/root/1.txt";
QFile::remove(fileName);
```





#### QFileInfo

~~~c++
//QFileInfo 文件信息类
QFileInfo info(path);
qDebug() << "大小：" << info.size() << " 后缀：" << info.suffix() << " 文件名："<<info.fileName() << " 路径："<< info.filePath();
qDebug() << "创建日期：" << info.created().toString("yyyy/MM/dd hh:mm:ss");
qDebug() << "最后修改日期："<<info.lastModified().toString("yyyy-MM-dd hh:mm:ss");
~~~





## 类型转换



#### QImage与QPixmap

~~~c++
    QImage * img = new QImage;
    img->load(":/images/1.png");
    // QImage -> QPixmap
    QPixmap pixmap = QPixmap::fromImage(*img); 
    // QPixmap -> QImage
    *img = pixmap.toImage();
~~~

   



#### QString与char *

~~~c++
	QString foodName = "宫保鸡丁";
	//QString -> char * 先转成 QByteArray  ( .toUtf8() ) 再转char * ( .data() )
    qDebug() << "请老师吃饭，老师要吃:" << foodName.toUtf8().data() ;
~~~

- foodName转char *之前输出：请老师吃饭，老师要吃:"宫保鸡丁"
- foodName转char *之后输出：请老师吃饭，老师要吃:宫保鸡丁





#### QString转UTF8的16进制字符串

```c++
	QString m  = "北京";
    QString mm = m.toUtf8().toHex().toUpper(); 
    qDebug() << mm;//"E58C97E4BAAC"
```





#### UTF8的16进制字符串转QString

```c++
	QByteArray hex = QByteArray::fromHex("E58C97E4BAAC");
    qDebug() << QString(hex); //北京
```





#### QString转UTF8的QByteArray

```c++
	QTextCodec *utf8 = QTextCodec::codecForName("utf-8");
    QByteArray encoded = utf8->fromUnicode(QString::fromUtf8("北京"));
    qDebug() << encoded;"\xE5\x8C\x97\xE4\xBA\xAC"
```





#### UTF8的QByteArray转QString

```c++
	qDebug() << QString(encoded.toStdString().data());
```





#### QString转QByteArray

```c++
QString str("北京");
QByteArray by  = str.toLocal8Bit();
```



#### QByteArray转QString

```c++
QByteArray baData;
QString str = QString(baData);
```



#### QString转百分号%编码

```c++
	QString m  = "北京";
	QByteArray mba = m.toUtf8().toPercentEncoding();
    qDebug() << mba; //"%E5%8C%97%E4%BA%AC"
```





#### 百分号%编码转QString

```c++
QString m  = "北京";
QByteArray mba = m.toUtf8().toPercentEncoding();	
qDebug() << QString(QByteArray::fromPercentEncoding(mba)); //"北京"
```





#### int 转 QString

```c++
	QString::number(18);
```





## 编码解码



#### QString的Base64编码

```c++
QString encodeStr = QString(QString("颁发").toLocal8Bit().toBase64()); //6aKB5Y+R
```



#### QString的Base64解码

```c++
QString encodeStr("6aKB5Y+R");//颁发
QString decodeStr = QString(QByteArray::fromBase64(encodeStr.toLocal8Bit())); 
```

```c++
QString decodeBase64(QString base64String)
{
    return QString(QByteArray::fromBase64(base64String.toLocal8Bit()));
}
```







#### QByteArray的Base64编码

- 接口：

```cpp
QByteArray QByteArray::toBase64() const
QByteArray QByteArray::toBase64(QByteArray::Base64Options options) const
```

- 示例：

```cpp
QByteArray text("Hello world");
text.toBase64(); 
/* 输出: SGVsbG8gd29ybGQ= */
```







#### QByteArray的Base64解码

- 接口：

```cpp
[static] QByteArray QByteArray::fromBase64(const QByteArray &base64)
[static] QByteArray QByteArray::fromBase64(const QByteArray &base64, 
                                           QByteArray::Base64Options options)
```

- 示例：

```cpp
QByteArray::fromBase64("SGVsbG8gd29ybGQ="); 
/* 输出: Hello world */
```





## 其它



### 图像相关

#### 图像旋转

~~~c++
void MyWidget::rotateImage(int angle)
{
    QMatrix matrix;
    matrix.rotate(angle);

    QPixmap pixmap = QPixmap::fromImage(*mImage);
    QPixmap pixmapRotate = pixmap.transformed(matrix, Qt::SmoothTransformation);
    *mImage = pixmapRotate.toImage();
}
~~~



#### 图像缩放

~~~c++
QPixmap pixmap = QPixmap::fromImage(*mImage);
QPixmap fitpixmap = pixmap.scaled(w, h, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
~~~





### 字符串相关

#### 格式化字符串

~~~c++
	QString str = QString("%1 xxxx %2").arg(111).arg(222);
~~~



#### 截取指定位置字符

```c++
QString str = "AT+LOC+LOCATION: 115.850441,33.004833";
QString s = str.mid(6); //"+LOCATION: 115.850441,33.004833"  
QString s = str.mid(6, 9); //"+LOCATION"
```



#### 以某个字符切割字符串

函数返回值为切割后的字符串

```c++
QString QString::section ( QChar sep, int start, int end = -1, SectionFlags flags = SectionDefault ) const
```

以下为Qt 助手给我们提供的例子，sep表示用来切割的字符，start表示开始切割的位置，end表示切割的结束位置，flag参数可以用来影响函数的行为的某些方面,例如是否区分大小写,是否跳过空字段和如何处理前导和尾随分隔符。**结果返回开始切割那个字符到结束切割的那个字符之后的那个字符串。**

```c++
QString str;
QString csv = "forename,middlename,surname,phone";
QString path = "/usr/local/bin/myapp"; // First field is empty
QString::SectionFlag flag = QString::SectionSkipEmpty;
str = csv.section(',', 2, 2);   // str == "surname"
str = path.section('/', 3, 4);  // str == "bin/myapp"
str = path.section('/', 3, 3, flag); // str == "myapp"
```









### 文件相关



#### 文件拷贝

```c++
bool CopyFileToPath(QString sourceDir, QString toDir, bool coverFileExist)
{
    /*-----sourceDir目标文件路径 如"C:/text.txt"
    *-----toDir目标文件复制的位置 如"D:/text.txt" 表示将c盘根目录下的tetx.txt拷到D盘根目录下以text.txt命名
    *-----如果D盘根目录已经存在text.txt 判断coverFileIfExist 如果为true则覆盖否则退出*/
    toDir.replace("\\", "/");
    if (sourceDir == toDir){   //源目录与目标目录相同，直接返回拷贝不成功
        return true;
    }
    if (!QFile::exists(sourceDir)){  //源目录不存在
        qDebug() << "拷贝失败 源目录不存在";
        return false;
    }
    QDir *createFile = new QDir;
    bool exist = createFile->exists(toDir);
    if (exist){   //如果目标路径中存在源文件 直接覆盖
        if (coverFileExist){
            createFile->remove(toDir);
        }
    }
    if (!QFile::copy(sourceDir, toDir)){
        qDebug() << "copy fail";
        return false;
    }
    qDebug() << "copy success!";
    return true;
}
```



#### QString写入到文件

```c++

 	QString response = "123456";
    QByteArray resData = QByteArray::fromBase64(response.toLocal8Bit());

    QFile file("/home/yan/downloads/tmp1.png");
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out.writeRawData(resData.data(),resData.length());
    file.close();
```

```c++
QFile file("/home/yan/downloads/tmp1.tmp");
file.open(QIODevice::WriteOnly);
file.write(htmlStr.toLocal8Bit());
file.close();
```







#### 文件局部编辑

```c++
QString signContent = "123456";
QFile file("/home/yan/downloads/test_edit.pdf");
file.open(QIODevice::ReadWrite);//必须以此模式打开
file.seek(456);
QDataStream out(&file);
out.writeRawData(signContent.toUtf8().data(), signContent.length());
file.flush();
file.close();
```





### 其他

#### 读取qrc资源文件里的txt文件

```c++
// 指定文件：
QFile inputFile(":/input.txt");
// 只读打开：
inputFile.open(QIODevice::ReadOnly);
// 文本流：
QTextStream in(&inputFile);
// 将文本流读取到字符串中：
QString line = in.readAll();
// 关闭文本流：
inputFile.close();
```



```c
QFile f(":/html/parent.htm");
f.open(QIODevice::ReadOnly|QIODevice::Text);
QString parentHtml(f.readAll());
qDebug() << "parentHtml:" << parentHtml;
f.close();
```





#### 生成UUID

```c++
#include <QUuid>
 
QUuid id = QUuid::createUuid();
QString strId = id.toString(); //"{caf958c6-a2aa-4a9e-8980-2b560cc4fa90}"
```





#### getStringByName

```c++
QString getStringByName(QString sSource, QString sName)
{
    QString source(";" + sSource);
    if(source.indexOf(";"+sName+"=")<0) return "";

    QString subStr = source.mid(source.indexOf(";"+sName+"="));
    subStr = subStr.mid(1);
    int start = subStr.indexOf('=')+1;
    int end = subStr.indexOf(';');
    return subStr.mid(start, end-start );

}
```





#### widget构造函数传参



Qt中带自定义的构造函数事实上和C++的构造函数一样，但又有些不同。

例如：

```c++
class DataTerminal : public QMainWindow
{
  Q_OBJECT

public:
  explicit DataTerminal(QString username,QWidget *parent = 0);//构造函数有默认值的要放后面

private：

QString name;
}
```



构造函数：

```c++
DataTerminal::DataTerminal(QString username，QWidget *parent ) :
  QMainWindow(parent),name（username），
  ui(new Ui::DataTerminal)
{
  ui->setupUi(this);
}
```







#### Qt 取整函数

- qCeil 向上取整
- qFloor 向下取整
- qRound 四舍五入

#### 只运行一个程序实例

利用 QSharedMemory。程序创建时在内存中开辟一块内存，在运行时根据内存a是否已经建立判断程序是不是唯一运行。

```c++
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
    // 在linux / unix 程序异常结束共享内存不会回收
    // 在这里需要提供释放内存的接口，就是在程序运行的时候如果有这段内存 先清除掉
    QSharedMemory nix_fix_shared_memory("SingleApp");
    if (nix_fix_shared_memory.attach()){
        nix_fix_shared_memory.detach();
    }
    static QSharedMemory *shareMem = new QSharedMemory("SingleApp");
    // 只能运行一个实例
    if (!shareMem->create(1)){
        qApp->quit();
        return -1;
    }

    MainWindow w;
    w.show();

    return a.exec();
}
```

参考链接：https://blog.csdn.net/y396397735/article/details/80814497







#### 弹出自定义窗口

注意new创建form时，使用空参数，不要传递this

```c++
    QTimer::singleShot(1000,this,[=](){
        Form* form = new Form();
        form->resize(200,200);
        form->move(100,100);
        form->show();
        qDebug() << "form show";
    });
```











#### 打印对话框

```sh
void MainWindow::showPrintDialog()
{
    QPrintDialog printDlg(this);

    if (printDlg.exec() == QDialog::Accepted){
        QString jobName;
        QString fileName;
        if(nullptr == m_PdfCtrl){
            jobName = m_FileName.mid(m_FileName.lastIndexOf(QDir::separator())+1);
            fileName = m_FileName;
        }else{
            jobName = m_PdfCtrl->FileName;
            fileName = m_TmpFileName;
        }


        QString printCmdTxt = "lpr -C "+ jobName +" -P "+ printDlg.printer()->printerName()+ " -# "+ QString::number( printDlg.printer()->copyCount() );

        if(printDlg.printer()->fromPage() > 0 ){
            printCmdTxt += " -o page-ranges=" +   QString::number(printDlg.printer()->fromPage())
                            + "-" + QString::number(printDlg.printer()->toPage());
        }

        if(2 == printDlg.printer()->duplex()){
            printCmdTxt += " -o sides=two-sided-long-edge ";
        }else if(3 == printDlg.printer()->duplex()){
            printCmdTxt += " -o sides=two-sided-short-edge ";
        }


        printCmdTxt += "  "+fileName;
        qDebug() << printCmdTxt;

        QProcess process;
        process.execute(printCmdTxt);
    }
}
```











### XML操作



#### 读XML

```c++
	QDomDocument doc;
    doc.setContent(hiddenFieldDecode(response),nullptr,nullptr,nullptr);
    QDomElement root = doc.documentElement();
    qDebug() << root.nodeName();
    QDomNode child = root.firstChild();
    qDebug() << child.toElement().nodeName();

    QDomNode zoomSealListNode = child;
    QDomNode rootCertListNode = child.nextSibling();

    QDomNode zoomSealNode = zoomSealListNode.firstChild();
    ZoomSeal* zoomSeal = new ZoomSeal();
    for(int i=0; i < zoomSealNode.childNodes().count(); i++){
 		qDebug()<< zoomSealNode.childNodes().item(i).attributes().namedItem("Name").nodeValue();
        if("ID" == zoomSealNode.childNodes().item(i).attributes().namedItem("Name").nodeValue()){
            zoomSeal->ID = zoomSealNode.childNodes().item(i).firstChild().nodeValue();
        }
    }
```





#### 写XML

```c++
	QFile sif("sealInfo.txt");
    sif.open(QIODevice::WriteOnly);
    QTextStream sifStm(&sif);

    QFile f(":/seal_info.xml");
    f.open(QIODevice::ReadOnly);
    QTextStream txtStm(&f);

    QDomDocument doc;
    doc.setContent(txtStm.readAll(),nullptr,nullptr,nullptr);
    QDomElement root = doc.documentElement();
    qDebug() << root.nodeName();
    QDomNode child = root.firstChild();
    qDebug() << child.toElement().nodeName();
	//   child.appendChild(doc.createTextNode("11111"));
	root.elementsByTagName("SignTime").item(0).appendChild(doc.createTextNode("123"));
    doc.save(sifStm,4);

    f.close();
    sif.close();
```

