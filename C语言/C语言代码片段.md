

## 字符串相关

### 删除字符串中空格

```c
int trimSpace(char *inbuf, char *outbuf)
{
    char *in = inbuf;
    char *out = outbuf;
    int ret = 0;
    int inLen = strlen(in);
    if (!inbuf || !outbuf)
    {
        ret = -1;
        printf("func trimSpace(char *inbuf, char *outbuf) err: ret = %d->function parameter is null.\n", ret);
        return ret;
    }
    else
    {
        ret = 1;
        for (int i = 0; i < inLen; i++)
        {
            if (in[i] != ' ')
            {
                *out++ = in[i];
            }
        }
    }
    return ret;
}
```





### trim字符串空格

```c
char *trim(char *str)
{
        char *p = str;
        char *p1;
        if(p)
        {
                p1 = p + strlen(str) - 1;
                while(*p && isspace(*p)) p++;
                while(p1 > p && isspace(*p1)) *p1-- = '\0';
        }
        return p;
}
```



### 文件中字符串最后一次出现的位置

```c
int SearchLastKey(char *filePath, int from, int to, char *key, int key_len)
{
    int result = -1;
    int n = 0, count = 0;
    int found = 0;
    char buf[10240];

    FILE* f = fopen(filePath, "rb");
    fseek(f, 0, SEEK_END);
    int file_size = ftell(f);
    fseek(f, from, SEEK_SET);
    while ((n = fread(buf, 1, sizeof(buf), f)) > 0)
    {
        if (found > 0)
        {
            int lastCount = key_len - found;
            for (int i = 0; i < lastCount; i++)
            {
                if (key[found] == buf[i])
                {
                    found++;
                }
                else {
                    found = 0;
                    break;
                }
            }

            if (key_len == found)
            {
                found = 0;
                if (from + count + lastCount < to)
                    result = count + lastCount;
                else
                    break;

            }
        }

        for (int i = 0; i < n; i++)
        {
            if (key[0] == buf[i])
            {
                found++;

                if (i + key_len - 1 < n)
                {
                    for (int j = 1; j < key_len; j++)
                    {
                        if (key[j] == buf[i + j])
                        {
                            found++;
                        }
                        else {
                            found = 0;
                            break;
                        }
                    }

                    if (key_len == found)
                    {
                        found = 0;
                        if (from + count + i + key_len < to)
                        {
                            result = count + i + key_len;
                        }
                    }

                }
                else {

                    for (int j = 1; j < n - i; j++)
                    {
                        if (key[j] == buf[i + j])
                        {
                            found++;
                            if (j + 1 == n - i)
                            {
                                i = n;//跳出外层for循环
                            }
                        }
                        else {
                            found = 0;
                            break;
                        }
                    }
                }
            }
        }

        if (from + count > to)
        {
            break;
        }

        count += n;
    }

    fclose(f);

    return result;
}
```





### int 转字符串

```c
int pd = 100;
char cpd[10];
sprintf(cpd,"%d",pd);
```





### 字节流转换为十六进制字符串

```c
void ByteToHexStr(const unsigned char* source, char* dest, int sourceLen)
{
    short i;
    unsigned char highByte, lowByte;

    for (i = 0; i < sourceLen; i++)
    {
        highByte = source[i] >> 4;
        lowByte = source[i] & 0x0f;

        highByte += 0x30;

        if (highByte > 0x39)
            dest[i * 2] = highByte + 0x07;
        else
            dest[i * 2] = highByte;

        lowByte += 0x30;
        if (lowByte > 0x39)
            dest[i * 2 + 1] = lowByte + 0x07;
        else
            dest[i * 2 + 1] = lowByte;
    }
    return;
}
```

```c
void ByteToHexStr(unsigned char* str, char* dest, int len)
{
    unsigned char tmp;
    char stb[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    for (size_t i = 0; i < len; i++)
    {
        tmp = str[i];
        dest[i * 2] = stb[tmp >> 4];
        dest[i * 2 + 1] = stb[tmp & 15];
    }
    return;
}
```







### 十六进制字符串转换为字节流

```c
void HexStrToByte(const char* source,  char* dest, int sourceLen)
{
    short i;
    unsigned char highByte, lowByte;

    for (i = 0; i < sourceLen; i += 2)
    {
        highByte = toupper(source[i]);
        lowByte = toupper(source[i + 1]);

        if (highByte > 0x39)
            highByte -= 0x37;
        else
            highByte -= 0x30;

        if (lowByte > 0x39)
            lowByte -= 0x37;
        else
            lowByte -= 0x30;

        dest[i / 2] = (highByte << 4) | lowByte;
    }
    return;
}
```





## 日期时间



### 获取当前时间

```c
#include <time.h>
void main(){
    char *wday[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    time_t timep;
    struct tm *p;
    time(&timep);
    p = gmtime(&timep);
    printf("%d%d%d", (1900+p->tm_year), (1+p->tm_mon), p->tm_mday);
    printf("%s%d;%d;%d\n", wday[p->tm_wday], p->tm_hour, p->tm_min, p->tm_sec);
}
```

参考链接：http://c.biancheng.net/cpp/html/143.html







## 系统相关



### 启动其他程序

```c
system("/home/tom/hello");
```





### 信号列表

```sh
kill -l
```

![image-20210618141445971](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210618141445971.png)

信号的意义，参考链接：https://www.cnblogs.com/xiao0913/p/11846212.html







## 其他



#### 生成随机数

```c
#include <stdio.h>
#include <stdlib.h>
/**
 *  * Create random UUID
 *   *
 *    * @param buf - buffer to be filled with the uuid string
 *     */
char *random_uuid( char buf[37] )
{
    const char *c = "89ab";
    char *p = buf;
    int n;

    for( n = 0; n < 16; ++n )
    {
        int b = rand()%255;

        switch( n )
        {
            case 6:
                sprintf(
                    p,
                    "4%x",
                    b%15 );
                break;
            case 8:
                sprintf(
                    p,
                    "%c%x",
                    c[rand()%strlen( c )],
                    b%15 );
                break;
            default:
                sprintf(
                    p,
                    "%02x",
                    b );
                break;
        }

        p += 2;

        switch( n )
        {
            case 3:
            case 5:
            case 7:
            case 9:
                *p++ = '-';
                break;
        }
    }

    *p = 0;

    return buf;
}

int main(){
	char buf[37];
	srand((unsigned)time(0));//用时间作为随机种子

	char * result = random_uuid(buf);
	printf("uuid:%s\n",result);
	return 0;
}
```





#### 写txt文本文件

```c
int writeLog(char* path, char* log)
{
    FILE *fp = NULL;

    fp = fopen(path, "a+");
    fprintf(fp, "log: %s\n", log);
    fclose(fp);
}
```



