## 查看配置
~~~shell
git config -l
git config --global -l
~~~



## 配置个人信息
~~~shell
git config --global user.name ''
git config --global user.email ''
~~~



## 初始化本地库
~~~shell
git init
~~~



## 仓库一致性检查

```shell
git fsck
```







## 暂存区操作

### 添加指定文件到暂存区
~~~shell
git add file...
~~~



### 添加全部文件到暂存区
~~~sh
git add .
git add -A
~~~



### 删除除暂存区文件
~~~sh
git rm --cached file...
~~~



## 查看文件状态

红色(工作区)，绿色(暂存区)

~~~sh
git status
~~~



## 提交文件
~~~sh
git commit -m'提交详情'
~~~



## 查看日志
~~~sh
git log
git log --oneline 
git log --oneline --graph
git log --oneline --graph --all
git shortlog
~~~



## 回退版本
~~~sh
git reset --head 6a9d03bb
~~~



## 修改commit的注释信息

```sh
git commit --amend
```







## 暂存临时代码

1、git 代码暂存指令：

```sh
git stash
```



2、git 代码暂存列表信息：

```sh
git stash list
```



3、git 代码应用暂存代码：

```sh
git stash apply stash@{0}
```



4、git 代码清空暂存：

```sh
git stash clear
```



## 逐行检查

逐行检查任何文件的内容

```shell
git blame <file_name>
```



## 删除未跟踪文件

git clean 删除未跟踪文件。

fatal: clean.requireForce 默认为 true 且未提供 -i、-n 或 -f 选项，拒绝执行清理动作

上面的命令失败，因为这是在我的系统中配置git的方式。您可以通过添加-n，-i或-f选项来解决此问题。

查看即将删除哪些文件

```shell
git clean -n
```

![image-20210604131504346](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210604131504346.png)

```sh
git clean -i
```

![image-20210604131613726](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210604131613726.png)







## 取消某个文件的跟踪

```sh
git rm --cached readme1.txt    #删除readme1.txt的跟踪，并保留在本地。
git rm --f readme1.txt    #删除readme1.txt的跟踪，并且删除本地文件。
```











## 撤销操作

### 撤销当前所有修改

~~~sh
git checkout .
~~~



### 撤销对file1的所有修改

~~~sh
git checkout file1
~~~



### 撤销 git add 错添加的文件 

~~~sh
git status 			#先看一下add 中的文件 
git reset HEAD 		#如果后面什么都不跟的话 就是上一次add 里面的全部撤销了 
git reset HEAD XXX/XXX/XXX.java  	#就是对某个文件进行撤销了
~~~



## 查看文件历史版本内容

```sh
git show a648c06:file1.txt
```





## 文件对比

### 工作区与暂存区比较

~~~sh
git diff  filepath
~~~



### 工作区与HEAD ( 当前工作分支) 比较

~~~sh
 git diff HEAD filepath 
~~~



### 暂存区与HEAD比较

~~~sh
git diff --staged 或 --cached  filepath
~~~





### 与branchName 分支的文件比较

~~~sh
git diff branchName filepath 
~~~



### 与某一次提交进行比较

~~~sh
git diff 99fac2d86 filepath
~~~





## 远程仓库

### 查看远程仓库信息
~~~sh
git remote -v
~~~



### 添加关联

~~~sh
git remote add origin git@code.xxx.com:kuangshenxiaomidi233/nodejs-study.git
~~~



### 取消关联
~~~sh
git remote rm origin
~~~



### 拉取项目
~~~sh
git pull origin master
~~~



### 推送项目
~~~sh
git push -u origin master
git push -u origin test
~~~



### 克隆仓库
~~~sh
git clone https://gitee.com/aaa/ssss.git
~~~



### 克隆仓库指定分支

~~~sh
git clone -b 分支名  仓库地址
git clone -b v2.8.1 https://git.oschina.net/oschina/android-app.git
~~~





### 删除已经push到远程的提交

删除已经push到远程gitlab 的提交

1. gitlab上解除分支保护

   ![image-20220621175532790](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220621175532790.png)

      

   

2. 本地git操作

```sh
# 先删除本地的：
git reset –hard  <commit hash>
# 然后push到remote：
git push origin master -f 
```







## 分支操作

### 查看本地分支
~~~sh
git branch
~~~



### 查看远程分支
~~~sh
git branch -a
~~~



### 拉取远程分支并作为本地新创建分支
~~~sh
git checkout -b 1.4.0 origin/1.4.0
~~~



### 切换分支
~~~sh
git checkout 1.4.0
~~~



### 更新本地分支
~~~sh
git pull origin 1.4.0
~~~



### 更新远程分支
~~~sh
git push -u origin 1.4.0
~~~



### 创建本地分支
~~~sh
git branch name
~~~



### 删除分支
~~~sh
git branch -d name
~~~



### 合并name分支到当前分支
~~~sh
git merge name
~~~





### 查看分支当前版本

~~~sh
 git rev-parse HEAD 
~~~



