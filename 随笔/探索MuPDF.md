



# 第一部分 MuPDF C API



## 第3章 快速开始



### 如何打开一个文档并呈现页面

有关如何打开文档并呈现某些页面的简单示例，请参阅docs/example.c。

你在这个例子中遇到的概念将在下面的章节中解释和扩展。当你继续阅读时，手头有这个例子可能很有用，可以让你具体说明所讨论的想法。





## 第4章 命名规则

MuPDF中的函数和变量名称都是按照标准惯例精心选择的。

通过一致地使用相同的术语，我们希望可以很容易地记住一些事情，比如函数的引用计数行为。

我们要求任何提交给MuPDF的代码都要遵循同样的约定，并且并鼓励人们在自己的代码中遵循与MuPDF接口的代码同样的风格。



### 4.1 前缀

MuPDF所依赖的图形库为“**fitz**”。因此，所有MuPDF的API调用和大多数类型都以“**fz_**”开头。

例外情况是，我们有由特定格式处理程序(如PDF或XPS)提供的API和类型。这些函数/类型以格式处理程序名称为前缀，例如“**pdf_**”或“**xps_**”。

所有导出的函数和类型都应以这种方式作为前缀，以避免在调用应用程序时与符号发生冲突。内部函数不需要加前缀，但为了清晰起见，我们推荐这样做。



### 4.2 命名

所有函数都是根据以下方案之一命名的：

- verb_noun
- verb_noun_with_noun
- noun_attribute
- set_noun_attribute
- noun_from_noun 从一种类型转换为另一种类型 (avoid noun_to_noun)  

唯一的例外是当我们有MuPDF特定的函数来模拟（或扩展）"well known  "的函数时，我们会模仿这些函数。例如，"fz_printf "或 "fz_strdup"。

此外，我们避免在函数名中使用“get”，因为这通常是多余的。

然而，相比之下，我们在需要的地方使用“set”。例如：

​	fz_aa_level（检索当前抗锯齿级别）

​	fz_set_aa_level（设置抗锯齿级别）

MuPDF广泛使用了引用计数（更多详细信息，请参阅第22.3节引用计数）。我们保留了各种单词来表示正在使用引用计数：

- **new** 表示此调用创建新对象并返回对它的引用。

  例如，fz_new_pixmap

- **find** 表示此调用从某处查找对象（通常是从缓存或一组标准对象），并返回一个新的对它的引用。例如，fz_find_color_converter  

- **load** 表示此调用创建新对象并返回对它的引用。这类似于“新建”，但意味着操作将读取一些数据，并需要在创建对象之前进行一些（可能重要的）数据处理。例如，fz_load_outline 或fz_load_jpeg

- **open** 表示此调用将创建流对象，并返回对它的引用。例如，fz_open_document

- **keep** 表示此调用将创建对现有引用对象的新引用。例如，fz_keep_colorspace

所有这些调用都返回一个对象引用。调用方负责在对象所需的生命周期内安全地将此引用存储在某个位置，并在调用方不再需要时销毁该引用。

除了使用上面保留字命名的任何函数都不应对引用添加引用计数。

一旦释放了对给定对象的所有引用计数，系统将删除该对象本身。

- **drop** 表示此函数将减少引用计数，例如，fz_drop_font

未能删除引用计数将导致内存泄漏。过早删除引用计数可能会导致崩溃，因为对象在被销毁后可能被访问。

用于销毁不受引用计数限制的对象，可以通过使用单词“**drop**”、“**close**”或“**free**”命名的函数来销毁。

与上面描述的“**find**”不同，我们还有一个关于搜索的保留字：

- **lookup** 表示此函数调用将返回借用的指针（或值）。

当我们已经分配了一个结构体，并且希望初始化其部分或全部内部细节时，我们使用“**init**”。与此匹配的对应命名为“**fin**”。例如，fz_cmm_init_profile与fz_cmm_fin_profile匹配。

有些对象是使用使用动词“**create**”的函数创建的。有时这些对象可以是引用计数对象（例如“pdf_create_document”），在这种情况下，它们应该像往常一样“**drop**”。非引用计数对象应被“**destroy**”，例如，fz_destroy_mutex



## 第6章 错误处理



### 6.1 概述

MuPDF使用一个异常系统来处理所有的错误。在表面上看起来与C++异常处理相似，但（由于MuPDF是用C语言编写的）它是用包裹**setjmp**/**longjmp**标准C函数的宏来实现。

最好不要研究背后的实现细节，而只是把这些结构看作是语言的扩展。事实上，我们已经非常努力地确保将其中的复杂性降到最低。

除非另有规定，所有的MuPDF API函数都可以抛出异常，因此应该在**fz_try**/**fz_always**/**fz_catch**结构中调用。

那些从不抛出异常的特定函数包括所有名为**fz_keep_xxx**、**fz_drop_xxx**和**fz_free**的函数。这一点，再加上所有这些 destructor析构函数都会默默地接受一个NULL参数，推荐整个函数中使用的资源都在**fz_always**块释放。

此异常结构代码如下：

```c
fz_try(ctx)
{
    /* 在此处执行可能发生异常的代码。
    * 绝对不要在这里执行 return 返回. 
    * ”break”可用于继续执行后面的代码（在always块中或catch块之后） */
}
fz_always(ctx)
{
    /*
    * 这里的任何代码都会被执行，无论fz_try子句是否是抛出了异常。
    * 尽量避免在这里调用那些可以抛出异常的函数，否则fz_always块的其余部分将被跳过--这绝不是我们所希望的。
    * 绝对不要在这里执行 return 返回. 
    * ”break”可用于继续执行catch块中及之后的代码*/
}
fz_catch(ctx)
{
    /* 如果（也只有在）fz_try块中的任何东西调用了fz_throw，这个块就会执行。
    * 在这里释放所有需要释放的资源. 
    * 如果我们在一个嵌套的fz_try/fz_catch块中，我们可以调用fz_rethrow来将错误抛到上层的catch中。
    * 除非这个异常被重新抛出（或抛出一个新的异常），否则在这个块之后继续执行 */
}

```

fz_always块是完全可选的。以下代码完全有效：

```c
fz_try(ctx)
{
	/* Do stuff here */
}
fz_catch(ctx)
{
	/* Clean up from errors here */
}
```

在理想情况下就是这样，但实际上存在两个问题。

第一个问题相对简单，就是绝对不能在**fz_try**块中执行return返回。这样做会损坏异常堆栈并导致问题和崩溃。为了缓解这种情况，您可以在**fz_try**中执行break跳出，执行**fz_always**（如果有）中的代码，或**fz_catch**之后的代码。

类似的，您也可以在**fz_always**中执行break跳出，继续执行**fz_catch**之后的代码，但实际中用处不大。

第二个更复杂。如果您不想理解这背后的漫长而复杂的原因，请跳过下面的小节，只需阅读下面更正的示例。只要你遵守最后总结中给出的规则，你就会没事。





#### 6.1.1  为什么必须执行fz_var

如前所述，fz-try/fz-catch是使用setjmp/longjmp实现的，并且此项技术实现方式可能会“丢失”更改了的变量值。





#### 6.1.2 示例：如何使用fz_var保护局部变量

代码的修正版本如下：

```c
house_t *build_house(fz_context *ctx)
{
    walls_t *w = NULL;
    roof_t *r = NULL;
    house_t *h = NULL;
    
    fz_var(w);
    fz_var(r);
    
    fz_try(ctx)
	{
        w = make_walls();
        r = make_roof();
        h = combine(w, r); /* Note, NOT: return combine(w,r); */
    }
    fz_always(ctx)
    {
        drop_walls(w);
        drop_roof(r);
    }
    fz_catch(ctx)
    {
        /* Handle the error somehow. If we are nested within another
        * layer of fz_try/fz_catch, we can simply fz_rethrow. If
        * not, handle it in a way appropriate for this application,
        * perhaps by simply returning NULL. */
    	return NULL;
    }
    return h;
}

```

**fz_var**函数告诉编译器，如果在fz_try中抛出异常，不要丢弃w和r的值。见下文第6.4节总结中的规则5。



### 6.2 引发异常

如果只是开发应用程序App，那么只需要知道如何捕获core抛出的异常就足够了。如果您正在实现自己的设备或扩展MuPDF的core，那么您需要知道如何生成（并传递）自己的异常。

异常枚举：

```c
enum
{
    FZ_ERROR_NONE = 0,
    FZ_ERROR_MEMORY = 1,
    FZ_ERROR_GENERIC = 2,
    FZ_ERROR_SYNTAX = 3,
    FZ_ERROR_TRYLATER = 4,
    FZ_ERROR_ABORT = 5,
    FZ_ERROR_COUNT
};

void fz_throw(fz_context *ctx, int errcode, const char *, ...);
```

几乎在所有情况下，您都应该使用**FZ_ERROR_GENERIC**，例如：

```c
fz_throw(ctx, FZ_ERROR_GENERIC, "Failed to open file ’%s’", filename);
```

**FZ_ERROR_MEMORY**是为因内存分配失败而引发的异常保留的。这很少会被应用程序代码抛出；此类异常的典型生成器是fz_malloc/fz_calloc/fz_realloc等。

**FZ_ERROR_SYNTAX**保留用于解析PDF文档期间由于语法错误而引发的异常。这使解释器代码能够跟踪在文件中发现的语法错误数量，并在传递合理数量后中止解析操作。

**FZ_ERROR_TRYLATER**保留用于因渐进模式中缺少数据而引发的异常（有关详细信息，请参阅第16章渐进模式）。捕获此类型的错误可能会触发不同的处理，从而在收到更多数据时重试操作。

**FZ_ERROR_ABORT**保留用于操作过程中应该停止的所有异常；例如，当循环render页面上的annotations 时，大多数异常都会在顶层捕获并忽略，从而确保单个损坏的annotation不会导致跳过后续的annotation。FZ_ERROR_ABORT可用于覆盖此行为，并使annotation呈现过程尽快结束。





### 6.3 异常处理

一旦捕获到异常，大多数代码将简单地清理释放所有的资源（以防止泄漏），并将异常重新提交给更高层的处理程序。

对于最上层的应用程序来说，这显然是不合适的。catch子句需要使用调用程序用于错误处理的任何进程来返回错误。

捕获到的错误的详细信息可以被fz_catch块读取使用：

```c
const char *fz_caught_message(fz_context *ctx);
```

该错误将保持可读性，直到下次在同一上下文上使用fz_try/fz_catch为止。

某些代码可能会选择忽略错误，然后以不同的方式重试相同的代码。

为了方便起见，我们可以使用下面的代码找出错误的类型：

```c
int fz_caught(fz_context *ctx);
```

有关可能的异常类型的列表，请参阅《第6.2节抛出异常》

例如，如果尝试render一个page到一个整页的bitmap时引发异常，则完全可能是因为内存不足。应用程序应该合理地重新render，一次执行一个strip。

但是，如果由于文件损坏而导致渲染失败，那么重试将一无所获，因此应用程序应该检查异常的类型，以确保它是FZ_ERROR_MEMORY，然后再尝试其他技术。

为了简化决定是否传递给定类型的异常的工作，我们有一个方便的函数，只需重新抛出一个特定类型：

```c
void fz_rethrow_if(fz_context *ctx, int errcode);
```





### 6.4 总结

异常处理的基本规则如下：

1. 所有MuPDF函数（显式声明为其他的函数除外）都会引发错误异常，因此必须从fz_try/fz_catch构造中调用。

2. fz_try块必须与fz_catch块成对出现，并且可以选择在它们之间出现fz_always块。

3. 切勿在fz_try中调用return 返回。

4. 当程序执行到fz_try块的末尾或调用break时，fz_try块将终止。

5. 如果发生异常，在fz_try块中更改的任何局部变量都可能丢失其值，除非受到fz_var调用的保护。

6. 将始终执行fz_always块的内容（在fz_try块之后和fz_catch块之前，如果合适的话）。

7. 如果在fz_try块期间抛出异常，如果有fz_always块，程序将跳到fz_always块执行，然后继续跳到fz_catch块。