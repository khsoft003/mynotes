##### 使用openssl生成证书

1. 下载openssl工具到本地电脑，然后解压，进入bin目录

   在当前目录，按住shift键右击，选择"在此处打开命令窗口"

   

   ![image-20210412093211227](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093211227.png)

   

2. 

   打开cmd命令窗口之后，可以看到，已经自动切换到当前的bin目录位置

   输入openssl命令，进入openssl

   ![image-20210412093302048](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093302048.png)

   

3. 

   现在开始生成CA证书

   创建私钥：genrsa -out ca-key.pem 1024

   创建证书请求：req -new -out ca-req.csr -key ca-key.pem -config openssl.cnf

   执行之后，会在目录下生成ca-key.pem和ca-req.csr文件

   ![image-20210412093328176](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093328176.png)

   

4. 

   自签署证书：

   x509 -req -in ca-req.csr -out ca-cert.pem -signkey ca-key.pem -days 365

   执行完成之后会生成ca-cert.pem文件

   ![image-20210412093349717](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093349717.png)

5. 

   生成server证书

   创建私钥:

   genrsa -out server-key.pem 1024

   创建证书请求：

   req -new -out server-req.csr -key server-key.pem -config openssl.cnf

   执行完成生成server-key.pem和server-req.csr文件

   ![image-20210412093413360](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093413360.png)

   

6. 

   自签署证书：

   x509 -req -in server-req.csr -out server-cert.pem -signkey server-key.pem -CA ca-cert.pem -CAkey ca-key.pem -CAcreateserial -days 365

   执行完成生成server-cert.pem文件

   ![image-20210412093435034](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093435034.png)

   

7. 

   生成client证书，与服务器生成证书差不多

   创建私钥：

   genrsa -out client-key.pem 1024

   创建证书请求：

   req -new -out client-req.csr -key client-key.pem -config openssl.cnf

   自签署证书：

   x509 -req -in client-req.csr -out client-cert.pem -signkey client-key.pem -CA ca-cert.pem -CAkey ca-key.pem -CAcreateserial -days 365

   ![image-20210412093455791](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210412093455791.png)

8. 

   到此CA证书、server证书、client证书全部生成完成，导入对应的服务器或客户端则可以正常使用了





##### 参考网址

https://jingyan.baidu.com/article/6c67b1d6be538c2787bb1e06.html