

## OpenSSL概述和应用场景

- OpenSSL是一个用于TLS/SSL协议的工具包。它也是一个通用密码库。
- 2020年 OpenSSL 3.0 Alpha2 Release (支持国密sm2 sm3 sm4 )
- 包含对称加密，非对称加密，单项散列，伪随机，签名，密码交换，证书等一系列算法库





## DES算法原理

- Data Encryption Standard
- 已经能够暴力破解
- 维护旧系统，解密旧文件
- 属于分组密码

![image-20210303174311360](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210303174311360.png)





## DES算法-轮计算

- 16轮循环
- 每轮使用不同的子秘钥
- 每两轮讲左右侧对调
- Ln = R(n-1);
- Rn = L(n - 1)⊕ f(Rn-1,kn-1)
- 秘钥置换--子密钥生成
  -  循环左移变换

![image-20210303174734423](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210303174734423.png)



## 最简单的openssl项目

~~~c++
#include <iostream>
#include <openssl/rand.h>
#include <openssl/des.h>
#include <openssl/bio.h>
#include <time.h>
#include <thread>
#include <iostream>

#ifdef _WIN32
//解决运行错误 OPENSSL_Uplink(0F5824F8,08): no OPENSSL_Applink
// _CRT_SECURE_NO_WARNINGS
#include <openssl/applink.c>
#endif

/////////////////////////////////////////////////////////////////////////////////////////
//// # @ 老夏课堂 夏曹俊整理
//// # openssl.vip
//// # 下载路径 http ://openssl.vip/download
//// # 文档参考 http ://openssl.vip/docs
//// # 视频课程地址 https://jiedi.ke.qq.com/
//// # QQ群 296249312
using namespace std;
int main(int argc,char *argv[])
{
    cout << "Test OpenSSL" << endl;
    //用时间戳做随机数的种子
    time_t t = time(0);
    RAND_seed(&t,sizeof(t));
    auto mem_bio = BIO_new(BIO_s_mem());
    
    //生成随机数
    unsigned char buf[16] = { 0 };
    int len = sizeof(buf);
    int re = RAND_bytes(buf, len);
    for (int i = 0; i < len; i++)
    {
        cout << "["<<(int)buf[i]<<"]";
    }
    
    return 0;
}
~~~



## DES加密代码测试

- 调用OpenSSL接口进行DES ECB模式加解密
- 模拟对ECB加密的攻击并测试CBC模式
- int DES_ set_ key(const_ DES_ cblock *key,DES_ key_ schedule * schedule)
- void DES_ ecb_ encrypt(const_ DES_ cblock *input,
  									DES_ cblock *output, DES_ key_ schedule *ks,
    									int enc)
- void DES_ cbc_ encrypt(const unsigned char *input,
  				unsigned char *output, long length,
    				DES_ key_ schedule *schedule, 
    				DES_ cblock *ivec, int enc)
- DES_ ncbc_ encrypt







## openssl相关网站

openssl.vip：http://openssl.vip

openssl wiki： https://wiki.openssl.org/index.php/Main_Page

