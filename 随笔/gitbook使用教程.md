# Gitbook使用教程

## GitBook安装

GitBook是一个能将使用 Markdown 语法，快速制作成各种格式电子书的工具。它是一个基于 [Node](https://so.csdn.net/so/search?q=Node&spm=1001.2101.3001.7020).js 的命令行工具，可以用来制作精美的电子书。首先我们得下载Nodejs并安装



> 注意: 截止到目前的 Gitbook V3.2.3版本，需要使用NodeJs的v10+版本，否则会产生各种报错。
>
> 这里建议下载`v10.23.1`版本，官网最新版本我试了也是不行的。



**先安装Nodejs**

`nodejs`历史版本下载连接 `https://nodejs.org/zh-cn/download/releases/`，下载好了直接下一步安装即可。

![image-20220802101804626](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802101804626.png)

安装好了nodejs之后，输入命令检查是否安装 成功。如果命令无效，检查一下环境变量是否配置。

```shell
$ node -v                                                
v10.24.0
```



**安装GitBook**

接下来使用`npm`命令安装`gitbook-cli`。它是在同一系统上安装和使用多个版本的GitBook的实用程序。它将自动安装所需版本的GitBook程序。（可能需要一些时间，耐心等待就行）

```sh
$ npm install gitbook-cli -g
```

通过`gitbook -V`命令查看是否安装成功（可能需要一些时间，耐心等待就行）。V是大写的

```sh
$ gitbook -V
```





## Gitbook基本使用

- **Gitbook文档目录结构**

```
GitBook 基本的目录结构如下所示
|- book.json		//电子书的配置文件
|- README.md		//电子书的主要说明文件
|- SUMMARY.md		//电子书的目录
|- chapter-1/		//电子书的章节1文件夹(chapter-1是文件夹名称，可以自定义)
	|- README.md	    //章节1的说明文件
 	|- 文档1.md		//章节下面的小节1
    |- 文档2.md		//章节下面的小节2
|- chapter-2/		//电子书的章节2文件夹(chapter-2是文件夹名称，可以自定义)
	|- README.md	    //章节2的说明文件
 	|- 文档1.md		//章节下面的小节2
    |- 文档2.md		//章节下面的小节2
```

- **Gitbook初始化**

新建一个文件夹如`gitbook_test`，使用cd命令进入到该文件夹下，在该文件夹下进行初始化。

```sh
$ gitbook init
```

会自动在目录中生成两个文件，一个是主要说明文件，一个是目录文件

![image-20220802102131570](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102131570.png)



- **接下来编写目录**

使用typora打开`SUMMARY.md`文件编写目录，格式如下

```
# Summary
* [教程导读](README.md)
* [day01—环境搭建&快速入门](day01—Java开发环境/README.md)
    * [环境搭建](day01—Java开发环境/环境搭建.md)
    * [入门案例](day01—Java开发环境/基础语法.md)
    * [基础语法](day01—Java开发环境/入门案例.md)
    * [课后练习](day01—Java开发环境/课后练习.md)
* [day02—类型转换&运算符](day02—类型转换&运算符/README.md)
    * [类型转换](day02—类型转换&运算符/类型转换.md)
    * [运算符](day02—类型转换&运算符/运算符.md)
    * [if语句](day02—类型转换&运算符/if语句.md)
    * [课后练习](

```

- **生成各小节md文件**

编写好目录之后，在`gitbook_test`文件夹下，再次使用`gitbook init`命令生成个目录中的各小节md文件。

```sh
$ gitbook init
```

![image-20220802102246768](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102246768.png)

- **编译生成静态网页**

将会在`gitbook_test`目录下生成一个`_book`文件夹，这就是我们的一个静态站点

```sh
$ gitbook build
```

- **编译并预览静态网页**

先编译生成静态站点，并且开启一个4000端口服务，在浏览器中使用`localhost:4000`进行访问

```sh
$ gitbook serve
```

![image-20220802102341539](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102341539.png)





## Gitbook配置文件

Gitbook有一个配置文件`book.json`，在该配置文件中可以配置各种插件，来美化页面的显示和交互效果。 由于用到的插件比较多，这里我把常用配置和插件已经写好了，直接粘贴即可。

> 注意：book.json文件，一定要放在根目录下

```json
{
    "title": "Java入门教程",
    "description": "用最通俗的语言，带你快速走进Java世界的大门",
    "author": "黑马程序员",
    "output.name": "site",
    "language": "zh-hans",
    "gitbook": "3.2.3",
    "root": ".",
	"plugins": [
		"-lunr","-search",
		"-sharing",
		"-default-theme",
		"search-pro",
		"theme-comscore",
		"splitter",
		"tbfed-pagefooter",
		"expandable-chapters",
		"hide-element",
		"custom-favicon",
		"-highlight","prism","prism-themes",
		"code",
		"sectionx",
		"flexible-alerts",
		"ancre-navigation"
	],
	
	"pluginsConfig": {
		"tbfed-pagefooter": {
			"copyright":"Copyright &copy www.itheima.com/javase 2021",
			"modify_label": "该文件修订时间：",
			"modify_format": "YYYY-MM-DD HH:mm:ss"
		},
		
		"hide-element": {
            "elements": [".gitbook-link"]
        },
				
		"favicon": "favicon.ico",
		
		"pdf": {
			"pageNumbers": true,
			"fontFamily": "Arial",
			"fontSize": 12,
			"paperSize": "a4",
			"margin": {
				"right": 62,
				"left": 62,
				"top": 56,
				"bottom": 56
			}
		},
		"page-toc-button": {
            "maxTocDepth": 2,
            "minTocSize": 2
           },
       "prism":{
			"css":[
				"prism-themes/themes/prism-darcula.css"
			]
       },
       "sectionx": {
          "tag": "b"
        }
	},

	"styles": {
	    "website": "styles/website.css",
	    "ebook": "styles/ebook.css",
	    "pdf": "styles/pdf.css",
	    "mobi": "styles/mobi.css",
	    "epub": "styles/epub.css"
	}
}
```

写好配置文件之后，需要执行命令来安装插件（这里下载插件需要一点时间，耐心等待即可）

```sh
$ gitbook install
```



## Gitbook文档编辑

- **显示模式切换**

使用支持markdown语法的编辑器都可以进行编辑，这里建议使用typora。

![image-20220802102511229](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102511229.png)



- **基本的markdown语法**

```
# 一级标题
## 二级标题
### 三级标题

> 引用格式

**文本加粗**

- 小黑点格式

​```
此处编写代码
​```

`行内代码`
```

- 文档内容折叠效果

如：我们想设置一个课堂提问，但是不想让学生看到答案。可以将答案内容折叠起来效果如下。

![image-20220802102706808](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102706808.png)

在md文件中，按照以下格式写入即可，编译之后的网页就是这种显示效果。

```
<!--sec data-title="问题1：跨平台中平台指的是什么？" data-id="section1" data-show=true data-collapse=true ces-->

Java的跨平台性指的是用Java语言开发的程序可以在多种平台上运行，这里的平台指的是操作系统，常见的操作系统有 **Windows**、**Linux**、**MacOS**

<!--endsec-->
```

> 注意：data-id的属性值不能你重复，如果一个页面中有多处折叠效果的话。

- 注意事项效果

如：经常有一些细节问题需要学生注意，需要有一个醒目但又直观的警告效果如下。

![image-20220802102803998](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102803998.png)

在md文件中采用以下格式编写，编译之后页面中显示的就是警告效果

```
> **[!WARNING] **
> 我们只能说Java程序是跨平台的，但是Java虚拟机不是夸平台的
```

- 友情提示效果

如：经常有一些小技巧需要给学生一些建议，可以使用提示效果如下。

![image-20220802102853702](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220802102853702.png)

在md文件中采用以下格式编写，编译之后页面中显示的就是TIP提示效果

```
> **[!TIP] **
> 此题能够写出来，说明你已经掌握了 **数值拆分** 这一类文件的解决思路。详细你，可以的
```



## Gitbook发布站点

如果想要你的站点能够在线访问，可以将编译之后生成的`_book`文件夹，推送到github上，或者gitee上进行托管，并开启Page服务。