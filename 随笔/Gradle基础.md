# Gradle 简介



 Gradle，这是一个基于 JVM 的富有突破性构建工具。

它为您提供了:

- 一个像 ant 一样，通用的灵活的构建工具
- 一种可切换的，像 maven 一样的基于约定约定优于配置的构建框架
- 强大的多工程构建支持
- 强大的依赖管理(基于 ApacheIvy)
- 对已有的 maven 和 ivy 仓库的全面支持
- 支持传递性依赖管理，而不需要远程仓库或者 pom.xml 或者 ivy 配置文件
- ant 式的任务和构建是 gradle 的第一公民
- 基于 groovy，其 build 脚本使用 groovy dsl 编写
- 具有广泛的领域模型支持你的构建

### 基于声明的构建和基于约定的构建

Gradle 的核心在于基于 Groovy 的丰富而可扩展的域描述语言(DSL)。 Groovy 通过声明性的语言元素将基于声明的构建推向下层，你可以按你想要的方式进行组合。 这些元素同样也为支持 Java， Groovy，OSGi，Web 和 Scala 项目提供了基于约定的构建。 并且，这种声明性的语言是可以扩展的。你可以添加新的或增强现有的语言元素。 因此，它提供了简明、可维护和易理解的构建。

### 多项目构建

Gradle 对多项目构建的支持非常出色。项目依赖是首先需要考虑的问题。 我们允许你在多项目构建当中对项目依赖关系进行建模，因为它们才是你真正的问题域。 Gradle 遵守你的布局。

Gradle 提供了局部构建的功能。 如果你在构建一个单独的子项目，Gradle 也会帮你构建它所依赖的所有子项目。 你也可以选择重新构建依赖于特定子项目的子项目。 这种增量构建将使得在大型构建任务中省下大量时间。

### 多种方式管理依赖

不同的团队喜欢用不同的方式来管理他们的外部依赖。 从 Maven 和 Ivy 的远程仓库的传递依赖管理，到本地文件系统的 jar 包或目录，Gradle 对所有的管理策略都提供了方便的支持。

### Gradle 是第一个构建集成工具

Ant tasks 是最重要的。而更有趣的是，Ant projects 也是最重要的。 Gradle 对任意的 Ant 项目提供了深度导入，并在运行时将 Ant 目标(target)转换为原生的 Gradle 任务(task)。 你可以从 Gradle 上依赖它们(Ant targets)，增强它们，甚至在你的 build.xml 上定义对 Gradle tasks 的依赖。Gradle 为属性、路径等等提供了同样的整合。

Gradle 完全支持用于发布或检索依赖的 Maven 或 Ivy 仓库。 Gradle 同样提供了一个转换器，用于将一个 Maven pom.xml 文件转换为一个 Gradle 脚本。Maven 项目的运行时导入的功能将很快会有。

### Groovy

Gradle 的构建脚本是采用 Groovy 写的，而不是用 XML。 但与其他方法不同，它并不只是展示了由一种动态语言编写的原始脚本的强大。 那样将导致维护构建变得很困难。 Gradle 的整体设计是面向被作为一门语言，而不是一个僵化的框架。

### The Gradle wrapper

Gradle Wrapper 允许你在没有安装 Gradle 的机器上执行 Gradle 构建。 这一点是非常有用的。比如，对一些持续集成服务来说。 它对一个开源项目保持低门槛构建也是非常有用的。 Wrapper 对企业来说也很有用，它使得对客户端计算机零配置。 它强制使用指定的版本，以减少兼容支持问题。



### 为什么使用 Groovy?

我们认为内部 DSL（基于一种动态语言）相比 XML 在构建脚本方面优势非常大。它们是一对动态语言。 为什么使用 Groovy？答案在于 Gradle 内部的运行环境。 虽然 Gradle 核心目的是作为通用构建工具，但它还是主要面向 Java 项目。 这些项目的团队成员显然熟悉 Java。我们认为一个构建工具应该尽可能地对所有团队成员透明。

你可能会想说，为什么不能使用 Java 来作为构建脚本的语言。 我认为这是一个很有意义的问题。对你们的团队来讲，它确实会有最高的透明度和最低的学习曲线。 但由于 Java 本身的局限性，这种构建语言可能就不会那样友善、 富有表现力和强大。 [1] 这也是为什么像 Python，Groovy 或者 Ruby 这样的语言在这方面表现得更好的原因。 我们选择了 Groovy，因为它向 Java 人员提供了目前为止最大的透明度。 其基本的语法，类型，包结构和其他方面都与 Java 一样，Groovy 在这之上又增加了许多东西。但是和 Java 也有着共同点。







# Gradle 安装

1. 已安装 JDK/JRE（版本 7 或以上）

2. 从 [Gralde 官方网站](https://gradle.org/releases/)下载 Gradle 的最新发行包。

   ![image-20210617205939010](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20210617210106.png)

3. 配置环境变量 %GRADLE_HOME%\bin 加入到 PATH 环境变量中

4. 测试安装是否成功

   ```sh
   gradle -v
   ```

   



# Gradle操作笔记



#### 查看gradle版本

```sh
gradle -v
```











#### 使用gradle wrapper

1. 创建一个build.gradle文件
2. 执行 gradle wrapper命令

3. 执行结果：

   ![image-20210630212615063](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20210630212615.png)

4. 使用git管理项目时，无需上传.gradle文件夹中的文件

5. git 克隆demo3项目到新的电脑以后，在demo3 文件夹下执行gradlew命令，就会重新生成.gradle文件夹

   ```sh
   ./gradlew
   ```

   ![image-20210630214741216](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20210630214741.png)









# IDEA新建gradle项目





#### Java项目目录说明

- src/main/java	放置正式代码目录
- src/main/ resouces 放置正式配置文件目录
- src/test/java  放置单元测试代码目录
- src/ /test/ resources  放置测试配置文件目录
- src/main/ webapp   放页面元素，比如：js, css, Img,jsp,html等等

