

## openssl相关

### openssl 1.0.2与1.1.0的差别

​	研究结果：以下函数在1.1.0中才有

```sh
build/release/libmupdf-pkcs7.a(pkcs7-openssl.o)：在函数‘stream_free’中：
pkcs7-openssl.c:(.text.stream_free+0xc)：对‘BIO_get_data’未定义的引用
pkcs7-openssl.c:(.text.stream_free+0x20)：对‘BIO_set_data’未定义的引用
pkcs7-openssl.c:(.text.stream_free+0x2c)：对‘BIO_set_init’未定义的引用
build/release/libmupdf-pkcs7.a(pkcs7-openssl.o)：在函数‘stream_new’中：
pkcs7-openssl.c:(.text.stream_new+0x35)：对‘BIO_set_init’未定义的引用
pkcs7-openssl.c:(.text.stream_new+0x42)：对‘BIO_set_data’未定义的引用
build/release/libmupdf-pkcs7.a(pkcs7-openssl.o)：在函数‘stream_ctrl’中：
pkcs7-openssl.c:(.text.stream_ctrl+0xf)：对‘BIO_get_data’未定义的引用
build/release/libmupdf-pkcs7.a(pkcs7-openssl.o)：在函数‘stream_read’中：
pkcs7-openssl.c:(.text.stream_read+0x10)：对‘BIO_get_data’未定义的引用
build/release/libmupdf-pkcs7.a(pkcs7-openssl.o)：在函数‘BIO_GetStreamHash’中：
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x5c)：对‘BIO_get_data’未定义的引用
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x1b8)：对‘BIO_meth_new’未定义的引用
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x1d7)：对‘BIO_meth_set_read’未定义的引用
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x1ea)：对‘BIO_meth_set_ctrl’未定义的引用
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x1fd)：对‘BIO_meth_set_create’未定义的引用
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x210)：对‘BIO_meth_set_destroy’未定义的引用
pkcs7-openssl.c:(.text.BIO_GetStreamHash+0x223)：对‘BIO_meth_set_callback_ctrl’未定义的引用
```



