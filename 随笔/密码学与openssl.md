# 密码学



## 算法分类

### 1 对称加密

- DES
- 3DES
- DESX
- Blowfish
- IDEA
- RC4
- RC5
- RC6
- AES



### 2 非对称加密

​		

|                   | 基于数学难题分类 |
| ----------------- | ---------------- |
| RSA               | 大整数分解问题类 |
| DSA(数字签名较优) | 离散对数问题类   |
| ECC(移动设备用)   | 椭圆曲线类       |
| Diffie-Hellman    |                  |
| EI Gamal          |                  |

​		





### 3 Hash算法

- MD2
- MD4
- MD5
- HAVAL
- SHA
- SHA-1
- HMAC
- HMAC-MD5
- HMAC-SHA1





## openssl RSA

### 私钥的生成

```SH
openssl genrsa -out private.pem 2048
```





### 公钥的生成

```sh
openssl rsa -in ./private.pem -pubout -out pulbic.pem
```



### 使用公钥对文件加密

```sh
openssl rsautl -encrypt -in a.txt -inkey public.pem -out message.en
```



### 使用私钥对文件解密

```sh
openssl rsautl -decrypt -in message.en -inkey private.pem -out message.txt
```



### 使用私钥对文件签名

```sh
openssl dgst -sign private.pem -md5 -out message.sign  a.txt
```



### 使用公钥验证签名

```sh
openssl dgst -verify public.pem -md5 -signature message.sign a.txt
```



### 摘要算法

```sh
openssl dgst -sha1 file.txt
```





