## 安装相关软件及扩展

1. 安装Picgo客户端 [下载地址](https://github.com/Molunerfinn/PicGo)

2. 在Picgo里面安装Gitee扩展插件件

   ![image-20210205103640915](https://gitee.com/khsoft003/mypics/raw/master/image-20210205103640915.png)

3. 安装完成之后图床设置这里就会显示gitee

   ![image-20210205103726224](https://gitee.com/khsoft003/mypics/raw/master/image-20210205103726224.png)







## Gitee创建图床仓库，获取Token

1. 自行注册账号，创建仓库（仓库选公开类型） [Gitee官网](https://gitee.com/)

2. 在设置中创建私人令牌（Token）自行保存好，它只会在创建的时候显示一次，之后就不再显示，只能重新生成或者创建

   ![image-20210205103802718](https://gitee.com/khsoft003/mypics/raw/master/image-20210205103802718.png)











## 配置Picgo上的Gitee

#### 1. 配置如下图

![image-20210205103828092](https://gitee.com/khsoft003/mypics/raw/master/image-20210205103828092.png)







#### 2. Picgo 图片上传

- 操作界面上传

![image-20210205103900617](https://gitee.com/khsoft003/mypics/raw/master/image-20210205103900617.png)

首先一定选中gitee（如果你配置的是其他仓库（如Github）自行选择自己的仓库即可）

可以直接拖拽上传，也可以把图片复制点剪贴板点击 相应按钮上传

- 快捷键上传

  Picgo 可以自定义上传快捷键，直接将剪贴板的图片上传

![image-20210205104021422](https://gitee.com/khsoft003/mypics/raw/master/image-20210205104021422.png)

![image-20210205104044001](https://gitee.com/khsoft003/mypics/raw/master/image-20210205104044001.png)

**至此Picgo搭配Gitee的图床就搭建完了**











## 使用Typora的Picgo插件

- VSCode里面的PicGo扩展用不了Gitee，可以使用Typora来代替
- 并且Typora可以自动将文章上的图片上传至图床

#### 1. 下载 Typora [下载地址](https://typora.io/)



#### 2. 配置Picgo

   - 进入Typora设置

   ![image-20210205104152722](https://gitee.com/khsoft003/mypics/raw/master/image-20210205104152722.png)

   - 参考图片内容设置

     ![image-20210205104217961](https://gitee.com/khsoft003/mypics/raw/master/image-20210205104217961.png)

   - 该设置完成之后，直接将图片复制进Typora 的编辑区即可完成图片的上传，并且文章使用的图片Url就是图床相应的Url
