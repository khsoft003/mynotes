# OIDs

0.4.0.1862.1.1, QcCompliance

0.4.0.1862.1.2, QcLimitValue

0.4.0.1862.1.3, QcRetentionPeriod

0.4.0.1862.1.4, QcSSCD

0.4.0.1862.1.5, QcPDS

0.4.0.1862.1.6, QcType

0.4.0.1862.1.6.1, esign

0.4.0.1862.1.6.2, eseal

0.4.0.1862.1.6.3, web

0.4.0.19495.1, RoleOfPsp

0.4.0.19495.1.1, PSP_AS

0.4.0.19495.1.2, PSP_PI

0.4.0.19495.1.3, PSP_AI

0.4.0.19495.1.4, PSP_IC

0.4.0.19495.2, PSD2 qcstatement

0.4.0.194112.1.0, QCP-n

0.4.0.194112.1.1, QCP-l

0.4.0.194112.1.2, QCP-n-qscd

0.4.0.194112.1.3, QCP-l-qscd

0.4.0.194112.1.4, QCP-w

0.9.2342.19200300.100.1.25, domainComponent

1.2.36.68980861.1.1.10, Signet pilot

1.2.36.68980861.1.1.11, Signet intraNet

1.2.36.68980861.1.1.2, Signet personal

1.2.36.68980861.1.1.20, Signet securityPolicy

1.2.36.68980861.1.1.3, Signet business

1.2.36.68980861.1.1.4, Signet legal

1.2.36.75878867.1.100.1.1, Certificates Australia policyIdentifier

1.2.752.34.1, seis-cp

1.2.752.34.1.1, SEIS certificatePolicy-s10

1.2.752.34.2, SEIS pe

1.2.752.34.3, SEIS at

1.2.752.34.3.1, SEIS at-personalIdentifier

1.2.840.10040.2.1, holdinstruction-none

1.2.840.10040.2.2, holdinstruction-callissuer

1.2.840.10040.2.3, holdinstruction-reject

1.2.840.10040.4.1, dsa

1.2.840.10040.4.3, dsaWithSha1

1.2.840.10045.1, fieldType

1.2.840.10045.1.1, prime-field

1.2.840.10045.1.2, characteristic-two-field

1.2.840.10045.1.2.1, ecPublicKey

1.2.840.10045.1.2.3, characteristic-two-basis

1.2.840.10045.1.2.3.1, onBasis

1.2.840.10045.1.2.3.2, tpBasis

1.2.840.10045.1.2.3.3, ppBasis

1.2.840.10045.2, publicKeyType

1.2.840.10045.2.1, ecPublicKey

1.2.840.10046.2.1, dhPublicNumber

1.2.840.113533.7, nsn

1.2.840.113533.7.65, nsn-ce

1.2.840.113533.7.65.0, entrustVersInfo

1.2.840.113533.7.66, nsn-alg

1.2.840.113533.7.66.10, cast5CBC

1.2.840.113533.7.66.11, cast5MAC

1.2.840.113533.7.66.12, pbeWithMD5AndCAST5-CBC

1.2.840.113533.7.66.13, passwordBasedMac

1.2.840.113533.7.66.3, cast3CBC

1.2.840.113533.7.67, nsn-oc

1.2.840.113533.7.67.0, entrustUser

1.2.840.113533.7.68, nsn-at

1.2.840.113533.7.68.0, entrustCAInfo

1.2.840.113533.7.68.10, attributeCertificate

1.2.840.113549.1.1, pkcs-1

1.2.840.113549.1.1.1, rsaEncryption

1.2.840.113549.1.1.2, md2withRSAEncryption

1.2.840.113549.1.1.3, md4withRSAEncryption

1.2.840.113549.1.1.4, md5withRSAEncryption

1.2.840.113549.1.1.5, sha1withRSAEncryption

1.2.840.113549.1.1.6, rsaOAEPEncryptionSET

1.2.840.113549.1.12, pkcs-12

1.2.840.113549.1.12.1, pkcs-12-PbeIds

1.2.840.113549.1.12.1.1, pbeWithSHAAnd128BitRC4

1.2.840.113549.1.12.1.2, pbeWithSHAAnd40BitRC4

1.2.840.113549.1.12.1.3, pbeWithSHAAnd3-KeyTripleDES-CBC

1.2.840.113549.1.12.1.4, pbeWithSHAAnd2-KeyTripleDES-CBC

1.2.840.113549.1.12.1.5, pbeWithSHAAnd128BitRC2-CBC

1.2.840.113549.1.12.1.6, pbeWithSHAAnd40BitRC2-CBC

1.2.840.113549.1.12.10, pkcs-12Version1

1.2.840.113549.1.12.10.1, pkcs-12BadIds

1.2.840.113549.1.12.10.1.1, pkcs-12-keyBag

1.2.840.113549.1.12.10.1.2, pkcs-12-pkcs-8ShroudedKeyBag

1.2.840.113549.1.12.10.1.3, pkcs-12-certBag

1.2.840.113549.1.12.10.1.4, pkcs-12-crlBag

1.2.840.113549.1.12.10.1.5, pkcs-12-secretBag

1.2.840.113549.1.12.10.1.6, pkcs-12-safeContentsBag

1.2.840.113549.1.12.2, pkcs-12-ESPVKID

1.2.840.113549.1.12.2.1, pkcs-12-PKCS8KeyShrouding

1.2.840.113549.1.12.3, pkcs-12-BagIds

1.2.840.113549.1.12.3.1, pkcs-12-keyBagId

1.2.840.113549.1.12.3.2, pkcs-12-certAndCRLBagId

1.2.840.113549.1.12.3.3, pkcs-12-secretBagId

1.2.840.113549.1.12.3.4, pkcs-12-safeContentsId

1.2.840.113549.1.12.3.5, pkcs-12-pkcs-8ShroudedKeyBagId

1.2.840.113549.1.12.4, pkcs-12-CertBagID

1.2.840.113549.1.12.4.1, pkcs-12-X509CertCRLBagID

1.2.840.113549.1.12.4.2, pkcs-12-SDSICertBagID

1.2.840.113549.1.12.5, pkcs-12-OID

1.2.840.113549.1.12.5.1, pkcs-12-PBEID

1.2.840.113549.1.12.5.1.1, pkcs-12-PBEWithSha1And128BitRC4

1.2.840.113549.1.12.5.1.2, pkcs-12-PBEWithSha1And40BitRC4

1.2.840.113549.1.12.5.1.3, pkcs-12-PBEWithSha1AndTripleDESCBC

1.2.840.113549.1.12.5.1.4, pkcs-12-PBEWithSha1And128BitRC2CBC

1.2.840.113549.1.12.5.1.5, pkcs-12-PBEWithSha1And40BitRC2CBC

1.2.840.113549.1.12.5.1.6, pkcs-12-PBEWithSha1AndRC4

1.2.840.113549.1.12.5.1.7, pkcs-12-PBEWithSha1AndRC2CBC

1.2.840.113549.1.12.5.2, pkcs-12-EnvelopingID

1.2.840.113549.1.12.5.2.1, pkcs-12-RSAEncryptionWith128BitRC4

1.2.840.113549.1.12.5.2.2, pkcs-12-RSAEncryptionWith40BitRC4

1.2.840.113549.1.12.5.2.3, pkcs-12-RSAEncryptionWithTripleDES

1.2.840.113549.1.12.5.3, pkcs-12-SignatureID

1.2.840.113549.1.12.5.3.1, pkcs-12-RSASignatureWithSHA1Digest

1.2.840.113549.1.3, pkcs-3

1.2.840.113549.1.3.1, dhKeyAgreement

1.2.840.113549.1.5, pkcs-5

1.2.840.113549.1.5.1, pbeWithMD2AndDES-CBC

1.2.840.113549.1.5.10, pbeWithSHAAndDES-CBC

1.2.840.113549.1.5.3, pbeWithMD5AndDES-CBC

1.2.840.113549.1.5.4, pbeWithMD2AndRC2-CBC

1.2.840.113549.1.5.6, pbeWithMD5AndRC2-CBC

1.2.840.113549.1.5.9, pbeWithMD5AndXOR

1.2.840.113549.1.7, pkcs-7

1.2.840.113549.1.7.1, data

1.2.840.113549.1.7.2, signedData

1.2.840.113549.1.7.3, envelopedData

1.2.840.113549.1.7.4, signedAndEnvelopedData

1.2.840.113549.1.7.5, digestData

1.2.840.113549.1.7.6, encryptedData

1.2.840.113549.1.7.7, dataWithAttributes

1.2.840.113549.1.7.8, encryptedPrivateKeyInfo

1.2.840.113549.1.9, pkcs-9

1.2.840.113549.1.9.1, emailAddress

1.2.840.113549.1.9.2, unstructuredName

1.2.840.113549.1.9.3, contentType

1.2.840.113549.1.9.4, messageDigest

1.2.840.113549.1.9.5, signingTime

1.2.840.113549.1.9.6, countersignature

1.2.840.113549.1.9.7, challengePassword

1.2.840.113549.1.9.8, unstructuredAddress

1.2.840.113549.1.9.9, extendedCertificateAttributes

1.2.840.113549.1.9.10, issuerAndSerialNumber

1.2.840.113549.1.9.11, passwordCheck

1.2.840.113549.1.9.12, publicKey

1.2.840.113549.1.9.13, signingDescription

1.2.840.113549.1.9.14, extensionReq

1.2.840.113549.1.9.15, sMIMECapabilities

1.2.840.113549.1.9.15.1, preferSignedData

1.2.840.113549.1.9.15.2, canNotDecryptAny

1.2.840.113549.1.9.15.3, receiptRequest

1.2.840.113549.1.9.15.4, receipt

1.2.840.113549.1.9.15.5, contentHints

1.2.840.113549.1.9.15.6, mlExpansionHistory

1.2.840.113549.1.9.16, id-sMIME

1.2.840.113549.1.9.16.0, id-mod

1.2.840.113549.1.9.16.0.1, id-mod-cms

1.2.840.113549.1.9.16.0.2, id-mod-ess

1.2.840.113549.1.9.16.1, id-ct

1.2.840.113549.1.9.16.1.1, id-ct-receipt

1.2.840.113549.1.9.16.1.4, Timestamp Token

1.2.840.113549.1.9.16.2, id-aa

1.2.840.113549.1.9.16.2.1, id-aa-receiptRequest

1.2.840.113549.1.9.16.2.2, id-aa-securityLabel

1.2.840.113549.1.9.16.2.3, id-aa-mlExpandHistory

1.2.840.113549.1.9.16.2.4, id-aa-contentHint

1.2.840.113549.1.9.16.2.47, Signing certificate V2

1.2.840.113549.1.9.20, friendlyName

1.2.840.113549.1.9.21, localKeyID

1.2.840.113549.1.9.22, certTypes

1.2.840.113549.1.9.22.1, x509Certificate

1.2.840.113549.1.9.22.2, sdsiCertificate

1.2.840.113549.1.9.23, crlTypes

1.2.840.113549.1.9.23.1, x509Crl

1.2.840.113549.2, digestAlgorithm

1.2.840.113549.2.2, md2

1.2.840.113549.2.4, md4

1.2.840.113549.2.5, md5

1.2.840.113549.3, encryptionAlgorithm

1.2.840.113549.3.10, desCDMF

1.2.840.113549.3.2, rc2CBC

1.2.840.113549.3.3, rc2ECB

1.2.840.113549.3.4, rc4

1.2.840.113549.3.5, rc4WithMAC

1.2.840.113549.3.6, DESX-CBC

1.2.840.113549.3.7, DES-EDE3-CBC

1.2.840.113549.3.8, RC5CBC

1.2.840.113549.3.9, RC5-CBCPad

1.2.840.113556.1.4.319, LDAP_PAGED_RESULT_OID_STRING

1.2.840.113556.1.4.417, LDAP_SERVER_SHOW_DELETED_OID

1.2.840.113556.1.4.473, LDAP_SERVER_SORT_OID

1.2.840.113556.1.4.474, LDAP_SERVER_RESP_SORT_OID

1.2.840.113556.1.4.521, LDAP_SERVER_CROSSDOM_MOVE_TARGET_OID

1.2.840.113556.1.4.528, LDAP_SERVER_NOTIFICATION_OID

1.2.840.113556.1.4.529, LDAP_SERVER_EXTENDED_DN_OID

1.2.840.113556.1.4.619, LDAP_SERVER_LAZY_COMMIT_OID

1.2.840.113556.1.4.801, LDAP_SERVER_SD_FLAGS_OID

1.2.840.113556.1.4.802, LDAP_SERVER_RANGE_OPTION_OID

1.2.840.113556.1.4.805, LDAP_SERVER_TREE_DELETE_OID

1.2.840.113556.1.4.841, LDAP_SERVER_DIRSYNC_OID

1.2.840.113556.1.4.970, LDAP_SERVER_GET_STATS_OID

1.2.840.113556.1.4.1338, LDAP_SERVER_VERIFY_NAME_OID

1.2.840.113556.1.4.1339, LDAP_SERVER_DOMAIN_SCOPE_OID

1.2.840.113556.1.4.1340, LDAP_SERVER_SEARCH_OPTIONS_OID

1.2.840.113556.1.4.1341, LDAP_SERVER_RODC_DCPROMO_OID

1.2.840.113556.1.4.1413, LDAP_SERVER_PERMISSIVE_MODIFY_OID

1.2.840.113556.1.4.1504, LDAP_SERVER_ASQ_OID

1.2.840.113556.1.4.1852, LDAP_SERVER_QUOTA_CONTROL_OID

1.2.840.113556.1.4.1907, LDAP_SERVER_SHUTDOWN_NOTIFY_OID

1.2.840.113556.1.4.1948, LDAP_SERVER_RANGE_RETRIEVAL_NOERR_OID

1.2.840.113556.1.4.1974, LDAP_SERVER_FORCE_UPDATE_OID 

1.2.840.113556.1.4.2026, LDAP_SERVER_DN_INPUT_OID

1.2.840.113556.1.4.2064, LDAP_SERVER_SHOW_RECYCLED_OID

1.2.840.113556.1.4.2065, LDAP_SERVER_SHOW_DEACTIVATED_LINK_OID

1.2.840.113556.1.4.2066, LDAP_SERVER_POLICY_HINTS_DEPRECATED_OID

1.2.840.113556.1.4.2090, LDAP_SERVER_DIRSYNC_EX_OID

1.2.840.113556.1.4.2204, LDAP_SERVER_TREE_DELETE_EX_OID

1.2.840.113556.1.4.2205, LDAP_SERVER_UPDATE_STATS_OID

1.2.840.113556.1.4.2206, LDAP_SERVER_SEARCH_HINTS_OID

1.2.840.113556.1.4.2211, LDAP_SERVER_EXPECTED_ENTRY_COUNT_OID

1.2.840.113556.1.4.2239, LDAP_SERVER_POLICY_HINTS_OID

1.2.840.113556.1.4.2255, LDAP_SERVER_SET_OWNER_OID

1.2.840.113556.1.4.2256, LDAP_SERVER_BYPASS_QUOTA_OID

1.2.840.113556.4.3, microsoftExcel

1.2.840.113556.4.4, titledWithOID

1.2.840.113556.4.5, microsoftPowerPoint

1.3.6.1.5.5.7.48.1.1, Basic OCSP Response

1.3.6.1.5.5.7.48.1.2, OCSP Nonce

1.3.6.1.5.5.7.48.1.3, OCSP CRL Reference

1.3.6.1.5.5.7.48.1.5, OCSP No Revocation Checking

1.3.133.16.840.9.84, x9-84

1.3.133.16.840.9.84.0, x9-84-Module

1.3.133.16.840.9.84.0.1, x9-84-Biometrics

1.3.133.16.840.9.84.0.2, x9-84-CMS

1.3.133.16.840.9.84.0.3, x9-84-Identifiers

1.3.133.16.840.9.84.1, biometric

1.3.133.16.840.9.84.1.0, id-unknown-Type

1.3.133.16.840.9.84.1.1, id-body-Odor

1.3.133.16.840.9.84.1.10, id-palm

1.3.133.16.840.9.84.1.11, id-retina

1.3.133.16.840.9.84.1.12, id-signature

1.3.133.16.840.9.84.1.13, id-speech-Pattern

1.3.133.16.840.9.84.1.14, id-thermal-Image

1.3.133.16.840.9.84.1.15, id-vein-Pattern

1.3.133.16.840.9.84.1.16, id-thermal-Face-Image

1.3.133.16.840.9.84.1.17, id-thermal-Hand-Image

1.3.133.16.840.9.84.1.18, id-lip-Movement

1.3.133.16.840.9.84.1.19, id-gait

1.3.133.16.840.9.84.1.2, id-dna

1.3.133.16.840.9.84.1.3, id-ear-Shape

1.3.133.16.840.9.84.1.4, id-facial-Features

1.3.133.16.840.9.84.1.5, id-finger-Image

1.3.133.16.840.9.84.1.6, id-finger-Geometry

1.3.133.16.840.9.84.1.7, id-hand-Geometry

1.3.133.16.840.9.84.1.8, id-iris-Features

1.3.133.16.840.9.84.1.9, id-keystroke-Dynamics

1.3.133.16.840.9.84.2, processing-algorithm

1.3.133.16.840.9.84.3, matching-method

1.3.133.16.840.9.84.4, format-Owner

1.3.133.16.840.9.84.4.0, cbeff-Owner

1.3.133.16.840.9.84.4.1, ibia-Owner

1.3.133.16.840.9.84.4.1.1, id-ibia-SAFLINK

1.3.133.16.840.9.84.4.1.10, id-ibia-SecuGen

1.3.133.16.840.9.84.4.1.11, id-ibia-PreciseBiometric

1.3.133.16.840.9.84.4.1.12, id-ibia-Identix

1.3.133.16.840.9.84.4.1.13, id-ibia-DERMALOG

1.3.133.16.840.9.84.4.1.14, id-ibia-LOGICO

1.3.133.16.840.9.84.4.1.15, id-ibia-NIST

1.3.133.16.840.9.84.4.1.16, id-ibia-A3Vision

1.3.133.16.840.9.84.4.1.17, id-ibia-NEC

1.3.133.16.840.9.84.4.1.18, id-ibia-STMicroelectronics

1.3.133.16.840.9.84.4.1.2, id-ibia-Bioscrypt

1.3.133.16.840.9.84.4.1.3, id-ibia-Visionics

1.3.133.16.840.9.84.4.1.4, id-ibia-InfineonTechnologiesAG

1.3.133.16.840.9.84.4.1.5, id-ibia-IridianTechnologies

1.3.133.16.840.9.84.4.1.6, id-ibia-Veridicom

1.3.133.16.840.9.84.4.1.7, id-ibia-CyberSIGN

1.3.133.16.840.9.84.4.1.8, id-ibia-eCryp.

1.3.133.16.840.9.84.4.1.9, id-ibia-FingerprintCardsAB

1.3.133.16.840.9.84.4.2, x9-Owner

1.3.14.2.26.5, sha

1.3.14.3.2.1.1, rsa

1.3.14.3.2.10, desMAC

1.3.14.3.2.11, rsaSignature

1.3.14.3.2.12, dsa

1.3.14.3.2.13, dsaWithSHA

1.3.14.3.2.14, mdc2WithRSASignature

1.3.14.3.2.15, shaWithRSASignature

1.3.14.3.2.16, dhWithCommonModulus

1.3.14.3.2.17, desEDE

1.3.14.3.2.18, sha

1.3.14.3.2.19, mdc-2

1.3.14.3.2.2, md4WitRSA

1.3.14.3.2.2.1, sqmod-N

1.3.14.3.2.20, dsaCommon

1.3.14.3.2.21, dsaCommonWithSHA

1.3.14.3.2.22, rsaKeyTransport

1.3.14.3.2.23, keyed-hash-seal

1.3.14.3.2.24, md2WithRSASignature

1.3.14.3.2.25, md5WithRSASignature

1.3.14.3.2.26, sha1

1.3.14.3.2.27, dsaWithSHA1

1.3.14.3.2.28, dsaWithCommonSHA1

1.3.14.3.2.29, sha-1WithRSAEncryption

1.3.14.3.2.3, md5WithRSA

1.3.14.3.2.3.1, sqmod-NwithRSA

1.3.14.3.2.4, md4WithRSAEncryption

1.3.14.3.2.6, desECB

1.3.14.3.2.7, desCBC

1.3.14.3.2.8, desOFB

1.3.14.3.2.9, desCFB

1.3.14.3.3.1, simple-strong-auth-mechanism

1.3.14.7.2.1.1, ElGamal

1.3.14.7.2.3.1, md2WithRSA

1.3.14.7.2.3.2, md2WithElGamal

1.3.36.3, algorithm

1.3.36.3.1, encryptionAlgorithm

1.3.36.3.1.1, des

1.3.36.3.1.1.1.1, desECBPad

1.3.36.3.1.1.1.1.1, desECBPadISO

1.3.36.3.1.1.2.1, desCBCPad

1.3.36.3.1.1.2.1.1, desCBCPadISO

1.3.36.3.1.2, idea

1.3.36.3.1.2.1, ideaECB

1.3.36.3.1.2.1.1, ideaECBPad

1.3.36.3.1.2.1.1.1, ideaECBPadISO

1.3.36.3.1.2.2, ideaCBC

1.3.36.3.1.2.2.1, ideaCBCPad

1.3.36.3.1.2.2.1.1, ideaCBCPadISO

1.3.36.3.1.2.3, ideaOFB

1.3.36.3.1.2.4, ideaCFB

1.3.36.3.1.3, des-3

1.3.36.3.1.3.1.1, des-3ECBPad

1.3.36.3.1.3.1.1.1, des-3ECBPadISO

1.3.36.3.1.3.2.1, des-3CBCPad

1.3.36.3.1.3.2.1.1, des-3CBCPadISO

1.3.36.3.2, hashAlgorithm

1.3.36.3.2.1, ripemd160

1.3.36.3.2.2, ripemd128

1.3.36.3.2.3, ripemd256

1.3.36.3.2.4, mdc2singleLength

1.3.36.3.2.5, mdc2doubleLength

1.3.36.3.3, signatureAlgorithm

1.3.36.3.3.1, rsa

1.3.36.3.3.1.1, rsaMitSHA-1

1.3.36.3.3.1.2, rsaMitRIPEMD160

1.3.36.3.3.2, ellipticCurve

1.3.36.3.4, signatureScheme

1.3.36.3.4.1, iso9796-1

1.3.36.3.4.2.1, iso9796-2

1.3.36.3.4.2.2, iso9796-2rsa

1.3.36.4, attribute

1.3.36.5, policy

1.3.36.6, api

1.3.36.6.1, manufacturerSpecific

1.3.36.6.2, functionalitySpecific

1.3.36.7, api

1.3.36.7.1, keyAgreement

1.3.36.7.2, keyTransport

1.3.6.1.4.1.2428.10.1.1, UNINETT policyIdentifier

1.3.6.1.4.1.2712.10, ICE-TEL policyIdentifier

1.3.6.1.4.1.3029.32.1, cryptlibEnvelope

1.3.6.1.4.1.3029.32.2, cryptlibPrivateKey

1.3.6.1.4.1.311, Microsoft OID

1.3.6.1.4.1.311.10.2, nextUpdateLocation

1.3.6.1.4.1.311.2, Authenticode

1.3.6.1.4.1.311.2.1.4, SPC_INDIRECT_DATA_OBJID

1.3.6.1.4.1.311.2.1.10, SPC_SP_AGENCY_INFO_OBJID

1.3.6.1.4.1.311.2.1.11, SPC_STATEMENT_TYPE_OBJID

1.3.6.1.4.1.311.2.1.12, SPC_SP_OPUS_INFO_OBJID

1.3.6.1.4.1.311.2.1.14, SPC_CERT_EXTENSIONS_OBJID

1.3.6.1.4.1.311.2.1.15, SPC_PE_IMAGE_DATA_OBJID

1.3.6.1.4.1.311.2.1.18, SPC_RAW_FILE_DATA_OBJID

1.3.6.1.4.1.311.2.1.19, SPC_STRUCTURED_STORAGE_DATA_OBJID

1.3.6.1.4.1.311.2.1.20, SPC_JAVA_CLASS_DATA_OBJID

1.3.6.1.4.1.311.2.1.21, SPC_INDIVIDUAL_SP_KEY_PURPOSE_OBJID

1.3.6.1.4.1.311.2.1.22, SPC_COMMERCIAL_SP_KEY_PURPOSE_OBJID

1.3.6.1.4.1.311.2.1.25, SPC_CAB_DATA_OBJID

1.3.6.1.4.1.311.2.1.26, SPC_MINIMAL_CRITERIA_OBJID

1.3.6.1.4.1.311.2.1.27, SPC_FINANCIAL_CRITERIA_OBJID

1.3.6.1.4.1.311.2.1.28, SPC_LINK_OBJID

1.3.6.1.4.1.311.2.1.29, SPC_HASH_INFO_OBJID

1.3.6.1.4.1.311.2.1.30, SPC_SIPINFO_OBJID

1.3.6.1.4.1.311.2.2, CTL for Software Publishers Trusted CAs

1.3.6.1.4.1.311.2.2.1, szOID_TRUSTED_CODESIGNING_CA_LIST

1.3.6.1.4.1.311.2.2.2, szOID_TRUSTED_CLIENT_AUTH_CA_LIST

1.3.6.1.4.1.311.2.2.3, szOID_TRUSTED_SERVER_AUTH_CA_LIST

1.3.6.1.4.1.311.3, Time Stamping

1.3.6.1.4.1.311.3.2.1, SPC_TIME_STAMP_REQUEST_OBJID

1.3.6.1.4.1.311.3.3.1, RFC3161 Counter Signature

1.3.6.1.4.1.311.4, Permissions

1.3.6.1.4.1.311.10.3.1, certTrustListSigning

1.3.6.1.4.1.311.10.3.2, timeStampSigning

1.3.6.1.4.1.311.10.3.3, serverGatedCrypto

1.3.6.1.4.1.311.10.3.3.1, szOID_SERIALIZED

1.3.6.1.4.1.311.10.3.4, encryptedFileSystem

1.3.6.1.4.1.311.10.3.4.1, szOID_EFS_RECOVERY

1.3.6.1.4.1.311.10.3.5, szOID_WHQL_CRYPTO

1.3.6.1.4.1.311.10.3.6, szOID_NT5_CRYPTO

1.3.6.1.4.1.311.10.3.7, szOID_OEM_WHQL_CRYPTO

1.3.6.1.4.1.311.10.3.8, szOID_EMBEDDED_NT_CRYPTO

1.3.6.1.4.1.311.10.3.9, szOID_ROOT_LIST_SIGNER

1.3.6.1.4.1.311.10.3.10, szOID_KP_QUALIFIED_SUBORDINATION

1.3.6.1.4.1.311.10.3.11, szOID_KP_KEY_RECOVERY

1.3.6.1.4.1.311.10.3.12, szOID_KP_DOCUMENT_SIGNING

1.3.6.1.4.1.311.10.4.1, yesnoTrustAttr

1.3.6.1.4.1.311.10.5.1, szOID_DRM

1.3.6.1.4.1.311.10.5.2, szOID_DRM_INDIVIDUALIZATION

1.3.6.1.4.1.311.10.6.1, szOID_LICENSES

1.3.6.1.4.1.311.10.6.2, szOID_LICENSE_SERVER

1.3.6.1.4.1.311.10.7, szOID_MICROSOFT_RDN_PREFIX

1.3.6.1.4.1.311.10.7.1, szOID_KEYID_RDN

1.3.6.1.4.1.311.10.8.1, szOID_REMOVE_CERTIFICATE

1.3.6.1.4.1.311.10.9.1, szOID_CROSS_CERT_DIST_POINTS

1.3.6.1.4.1.311.10, Crypto 2.0

1.3.6.1.4.1.311.10.1, certTrustList

1.3.6.1.4.1.311.10.1.1, szOID_SORTED_CTL

1.3.6.1.4.1.311.10.10, Microsoft CMC OIDs

1.3.6.1.4.1.311.10.10.1, szOID_CMC_ADD_ATTRIBUTES

1.3.6.1.4.1.311.10.11, Microsoft certificate property OIDs

1.3.6.1.4.1.311.10.11.1, szOID_CERT_PROP_ID_PREFIX

1.3.6.1.4.1.311.10.12, CryptUI

1.3.6.1.4.1.311.10.12.1, szOID_ANY_APPLICATION_POLICY

1.3.6.1.4.1.311.12, Catalog

1.3.6.1.4.1.311.12.1.1, szOID_CATALOG_LIST

1.3.6.1.4.1.311.12.1.2, szOID_CATALOG_LIST_MEMBER

1.3.6.1.4.1.311.12.1.3, szOID_CATALOG_LIST_MEMBER_V2

1.3.6.1.4.1.311.12.2.1, CAT_NAMEVALUE_OBJID

1.3.6.1.4.1.311.12.2.2, CAT_MEMBERINFO_OBJID

1.3.6.1.4.1.311.12.2.3, CAT_MEMBERINFO2_OBJID

1.3.6.1.4.1.311.13, Microsoft PKCS10 OIDs

1.3.6.1.4.1.311.13.1, szOID_RENEWAL_CERTIFICATE

1.3.6.1.4.1.311.13.2.1, szOID_ENROLLMENT_NAME_VALUE_PAIR

1.3.6.1.4.1.311.13.2.2, szOID_ENROLLMENT_CSP_PROVIDER

1.3.6.1.4.1.311.13.2.3, OS Version

1.3.6.1.4.1.311.15, Microsoft Java

1.3.6.1.4.1.311.16, Microsoft Outlook/Exchange

1.3.6.1.4.1.311.16.4, Outlook Express

1.3.6.1.4.1.311.17, Microsoft PKCS12 attributes

1.3.6.1.4.1.311.17.1, szOID_LOCAL_MACHINE_KEYSET

1.3.6.1.4.1.311.18, Microsoft Hydra

1.3.6.1.4.1.311.19, Microsoft ISPU Test

1.3.6.1.4.1.311.20, Microsoft Enrollment Infrastructure

1.3.6.1.4.1.311.20.1, szOID_AUTO_ENROLL_CTL_USAGE

1.3.6.1.4.1.311.20.2, szOID_ENROLL_CERTTYPE_EXTENSION

1.3.6.1.4.1.311.20.2.1, szOID_ENROLLMENT_AGENT

1.3.6.1.4.1.311.20.2.2, szOID_KP_SMARTCARD_LOGON

1.3.6.1.4.1.311.20.2.3, szOID_NT_PRINCIPAL_NAME

1.3.6.1.4.1.311.20.3, szOID_CERT_MANIFOLD

1.3.6.1.4.1.311.21, Microsoft CertSrv Infrastructure

1.3.6.1.4.1.311.21.1, szOID_CERTSRV_CA_VERSION

1.3.6.1.4.1.311.21.20, Client Information

1.3.6.1.4.1.311.25, Microsoft Directory Service

1.3.6.1.4.1.311.25.1, szOID_NTDS_REPLICATION

1.3.6.1.4.1.311.30, IIS

1.3.6.1.4.1.311.31, Windows updates and service packs

1.3.6.1.4.1.311.31.1, szOID_PRODUCT_UPDATE

1.3.6.1.4.1.311.40, Fonts

1.3.6.1.4.1.311.41, Microsoft Licensing and Registration

1.3.6.1.4.1.311.42, Microsoft Corporate PKI (ITG)

1.3.6.1.4.1.311.60.2.1.3, Country

1.3.6.1.4.1.311.60.2.1.2, State or province

1.3.6.1.4.1.311.88, CAPICOM

1.3.6.1.4.1.311.88.1, CAPICOM Version

1.3.6.1.4.1.311.88.2, CAPICOM Attribute

1.3.6.1.4.1.311.88.2.1, CAPICOM Document Name

1.3.6.1.4.1.311.88.2.2, CAPICOM Document Description

1.3.6.1.4.1.311.88.3, CAPICOM Encrypted Data

1.3.6.1.4.1.311.88.3.1, CAPICOM Encrypted Content

1.3.6.1.4.1.11129.2.4.2, Signed Certificate Timestamp List

1.3.6.1.5.5.7, pkix

1.3.6.1.5.5.7.1, privateExtension

1.3.6.1.5.5.7.1.1, authorityInfoAccess

1.3.6.1.5.5.7.12.2, CMC Data

1.3.6.1.5.5.7.2, policyQualifierIds

1.3.6.1.5.5.7.2.1, CPS

1.3.6.1.5.5.7.2.2, User Notice

1.3.6.1.5.5.7.3, keyPurpose

1.3.6.1.5.5.7.3.1, serverAuth

1.3.6.1.5.5.7.3.2, clientAuth

1.3.6.1.5.5.7.3.3, codeSigning

1.3.6.1.5.5.7.3.4, emailProtection

1.3.6.1.5.5.7.3.5, ipsecEndSystem

1.3.6.1.5.5.7.3.6, ipsecTunnel

1.3.6.1.5.5.7.3.7, ipsecUser

1.3.6.1.5.5.7.3.8, timeStamping

1.3.6.1.5.5.7.4, cmpInformationTypes

1.3.6.1.5.5.7.4.1, caProtEncCert

1.3.6.1.5.5.7.4.2, signKeyPairTypes

1.3.6.1.5.5.7.4.3, encKeyPairTypes

1.3.6.1.5.5.7.4.4, preferredSymmAlg

1.3.6.1.5.5.7.4.5, caKeyUpdateInfo

1.3.6.1.5.5.7.4.6, currentCRL

1.3.6.1.5.5.7.48.1, ocsp

1.3.6.1.5.5.7.48.2, caIssuers

1.3.6.1.5.5.8.1.1, HMAC-MD5

1.3.6.1.5.5.8.1.2, HMAC-SHA

2.16.840.1.101.2.1.1.1, sdnsSignatureAlgorithm

2.16.840.1.101.2.1.1.10, mosaicKeyManagementAlgorithm

2.16.840.1.101.2.1.1.11, sdnsKMandSigAlgorithm

2.16.840.1.101.2.1.1.12, mosaicKMandSigAlgorithm

2.16.840.1.101.2.1.1.13, SuiteASignatureAlgorithm

2.16.840.1.101.2.1.1.14, SuiteAConfidentialityAlgorithm

2.16.840.1.101.2.1.1.15, SuiteAIntegrityAlgorithm

2.16.840.1.101.2.1.1.16, SuiteATokenProtectionAlgorithm

2.16.840.1.101.2.1.1.17, SuiteAKeyManagementAlgorithm

2.16.840.1.101.2.1.1.18, SuiteAKMandSigAlgorithm

2.16.840.1.101.2.1.1.19, mosaicUpdatedSigAlgorithm

2.16.840.1.101.2.1.1.2, mosaicSignatureAlgorithm

2.16.840.1.101.2.1.1.20, mosaicKMandUpdSigAlgorithms

2.16.840.1.101.2.1.1.21, mosaicUpdatedIntegAlgorithm

2.16.840.1.101.2.1.1.22, mosaicKeyEncryptionAlgorithm

2.16.840.1.101.2.1.1.3, sdnsConfidentialityAlgorithm

2.16.840.1.101.2.1.1.4, mosaicConfidentialityAlgorithm

2.16.840.1.101.2.1.1.5, sdnsIntegrityAlgorithm

2.16.840.1.101.2.1.1.6, mosaicIntegrityAlgorithm

2.16.840.1.101.2.1.1.7, sdnsTokenProtectionAlgorithm

2.16.840.1.101.2.1.1.8, mosaicTokenProtectionAlgorithm

2.16.840.1.101.2.1.1.9, sdnsKeyManagementAlgorithm

2.16.840.1.113730.1, cert-extension

2.16.840.1.113730.1.1, netscape-cert-type

2.16.840.1.113730.1.10, EntityLogo

2.16.840.1.113730.1.11, UserPicture

2.16.840.1.113730.1.12, netscape-ssl-server-name

2.16.840.1.113730.1.13, netscape-comment

2.16.840.1.113730.1.2, netscape-base-url

2.16.840.1.113730.1.3, netscape-revocation-url

2.16.840.1.113730.1.4, netscape-ca-revocation-url

2.16.840.1.113730.1.7, netscape-cert-renewal-url

2.16.840.1.113730.1.8, netscape-ca-policy-url

2.16.840.1.113730.1.9, HomePage-url

2.16.840.1.113730.2, data-type

2.16.840.1.113730.2.1, GIF

2.16.840.1.113730.2.2, JPEG

2.16.840.1.113730.2.3, URL

2.16.840.1.113730.2.4, HTML

2.16.840.1.113730.2.5, netscape-cert-sequence

2.16.840.1.113730.2.6, netscape-cert-url

2.16.840.1.113730.3, LDAPv3

2.16.840.1.113730.3.4.9, LDAP_CONTROL_VLVREQUEST

2.16.840.1.113730.3.4.10, LDAP_CONTROL_VLVRESPONSE

2.16.840.1.113730.4.1, serverGatedCrypto

2.16.840.1.113733.1.6.3, Unknown Verisign extension

2.16.840.1.113733.1.6.6, Unknown Verisign extension

2.16.840.1.113733.1.7.1.1, Verisign certificatePolicy

2.16.840.1.113733.1.7.1.1.1, Unknown Verisign policy qualifier

2.16.840.1.113733.1.7.1.1.2, Unknown Verisign policy qualifier

2.16.840.1.113733.1.7.23.3, Class 3 Certificate Policy

2.23.133, TCPA

2.23.133.1, tcpa_specVersion

2.23.133.2, tcpa_attribute

2.23.133.2.1, tcpa_at_tpmManufacturer

2.23.133.2.10, tcpa_at_securityQualities

2.23.133.2.11, tcpa_at_tpmProtectionProfile

2.23.133.2.12, tcpa_at_tpmSecurityTarget

2.23.133.2.13, tcpa_at_foundationProtectionProfile

2.23.133.2.14, tcpa_at_foundationSecurityTarget

2.23.133.2.15, tcpa_at_tpmIdLabel

2.23.133.2.2, tcpa_at_tpmModel

2.23.133.2.3, tcpa_at_tpmVersion

2.23.133.2.4, tcpa_at_platformManufacturer

2.23.133.2.5, tcpa_at_platformModel

2.23.133.2.6, tcpa_at_platformVersion

2.23.133.2.7, tcpa_at_componentManufacturer

2.23.133.2.8, tcpa_at_componentModel

2.23.133.2.9, tcpa_at_componentVersion

2.23.133.3, tcpa_protocol

2.23.133.3.1, tcpa_prtt_tpmIdProtocol

2.23.42.0, contentType

2.23.42.0.0, PANData

2.23.42.0.1, PANToken

2.23.42.0.2, PANOnly

2.23.42.1, msgExt

2.23.42.10, national

2.23.42.10.192, Japan

2.23.42.2, field

2.23.42.2.0, fullName

2.23.42.2.1, givenName

2.23.42.2.10, amount

2.23.42.2.2, familyName

2.23.42.2.3, birthFamilyName

2.23.42.2.4, placeName

2.23.42.2.5, identificationNumber

2.23.42.2.6, month

2.23.42.2.7, date

2.23.42.2.7.11, accountNumber

2.23.42.2.7.12, passPhrase

2.23.42.2.8, address

2.23.42.2.9, telephone

2.23.42.3, attribute

2.23.42.3.0, cert

2.23.42.3.0.0, rootKeyThumb

2.23.42.3.0.1, additionalPolicy

2.23.42.4, algorithm

2.23.42.5, policy

2.23.42.5.0, root

2.23.42.6, module

2.23.42.7, certExt

2.23.42.7.0, hashedRootKey

2.23.42.7.1, certificateType

2.23.42.7.2, merchantData

2.23.42.7.3, cardCertRequired

2.23.42.7.4, tunneling

2.23.42.7.5, setExtensions

2.23.42.7.6, setQualifier

2.23.42.8, brand

2.23.42.8.1, IATA-ATA

2.23.42.8.30, Diners

2.23.42.8.34, AmericanExpress

2.23.42.8.4, VISA

2.23.42.8.5, MasterCard

2.23.42.8.6011, Novus

2.23.42.9, vendor

2.23.42.9.0, GlobeSet

2.23.42.9.1, IBM

2.23.42.9.10, Griffin

2.23.42.9.11, Certicom

2.23.42.9.12, OSS

2.23.42.9.13, TenthMountain

2.23.42.9.14, Antares

2.23.42.9.15, ECC

2.23.42.9.16, Maithean

2.23.42.9.17, Netscape

2.23.42.9.18, Verisign

2.23.42.9.19, BlueMoney

2.23.42.9.2, CyberCash

2.23.42.9.20, Lacerte

2.23.42.9.21, Fujitsu

2.23.42.9.22, eLab

2.23.42.9.23, Entrust

2.23.42.9.24, VIAnet

2.23.42.9.25, III

2.23.42.9.26, OpenMarket

2.23.42.9.27, Lexem

2.23.42.9.28, Intertrader

2.23.42.9.29, Persimmon

2.23.42.9.3, Terisa

2.23.42.9.30, NABLE

2.23.42.9.31, espace-net

2.23.42.9.32, Hitachi

2.23.42.9.33, Microsoft

2.23.42.9.34, NEC

2.23.42.9.35, Mitsubishi

2.23.42.9.36, NCR

2.23.42.9.37, e-COMM

2.23.42.9.38, Gemplus

2.23.42.9.4, RSADSI

2.23.42.9.5, VeriFone

2.23.42.9.6, TrinTech

2.23.42.9.7, BankGate

2.23.42.9.8, GTE

2.23.42.9.9, CompuSource

2.5.29.1, authorityKeyIdentifier

2.5.29.10, basicConstraints

2.5.29.11, nameConstraints

2.5.29.12, policyConstraints

2.5.29.13, basicConstraints

2.5.29.14, subjectKeyIdentifier

2.5.29.15, keyUsage

2.5.29.16, privateKeyUsagePeriod

2.5.29.17, subjectAltName

2.5.29.18, issuerAltName

2.5.29.19, basicConstraints

2.5.29.2, keyAttributes

2.5.29.20, cRLNumber

2.5.29.21, cRLReason

2.5.29.22, expirationDate

2.5.29.23, instructionCode

2.5.29.24, invalidityDate

2.5.29.25, cRLDistributionPoints

2.5.29.26, issuingDistributionPoint

2.5.29.27, deltaCRLIndicator

2.5.29.28, issuingDistributionPoint

2.5.29.29, certificateIssuer

2.5.29.3, certificatePolicies

2.5.29.30, nameConstraints

2.5.29.31, cRLDistributionPoints

2.5.29.32, certificatePolicies

2.5.29.33, policyMappings

2.5.29.34, policyConstraints

2.5.29.35, authorityKeyIdentifier

2.5.29.36, policyConstraints

2.5.29.37, extKeyUsage

2.5.29.4, keyUsageRestriction

2.5.29.5, policyMapping

2.5.29.6, subtreesConstraint

2.5.29.7, subjectAltName

2.5.29.8, issuerAltName

2.5.29.9, subjectDirectoryAttributes

2.5.4.0, objectClass

2.5.4.1, aliasObjectName

2.5.4.10, organizationName

2.5.4.11, organizationalUnitName

2.5.4.12, title

2.5.4.13, description

2.5.4.14, searchGuide

2.5.4.15, businessCategory

2.5.4.16, postalAddress

2.5.4.17, postalCode

2.5.4.18, postOfficeBox

2.5.4.19, physicalDeliveryOfficeName

2.5.4.2, knowledgeInformation

2.5.4.20, telephoneNumber

2.5.4.21, telexNumber

2.5.4.22, teletexTerminalIdentifier

2.5.4.23, facsimileTelephoneNumber

2.5.4.24, x121Address

2.5.4.25, internationalISDNNumber

2.5.4.26, registeredAddress

2.5.4.27, destinationIndicator

2.5.4.28, preferredDeliveryMehtod

2.5.4.29, presentationAddress

2.5.4.3, commonName

2.5.4.30, supportedApplicationContext

2.5.4.31, member

2.5.4.32, owner

2.5.4.33, roleOccupant

2.5.4.34, seeAlso

2.5.4.35, userPassword

2.5.4.36, userCertificate

2.5.4.37, caCertificate

2.5.4.38, authorityRevocationList

2.5.4.39, certificateRevocationList

2.5.4.4, surname

2.5.4.40, crossCertificatePair

2.5.4.41, givenName

2.5.4.42, givenName

2.5.4.5, serialNumber

2.5.4.52, supportedAlgorithms

2.5.4.53, deltaRevocationList

2.5.4.58, crossCertificatePair

2.5.4.6, countryName

2.5.4.7, localityName

2.5.4.8, stateOrProvinceName

2.5.4.9, streetAddress

2.5.8, X.500-Algorithms

2.5.8.1, X.500-Alg-Encryption

2.5.8.1.1, rsa



# All GmSSL OIDs

```
0.3.4401.5.3.1.9.1 CAMELLIA-128-ECB
0.3.4401.5.3.1.9.10 CAMELLIA-128-CMAC
0.3.4401.5.3.1.9.21 CAMELLIA-192-ECB
0.3.4401.5.3.1.9.23 CAMELLIA-192-OFB
0.3.4401.5.3.1.9.24 CAMELLIA-192-CFB
0.3.4401.5.3.1.9.26 CAMELLIA-192-GCM
0.3.4401.5.3.1.9.27 CAMELLIA-192-CCM
0.3.4401.5.3.1.9.29 CAMELLIA-192-CTR
0.3.4401.5.3.1.9.3 CAMELLIA-128-OFB
0.3.4401.5.3.1.9.30 CAMELLIA-192-CMAC
0.3.4401.5.3.1.9.4 CAMELLIA-128-CFB
0.3.4401.5.3.1.9.41 CAMELLIA-256-ECB
0.3.4401.5.3.1.9.43 CAMELLIA-256-OFB
0.3.4401.5.3.1.9.44 CAMELLIA-256-CFB
0.3.4401.5.3.1.9.46 CAMELLIA-256-GCM
0.3.4401.5.3.1.9.47 CAMELLIA-256-CCM
0.3.4401.5.3.1.9.49 CAMELLIA-256-CTR
0.3.4401.5.3.1.9.50 CAMELLIA-256-CMAC
0.3.4401.5.3.1.9.6 CAMELLIA-128-GCM
0.3.4401.5.3.1.9.7 CAMELLIA-128-CCM
0.3.4401.5.3.1.9.9 CAMELLIA-128-CTR
0.9 data
0.9.2342 pss
0.9.2342.19200300 ucl
0.9.2342.19200300.100 pilot
0.9.2342.19200300.100.1 pilotAttributeType
0.9.2342.19200300.100.1.1 UID
0.9.2342.19200300.100.1.10 manager
0.9.2342.19200300.100.1.11 documentIdentifier
0.9.2342.19200300.100.1.12 documentTitle
0.9.2342.19200300.100.1.13 documentVersion
0.9.2342.19200300.100.1.14 documentAuthor
0.9.2342.19200300.100.1.15 documentLocation
0.9.2342.19200300.100.1.2 textEncodedORAddress
0.9.2342.19200300.100.1.20 homeTelephoneNumber
0.9.2342.19200300.100.1.21 secretary
0.9.2342.19200300.100.1.22 otherMailbox
0.9.2342.19200300.100.1.23 lastModifiedTime
0.9.2342.19200300.100.1.24 lastModifiedBy
0.9.2342.19200300.100.1.25 DC
0.9.2342.19200300.100.1.26 aRecord
0.9.2342.19200300.100.1.27 pilotAttributeType27
0.9.2342.19200300.100.1.28 mXRecord
0.9.2342.19200300.100.1.29 nSRecord
0.9.2342.19200300.100.1.3 mail
0.9.2342.19200300.100.1.30 sOARecord
0.9.2342.19200300.100.1.31 cNAMERecord
0.9.2342.19200300.100.1.37 associatedDomain
0.9.2342.19200300.100.1.38 associatedName
0.9.2342.19200300.100.1.39 homePostalAddress
0.9.2342.19200300.100.1.4 info
0.9.2342.19200300.100.1.40 personalTitle
0.9.2342.19200300.100.1.41 mobileTelephoneNumber
0.9.2342.19200300.100.1.42 pagerTelephoneNumber
0.9.2342.19200300.100.1.43 friendlyCountryName
0.9.2342.19200300.100.1.44 uid
0.9.2342.19200300.100.1.45 organizationalStatus
0.9.2342.19200300.100.1.46 janetMailbox
0.9.2342.19200300.100.1.47 mailPreferenceOption
0.9.2342.19200300.100.1.48 buildingName
0.9.2342.19200300.100.1.49 dSAQuality
0.9.2342.19200300.100.1.5 favouriteDrink
0.9.2342.19200300.100.1.50 singleLevelQuality
0.9.2342.19200300.100.1.51 subtreeMinimumQuality
0.9.2342.19200300.100.1.52 subtreeMaximumQuality
0.9.2342.19200300.100.1.53 personalSignature
0.9.2342.19200300.100.1.54 dITRedirect
0.9.2342.19200300.100.1.55 audio
0.9.2342.19200300.100.1.56 documentPublisher
0.9.2342.19200300.100.1.6 roomNumber
0.9.2342.19200300.100.1.7 photo
0.9.2342.19200300.100.1.8 userClass
0.9.2342.19200300.100.1.9 host
0.9.2342.19200300.100.10 pilotGroups
0.9.2342.19200300.100.3 pilotAttributeSyntax
0.9.2342.19200300.100.3.4 iA5StringSyntax
0.9.2342.19200300.100.3.5 caseIgnoreIA5StringSyntax
0.9.2342.19200300.100.4 pilotObjectClass
0.9.2342.19200300.100.4.13 domain
0.9.2342.19200300.100.4.14 rFC822localPart
0.9.2342.19200300.100.4.15 dNSDomain
0.9.2342.19200300.100.4.17 domainRelatedObject
0.9.2342.19200300.100.4.18 friendlyCountry
0.9.2342.19200300.100.4.19 simpleSecurityObject
0.9.2342.19200300.100.4.20 pilotOrganization
0.9.2342.19200300.100.4.21 pilotDSA
0.9.2342.19200300.100.4.22 qualityLabelledData
0.9.2342.19200300.100.4.3 pilotObject
0.9.2342.19200300.100.4.4 pilotPerson
0.9.2342.19200300.100.4.5 account
0.9.2342.19200300.100.4.6 document
0.9.2342.19200300.100.4.7 room
0.9.2342.19200300.100.4.9 documentSeries
1.0.10118.3.0.55 whirlpool
1.2 member-body
1.2.156 ISO-CN
1.2.156.10197 oscca
1.2.156.10197.1 sm-scheme
1.2.156.10197.1.101.1 SM6-ECB
1.2.156.10197.1.101.2 SM6-CBC
1.2.156.10197.1.101.3 SM6-OFB
1.2.156.10197.1.101.4 SM6-CFB
1.2.156.10197.1.102.1 SM1-ECB
1.2.156.10197.1.102.2 SM1-CBC
1.2.156.10197.1.102.3 SM1-OFB
1.2.156.10197.1.102.4 SM1-CFB
1.2.156.10197.1.102.5 SM1-CFB1
1.2.156.10197.1.102.6 SM1-CFB8
1.2.156.10197.1.103.1 SSF33-ECB
1.2.156.10197.1.103.2 SSF33-CBC
1.2.156.10197.1.103.3 SSF33-OFB
1.2.156.10197.1.103.4 SSF33-CFB
1.2.156.10197.1.103.5 SSF33-CFB1
1.2.156.10197.1.103.6 SSF33-CFB8
1.2.156.10197.1.104.1 SMS4-ECB
1.2.156.10197.1.104.10 SMS4-XTS
1.2.156.10197.1.104.100 SMS4-OCB
1.2.156.10197.1.104.11 SMS4-WRAP
1.2.156.10197.1.104.12 SMS4-WRAP-PAD
1.2.156.10197.1.104.2 SMS4-CBC
1.2.156.10197.1.104.3 SMS4-OFB
1.2.156.10197.1.104.4 SMS4-CFB
1.2.156.10197.1.104.5 SMS4-CFB1
1.2.156.10197.1.104.6 SMS4-CFB8
1.2.156.10197.1.104.7 SMS4-CTR
1.2.156.10197.1.104.8 SMS4-GCM
1.2.156.10197.1.104.9 SMS4-CCM
1.2.156.10197.1.201 SM5
1.2.156.10197.1.301 sm2p256v1
1.2.156.10197.1.301.1 (null)
1.2.156.10197.1.301.1 sm2sign
1.2.156.10197.1.301.101 wapip192v1
1.2.156.10197.1.301.3 sm2encrypt
1.2.156.10197.1.301.3.1 sm2encrypt-recommendedParameters
1.2.156.10197.1.301.3.2 sm2encrypt-specifiedParameters
1.2.156.10197.1.302 id-sm9PublicKey
1.2.156.10197.1.302.1 sm9sign
1.2.156.10197.1.302.2 sm9keyagreement
1.2.156.10197.1.302.3 sm9encrypt
1.2.156.10197.1.401 SM3
1.2.156.10197.1.401.2 HMAC-SM3
1.2.156.10197.1.501 SM2Sign-with-SM3
1.2.156.10197.1.502 SM2Sign-with-SHA1
1.2.156.10197.1.503 SM2Sign-with-SHA256
1.2.156.10197.1.504 SM2Sign-with-SHA511
1.2.156.10197.1.505 SM2Sign-with-SHA224
1.2.156.10197.1.506 SM2Sign-with-SHA384
1.2.156.10197.1.507 SM2Sign-with-RMD160
1.2.156.10197.1.800 ZUC
1.2.392.200011.61.1.1.1.2 CAMELLIA-128-CBC
1.2.392.200011.61.1.1.1.3 CAMELLIA-192-CBC
1.2.392.200011.61.1.1.1.4 CAMELLIA-256-CBC
1.2.392.200011.61.1.1.3.2 id-camellia128-wrap
1.2.392.200011.61.1.1.3.3 id-camellia192-wrap
1.2.392.200011.61.1.1.3.4 id-camellia256-wrap
1.2.410.200004 (null)
1.2.410.200004 (null)
1.2.410.200004 KISA
1.2.410.200004.1.3 SEED-ECB
1.2.410.200004.1.4 SEED-CBC
1.2.410.200004.1.5 SEED-CFB
1.2.410.200004.1.6 SEED-OFB
1.2.643.100.1 OGRN
1.2.643.100.111 subjectSignTool
1.2.643.100.112 issuerSignTool
1.2.643.100.3 SNILS
1.2.643.2.2 cryptopro
1.2.643.2.2.10 id-HMACGostR3411-94
1.2.643.2.2.14.0 id-Gost28147-89-None-KeyMeshing
1.2.643.2.2.14.1 id-Gost28147-89-CryptoPro-KeyMeshing
1.2.643.2.2.19 gost2001
1.2.643.2.2.20 gost94
1.2.643.2.2.20.1 id-GostR3410-94-a
1.2.643.2.2.20.2 id-GostR3410-94-aBis
1.2.643.2.2.20.3 id-GostR3410-94-b
1.2.643.2.2.20.4 id-GostR3410-94-bBis
1.2.643.2.2.21 gost89
1.2.643.2.2.22 gost-mac
1.2.643.2.2.23 prf-gostr3411-94
1.2.643.2.2.3 id-GostR3411-94-with-GostR3410-2001
1.2.643.2.2.30.0 id-GostR3411-94-TestParamSet
1.2.643.2.2.30.1 id-GostR3411-94-CryptoProParamSet
1.2.643.2.2.31.0 id-Gost28147-89-TestParamSet
1.2.643.2.2.31.1 id-Gost28147-89-CryptoPro-A-ParamSet
1.2.643.2.2.31.2 id-Gost28147-89-CryptoPro-B-ParamSet
1.2.643.2.2.31.3 id-Gost28147-89-CryptoPro-C-ParamSet
1.2.643.2.2.31.4 id-Gost28147-89-CryptoPro-D-ParamSet
1.2.643.2.2.31.5 id-Gost28147-89-CryptoPro-Oscar-1-1-ParamSet
1.2.643.2.2.31.6 id-Gost28147-89-CryptoPro-Oscar-1-0-ParamSet
1.2.643.2.2.31.7 id-Gost28147-89-CryptoPro-RIC-1-ParamSet
1.2.643.2.2.32.0 id-GostR3410-94-TestParamSet
1.2.643.2.2.32.2 id-GostR3410-94-CryptoPro-A-ParamSet
1.2.643.2.2.32.3 id-GostR3410-94-CryptoPro-B-ParamSet
1.2.643.2.2.32.4 id-GostR3410-94-CryptoPro-C-ParamSet
1.2.643.2.2.32.5 id-GostR3410-94-CryptoPro-D-ParamSet
1.2.643.2.2.33.1 id-GostR3410-94-CryptoPro-XchA-ParamSet
1.2.643.2.2.33.2 id-GostR3410-94-CryptoPro-XchB-ParamSet
1.2.643.2.2.33.3 id-GostR3410-94-CryptoPro-XchC-ParamSet
1.2.643.2.2.35.0 id-GostR3410-2001-TestParamSet
1.2.643.2.2.35.1 id-GostR3410-2001-CryptoPro-A-ParamSet
1.2.643.2.2.35.2 id-GostR3410-2001-CryptoPro-B-ParamSet
1.2.643.2.2.35.3 id-GostR3410-2001-CryptoPro-C-ParamSet
1.2.643.2.2.36.0 id-GostR3410-2001-CryptoPro-XchA-ParamSet
1.2.643.2.2.36.1 id-GostR3410-2001-CryptoPro-XchB-ParamSet
1.2.643.2.2.4 id-GostR3411-94-with-GostR3410-94
1.2.643.2.2.9 md_gost94
1.2.643.2.2.98 id-GostR3410-2001DH
1.2.643.2.2.99 id-GostR3410-94DH
1.2.643.2.9 cryptocom
1.2.643.2.9.1.3.3 id-GostR3411-94-with-GostR3410-94-cc
1.2.643.2.9.1.3.4 id-GostR3411-94-with-GostR3410-2001-cc
1.2.643.2.9.1.5.3 gost94cc
1.2.643.2.9.1.5.4 gost2001cc
1.2.643.2.9.1.6.1 id-Gost28147-89-cc
1.2.643.2.9.1.8.1 id-GostR3410-2001-ParamSet-cc
1.2.643.3.131.1.1 INN
1.2.643.7.1 id-tc26
1.2.643.7.1.1 id-tc26-algorithms
1.2.643.7.1.1.1 id-tc26-sign
1.2.643.7.1.1.1.1 gost2012_256
1.2.643.7.1.1.1.2 gost2012_512
1.2.643.7.1.1.2 id-tc26-digest
1.2.643.7.1.1.2.2 md_gost12_256
1.2.643.7.1.1.2.3 md_gost12_512
1.2.643.7.1.1.3 id-tc26-signwithdigest
1.2.643.7.1.1.3.2 id-tc26-signwithdigest-gost3410-2012-256
1.2.643.7.1.1.3.3 id-tc26-signwithdigest-gost3410-2012-512
1.2.643.7.1.1.4 id-tc26-mac
1.2.643.7.1.1.4.1 id-tc26-hmac-gost-3411-2012-256
1.2.643.7.1.1.4.2 id-tc26-hmac-gost-3411-2012-512
1.2.643.7.1.1.5 id-tc26-cipher
1.2.643.7.1.1.6 id-tc26-agreement
1.2.643.7.1.1.6.1 id-tc26-agreement-gost-3410-2012-256
1.2.643.7.1.1.6.2 id-tc26-agreement-gost-3410-2012-512
1.2.643.7.1.2 id-tc26-constants
1.2.643.7.1.2.1 id-tc26-sign-constants
1.2.643.7.1.2.1.2 id-tc26-gost-3410-2012-512-constants
1.2.643.7.1.2.1.2.0 id-tc26-gost-3410-2012-512-paramSetTest
1.2.643.7.1.2.1.2.1 id-tc26-gost-3410-2012-512-paramSetA
1.2.643.7.1.2.1.2.2 id-tc26-gost-3410-2012-512-paramSetB
1.2.643.7.1.2.2 id-tc26-digest-constants
1.2.643.7.1.2.5 id-tc26-cipher-constants
1.2.643.7.1.2.5.1 id-tc26-gost-28147-constants
1.2.643.7.1.2.5.1.1 id-tc26-gost-28147-param-Z
1.2.840 ISO-US
1.2.840.1.114334.1.1.1 type1curve
1.2.840.1.114334.1.1.2 type2curve
1.2.840.1.114334.1.1.3 type3curve
1.2.840.1.114334.1.1.4 type4curve
1.2.840.1.114334.1.2.1 bfibe
1.2.840.1.114334.1.2.2 bb1
1.2.840.1.114334.1.3.1 tate-pairing
1.2.840.1.114334.1.3.2 weil-pairing
1.2.840.1.114334.1.3.3 ate-pairing
1.2.840.1.114334.1.3.4 r-ate-pairing
1.2.840.10040 X9-57
1.2.840.10040.2.1 holdInstructionNone
1.2.840.10040.2.2 holdInstructionCallIssuer
1.2.840.10040.2.3 holdInstructionReject
1.2.840.10040.4 X9cm
1.2.840.10040.4.1 DSA
1.2.840.10040.4.3 DSA-SHA1
1.2.840.10045 ansi-X9-62
1.2.840.10045.1.1 prime-field
1.2.840.10045.1.2 characteristic-two-field
1.2.840.10045.1.2.3 id-characteristic-two-basis
1.2.840.10045.1.2.3.1 onBasis
1.2.840.10045.1.2.3.2 tpBasis
1.2.840.10045.1.2.3.3 ppBasis
1.2.840.10045.2.1 id-ecPublicKey
1.2.840.10045.3.0.1 c2pnb163v1
1.2.840.10045.3.0.10 c2pnb208w1
1.2.840.10045.3.0.11 c2tnb239v1
1.2.840.10045.3.0.12 c2tnb239v2
1.2.840.10045.3.0.13 c2tnb239v3
1.2.840.10045.3.0.14 c2onb239v4
1.2.840.10045.3.0.15 c2onb239v5
1.2.840.10045.3.0.16 c2pnb272w1
1.2.840.10045.3.0.17 c2pnb304w1
1.2.840.10045.3.0.18 c2tnb359v1
1.2.840.10045.3.0.19 c2pnb368w1
1.2.840.10045.3.0.2 c2pnb163v2
1.2.840.10045.3.0.20 c2tnb431r1
1.2.840.10045.3.0.3 c2pnb163v3
1.2.840.10045.3.0.4 c2pnb176v1
1.2.840.10045.3.0.5 c2tnb191v1
1.2.840.10045.3.0.6 c2tnb191v2
1.2.840.10045.3.0.7 c2tnb191v3
1.2.840.10045.3.0.8 c2onb191v4
1.2.840.10045.3.0.9 c2onb191v5
1.2.840.10045.3.1.1 prime192v1
1.2.840.10045.3.1.2 prime192v2
1.2.840.10045.3.1.3 prime192v3
1.2.840.10045.3.1.4 prime239v1
1.2.840.10045.3.1.5 prime239v2
1.2.840.10045.3.1.6 prime239v3
1.2.840.10045.3.1.7 prime256v1
1.2.840.10045.4 id-ecSigType
1.2.840.10045.4.1 ecdsa-with-SHA1
1.2.840.10045.4.2 ecdsa-with-Recommended
1.2.840.10045.4.3 ecdsa-with-Specified
1.2.840.10045.4.3.1 ecdsa-with-SHA224
1.2.840.10045.4.3.2 ecdsa-with-SHA256
1.2.840.10045.4.3.3 ecdsa-with-SHA384
1.2.840.10045.4.3.4 ecdsa-with-SHA512
1.2.840.10046.2.1 dhpublicnumber
1.2.840.113533.7.66.10 CAST5-CBC
1.2.840.113533.7.66.12 pbeWithMD5AndCast5CBC
1.2.840.113533.7.66.13 id-PasswordBasedMAC
1.2.840.113533.7.66.30 id-DHBasedMac
1.2.840.113549 rsadsi
1.2.840.113549.1 pkcs
1.2.840.113549.1.1 pkcs1
1.2.840.113549.1.1.1 rsaEncryption
1.2.840.113549.1.1.10 RSASSA-PSS
1.2.840.113549.1.1.11 RSA-SHA256
1.2.840.113549.1.1.12 RSA-SHA384
1.2.840.113549.1.1.13 RSA-SHA512
1.2.840.113549.1.1.14 RSA-SHA224
1.2.840.113549.1.1.2 RSA-MD2
1.2.840.113549.1.1.3 RSA-MD4
1.2.840.113549.1.1.4 RSA-MD5
1.2.840.113549.1.1.5 RSA-SHA1
1.2.840.113549.1.1.6 rsaOAEPEncryptionSET
1.2.840.113549.1.1.7 RSAES-OAEP
1.2.840.113549.1.1.8 MGF1
1.2.840.113549.1.1.9 PSPECIFIED
1.2.840.113549.1.12.1.1 PBE-SHA1-RC4-128
1.2.840.113549.1.12.1.2 PBE-SHA1-RC4-40
1.2.840.113549.1.12.1.3 PBE-SHA1-3DES
1.2.840.113549.1.12.1.4 PBE-SHA1-2DES
1.2.840.113549.1.12.1.5 PBE-SHA1-RC2-128
1.2.840.113549.1.12.1.6 PBE-SHA1-RC2-40
1.2.840.113549.1.12.10.1.1 keyBag
1.2.840.113549.1.12.10.1.2 pkcs8ShroudedKeyBag
1.2.840.113549.1.12.10.1.3 certBag
1.2.840.113549.1.12.10.1.4 crlBag
1.2.840.113549.1.12.10.1.5 secretBag
1.2.840.113549.1.12.10.1.6 safeContentsBag
1.2.840.113549.1.3 pkcs3
1.2.840.113549.1.3.1 dhKeyAgreement
1.2.840.113549.1.5 pkcs5
1.2.840.113549.1.5.1 PBE-MD2-DES
1.2.840.113549.1.5.10 PBE-SHA1-DES
1.2.840.113549.1.5.11 PBE-SHA1-RC2-64
1.2.840.113549.1.5.12 PBKDF2
1.2.840.113549.1.5.13 PBES2
1.2.840.113549.1.5.14 PBMAC1
1.2.840.113549.1.5.3 PBE-MD5-DES
1.2.840.113549.1.5.4 PBE-MD2-RC2-64
1.2.840.113549.1.5.6 PBE-MD5-RC2-64
1.2.840.113549.1.7 pkcs7
1.2.840.113549.1.7.1 pkcs7-data
1.2.840.113549.1.7.2 pkcs7-signedData
1.2.840.113549.1.7.3 pkcs7-envelopedData
1.2.840.113549.1.7.4 pkcs7-signedAndEnvelopedData
1.2.840.113549.1.7.5 pkcs7-digestData
1.2.840.113549.1.7.6 pkcs7-encryptedData
1.2.840.113549.1.9 pkcs9
1.2.840.113549.1.9.1 emailAddress
1.2.840.113549.1.9.14 extReq
1.2.840.113549.1.9.15 SMIME-CAPS
1.2.840.113549.1.9.16 SMIME
1.2.840.113549.1.9.16.0 id-smime-mod
1.2.840.113549.1.9.16.0.1 id-smime-mod-cms
1.2.840.113549.1.9.16.0.2 id-smime-mod-ess
1.2.840.113549.1.9.16.0.3 id-smime-mod-oid
1.2.840.113549.1.9.16.0.4 id-smime-mod-msg-v3
1.2.840.113549.1.9.16.0.5 id-smime-mod-ets-eSignature-88
1.2.840.113549.1.9.16.0.6 id-smime-mod-ets-eSignature-97
1.2.840.113549.1.9.16.0.7 id-smime-mod-ets-eSigPolicy-88
1.2.840.113549.1.9.16.0.8 id-smime-mod-ets-eSigPolicy-97
1.2.840.113549.1.9.16.1 id-smime-ct
1.2.840.113549.1.9.16.1.1 id-smime-ct-receipt
1.2.840.113549.1.9.16.1.19 id-smime-ct-contentCollection
1.2.840.113549.1.9.16.1.2 id-smime-ct-authData
1.2.840.113549.1.9.16.1.23 id-smime-ct-authEnvelopedData
1.2.840.113549.1.9.16.1.27 id-ct-asciiTextWithCRLF
1.2.840.113549.1.9.16.1.28 id-ct-xml
1.2.840.113549.1.9.16.1.3 id-smime-ct-publishCert
1.2.840.113549.1.9.16.1.4 id-smime-ct-TSTInfo
1.2.840.113549.1.9.16.1.5 id-smime-ct-TDTInfo
1.2.840.113549.1.9.16.1.6 id-smime-ct-contentInfo
1.2.840.113549.1.9.16.1.7 id-smime-ct-DVCSRequestData
1.2.840.113549.1.9.16.1.8 id-smime-ct-DVCSResponseData
1.2.840.113549.1.9.16.1.9 id-smime-ct-compressedData
1.2.840.113549.1.9.16.2 id-smime-aa
1.2.840.113549.1.9.16.2.1 id-smime-aa-receiptRequest
1.2.840.113549.1.9.16.2.10 id-smime-aa-contentReference
1.2.840.113549.1.9.16.2.11 id-smime-aa-encrypKeyPref
1.2.840.113549.1.9.16.2.12 id-smime-aa-signingCertificate
1.2.840.113549.1.9.16.2.13 id-smime-aa-smimeEncryptCerts
1.2.840.113549.1.9.16.2.14 id-smime-aa-timeStampToken
1.2.840.113549.1.9.16.2.15 id-smime-aa-ets-sigPolicyId
1.2.840.113549.1.9.16.2.16 id-smime-aa-ets-commitmentType
1.2.840.113549.1.9.16.2.17 id-smime-aa-ets-signerLocation
1.2.840.113549.1.9.16.2.18 id-smime-aa-ets-signerAttr
1.2.840.113549.1.9.16.2.19 id-smime-aa-ets-otherSigCert
1.2.840.113549.1.9.16.2.2 id-smime-aa-securityLabel
1.2.840.113549.1.9.16.2.20 id-smime-aa-ets-contentTimestamp
1.2.840.113549.1.9.16.2.21 id-smime-aa-ets-CertificateRefs
1.2.840.113549.1.9.16.2.22 id-smime-aa-ets-RevocationRefs
1.2.840.113549.1.9.16.2.23 id-smime-aa-ets-certValues
1.2.840.113549.1.9.16.2.24 id-smime-aa-ets-revocationValues
1.2.840.113549.1.9.16.2.25 id-smime-aa-ets-escTimeStamp
1.2.840.113549.1.9.16.2.26 id-smime-aa-ets-certCRLTimestamp
1.2.840.113549.1.9.16.2.27 id-smime-aa-ets-archiveTimeStamp
1.2.840.113549.1.9.16.2.28 id-smime-aa-signatureType
1.2.840.113549.1.9.16.2.29 id-smime-aa-dvcs-dvc
1.2.840.113549.1.9.16.2.3 id-smime-aa-mlExpandHistory
1.2.840.113549.1.9.16.2.4 id-smime-aa-contentHint
1.2.840.113549.1.9.16.2.5 id-smime-aa-msgSigDigest
1.2.840.113549.1.9.16.2.6 id-smime-aa-encapContentType
1.2.840.113549.1.9.16.2.7 id-smime-aa-contentIdentifier
1.2.840.113549.1.9.16.2.8 id-smime-aa-macValue
1.2.840.113549.1.9.16.2.9 id-smime-aa-equivalentLabels
1.2.840.113549.1.9.16.3 id-smime-alg
1.2.840.113549.1.9.16.3.1 id-smime-alg-ESDHwith3DES
1.2.840.113549.1.9.16.3.2 id-smime-alg-ESDHwithRC2
1.2.840.113549.1.9.16.3.3 id-smime-alg-3DESwrap
1.2.840.113549.1.9.16.3.4 id-smime-alg-RC2wrap
1.2.840.113549.1.9.16.3.5 id-smime-alg-ESDH
1.2.840.113549.1.9.16.3.6 id-smime-alg-CMS3DESwrap
1.2.840.113549.1.9.16.3.7 id-smime-alg-CMSRC2wrap
1.2.840.113549.1.9.16.3.8 ZLIB
1.2.840.113549.1.9.16.3.9 id-alg-PWRI-KEK
1.2.840.113549.1.9.16.4 id-smime-cd
1.2.840.113549.1.9.16.4.1 id-smime-cd-ldap
1.2.840.113549.1.9.16.5 id-smime-spq
1.2.840.113549.1.9.16.5.1 id-smime-spq-ets-sqt-uri
1.2.840.113549.1.9.16.5.2 id-smime-spq-ets-sqt-unotice
1.2.840.113549.1.9.16.6 id-smime-cti
1.2.840.113549.1.9.16.6.1 id-smime-cti-ets-proofOfOrigin
1.2.840.113549.1.9.16.6.2 id-smime-cti-ets-proofOfReceipt
1.2.840.113549.1.9.16.6.3 id-smime-cti-ets-proofOfDelivery
1.2.840.113549.1.9.16.6.4 id-smime-cti-ets-proofOfSender
1.2.840.113549.1.9.16.6.5 id-smime-cti-ets-proofOfApproval
1.2.840.113549.1.9.16.6.6 id-smime-cti-ets-proofOfCreation
1.2.840.113549.1.9.2 unstructuredName
1.2.840.113549.1.9.20 friendlyName
1.2.840.113549.1.9.21 localKeyID
1.2.840.113549.1.9.22.1 x509Certificate
1.2.840.113549.1.9.22.2 sdsiCertificate
1.2.840.113549.1.9.23.1 x509Crl
1.2.840.113549.1.9.3 contentType
1.2.840.113549.1.9.4 messageDigest
1.2.840.113549.1.9.5 signingTime
1.2.840.113549.1.9.6 countersignature
1.2.840.113549.1.9.7 challengePassword
1.2.840.113549.1.9.8 unstructuredAddress
1.2.840.113549.1.9.9 extendedCertificateAttributes
1.2.840.113549.2.10 hmacWithSHA384
1.2.840.113549.2.11 hmacWithSHA512
1.2.840.113549.2.2 MD2
1.2.840.113549.2.4 MD4
1.2.840.113549.2.5 MD5
1.2.840.113549.2.6 hmacWithMD5
1.2.840.113549.2.7 hmacWithSHA1
1.2.840.113549.2.8 hmacWithSHA224
1.2.840.113549.2.9 hmacWithSHA256
1.2.840.113549.3.10 DES-CDMF
1.2.840.113549.3.2 RC2-CBC
1.2.840.113549.3.4 RC4
1.2.840.113549.3.7 DES-EDE3-CBC
1.2.840.113549.3.8 RC5-CBC
1.3 ORG
1.3 identified-organization
1.3.101.1.4.1 SXNetID
1.3.101.110 X25519
1.3.101.111 X448
1.3.132 certicom-arc
1.3.132.0.1 sect163k1
1.3.132.0.10 secp256k1
1.3.132.0.15 sect163r2
1.3.132.0.16 sect283k1
1.3.132.0.17 sect283r1
1.3.132.0.2 sect163r1
1.3.132.0.22 sect131r1
1.3.132.0.23 sect131r2
1.3.132.0.24 sect193r1
1.3.132.0.25 sect193r2
1.3.132.0.26 sect233k1
1.3.132.0.27 sect233r1
1.3.132.0.28 secp128r1
1.3.132.0.29 secp128r2
1.3.132.0.3 sect239k1
1.3.132.0.30 secp160r2
1.3.132.0.31 secp192k1
1.3.132.0.32 secp224k1
1.3.132.0.33 secp224r1
1.3.132.0.34 secp384r1
1.3.132.0.35 secp521r1
1.3.132.0.36 sect409k1
1.3.132.0.37 sect409r1
1.3.132.0.38 sect571k1
1.3.132.0.39 sect571r1
1.3.132.0.4 sect113r1
1.3.132.0.5 sect113r2
1.3.132.0.6 secp112r1
1.3.132.0.7 secp112r2
1.3.132.0.8 secp160r1
1.3.132.0.9 secp160k1
1.3.132.1 secg-scheme
1.3.132.1.11.0 dhSinglePass-stdDH-sha224kdf-scheme
1.3.132.1.11.1 dhSinglePass-stdDH-sha256kdf-scheme
1.3.132.1.11.2 dhSinglePass-stdDH-sha384kdf-scheme
1.3.132.1.11.3 dhSinglePass-stdDH-sha512kdf-scheme
1.3.132.1.14.0 dhSinglePass-cofactorDH-sha224kdf-scheme
1.3.132.1.14.1 dhSinglePass-cofactorDH-sha256kdf-scheme
1.3.132.1.14.2 dhSinglePass-cofactorDH-sha384kdf-scheme
1.3.132.1.14.3 dhSinglePass-cofactorDH-sha512kdf-scheme
1.3.132.1.17.0 x9-63-kdf
1.3.132.1.17.1 nist-concatenation-kdf
1.3.132.1.17.2 tls-kdf
1.3.132.1.17.3 ikev2-kdf
1.3.132.1.18 xor-in-ecies
1.3.132.1.19 tdes-cbc-in-ecies
1.3.132.1.20.0 aes128-cbc-in-ecies
1.3.132.1.20.1 aes192-cbc-in-ecies
1.3.132.1.20.2 aes256-cbc-in-ecies
1.3.132.1.21.0 aes128-ctr-in-ecies
1.3.132.1.21.1 aes192-ctr-in-ecies
1.3.132.1.21.2 aes256-ctr-in-ecies
1.3.132.1.22 hmac-full-ecies
1.3.132.1.23 hmac-half-ecies
1.3.132.1.24.0 cmac-aes128-ecies
1.3.132.1.24.1 cmac-aes192-ecies
1.3.132.1.24.2 (null)
1.3.132.1.24.2 cmac-aes256-ecies
1.3.132.1.7 ecies-recommendedParameters
1.3.132.1.8 ecies-specifiedParameters
1.3.133.16.840.63.0.2 dhSinglePass-stdDH-sha1kdf-scheme
1.3.133.16.840.63.0.3 dhSinglePass-cofactorDH-sha1kdf-scheme
1.3.14.3.2 algorithm
1.3.14.3.2.11 rsaSignature
1.3.14.3.2.12 DSA-old
1.3.14.3.2.13 DSA-SHA
1.3.14.3.2.15 RSA-SHA
1.3.14.3.2.17 DES-EDE
1.3.14.3.2.18 SHA
1.3.14.3.2.26 SHA1
1.3.14.3.2.27 DSA-SHA1-old
1.3.14.3.2.29 RSA-SHA1-2
1.3.14.3.2.3 RSA-NP-MD5
1.3.14.3.2.6 DES-ECB
1.3.14.3.2.7 DES-CBC
1.3.14.3.2.8 DES-OFB
1.3.14.3.2.9 DES-CFB
1.3.36.3.2.1 (null)
1.3.36.3.2.1 RIPEMD160
1.3.36.3.3.1.2 RSA-RIPEMD160
1.3.36.3.3.2.8.1.1.1 brainpoolP160r1
1.3.36.3.3.2.8.1.1.10 brainpoolP320t1
1.3.36.3.3.2.8.1.1.11 brainpoolP384r1
1.3.36.3.3.2.8.1.1.12 brainpoolP384t1
1.3.36.3.3.2.8.1.1.13 brainpoolP512r1
1.3.36.3.3.2.8.1.1.14 brainpoolP512t1
1.3.36.3.3.2.8.1.1.2 brainpoolP160t1
1.3.36.3.3.2.8.1.1.3 brainpoolP192r1
1.3.36.3.3.2.8.1.1.4 brainpoolP192t1
1.3.36.3.3.2.8.1.1.5 brainpoolP224r1
1.3.36.3.3.2.8.1.1.6 brainpoolP224t1
1.3.36.3.3.2.8.1.1.7 brainpoolP256r1
1.3.36.3.3.2.8.1.1.8 brainpoolP256t1
1.3.36.3.3.2.8.1.1.9 brainpoolP320r1
1.3.6 DOD
1.3.6.1 IANA
1.3.6.1.1 directory
1.3.6.1.2 mgmt
1.3.6.1.3 experimental
1.3.6.1.4 private
1.3.6.1.4.1 enterprises
1.3.6.1.4.1.11129.2.4.2 ct_precert_scts
1.3.6.1.4.1.11129.2.4.3 ct_precert_poison
1.3.6.1.4.1.11129.2.4.4 ct_precert_signer
1.3.6.1.4.1.11129.2.4.5 ct_cert_scts
1.3.6.1.4.1.11591.4.11 id-scrypt
1.3.6.1.4.1.1466.344 dcobject
1.3.6.1.4.1.1722.12.2.1.16 BLAKE2b512
1.3.6.1.4.1.1722.12.2.2.8 BLAKE2s256
1.3.6.1.4.1.188.7.1.1.2 IDEA-CBC
1.3.6.1.4.1.3029.1.2 BF-CBC
1.3.6.1.4.1.311.10.3.1 msCTLSign
1.3.6.1.4.1.311.10.3.3 msSGC
1.3.6.1.4.1.311.10.3.4 msEFS
1.3.6.1.4.1.311.17.1 CSPName
1.3.6.1.4.1.311.17.2 LocalKeySet
1.3.6.1.4.1.311.2.1.14 msExtReq
1.3.6.1.4.1.311.2.1.21 msCodeInd
1.3.6.1.4.1.311.2.1.22 msCodeCom
1.3.6.1.4.1.311.20.2.2 msSmartcardLogin
1.3.6.1.4.1.311.20.2.3 msUPN
1.3.6.1.4.1.311.60.2.1.1 jurisdictionL
1.3.6.1.4.1.311.60.2.1.2 jurisdictionST
1.3.6.1.4.1.311.60.2.1.3 jurisdictionC
1.3.6.1.4.1.49549.1 CPK
1.3.6.1.4.1.49549.21 paillier
1.3.6.1.5 security
1.3.6.1.5.2.3 id-pkinit
1.3.6.1.5.2.3.4 pkInitClientAuth
1.3.6.1.5.2.3.5 pkInitKDC
1.3.6.1.5.5.7 PKIX
1.3.6.1.5.5.7.0 id-pkix-mod
1.3.6.1.5.5.7.0.1 id-pkix1-explicit-88
1.3.6.1.5.5.7.0.10 id-mod-qualified-cert-88
1.3.6.1.5.5.7.0.11 id-mod-qualified-cert-93
1.3.6.1.5.5.7.0.12 id-mod-attribute-cert
1.3.6.1.5.5.7.0.13 id-mod-timestamp-protocol
1.3.6.1.5.5.7.0.14 id-mod-ocsp
1.3.6.1.5.5.7.0.15 id-mod-dvcs
1.3.6.1.5.5.7.0.16 id-mod-cmp2000
1.3.6.1.5.5.7.0.2 id-pkix1-implicit-88
1.3.6.1.5.5.7.0.3 id-pkix1-explicit-93
1.3.6.1.5.5.7.0.4 id-pkix1-implicit-93
1.3.6.1.5.5.7.0.5 id-mod-crmf
1.3.6.1.5.5.7.0.6 id-mod-cmc
1.3.6.1.5.5.7.0.7 id-mod-kea-profile-88
1.3.6.1.5.5.7.0.8 id-mod-kea-profile-93
1.3.6.1.5.5.7.0.9 id-mod-cmp
1.3.6.1.5.5.7.1 id-pe
1.3.6.1.5.5.7.1.1 authorityInfoAccess
1.3.6.1.5.5.7.1.10 ac-proxying
1.3.6.1.5.5.7.1.11 subjectInfoAccess
1.3.6.1.5.5.7.1.14 proxyCertInfo
1.3.6.1.5.5.7.1.2 biometricInfo
1.3.6.1.5.5.7.1.24 tlsfeature
1.3.6.1.5.5.7.1.3 qcStatements
1.3.6.1.5.5.7.1.4 ac-auditEntity
1.3.6.1.5.5.7.1.5 ac-targeting
1.3.6.1.5.5.7.1.6 aaControls
1.3.6.1.5.5.7.1.7 sbgp-ipAddrBlock
1.3.6.1.5.5.7.1.8 sbgp-autonomousSysNum
1.3.6.1.5.5.7.1.9 sbgp-routerIdentifier
1.3.6.1.5.5.7.10 id-aca
1.3.6.1.5.5.7.10.1 id-aca-authenticationInfo
1.3.6.1.5.5.7.10.2 id-aca-accessIdentity
1.3.6.1.5.5.7.10.3 id-aca-chargingIdentity
1.3.6.1.5.5.7.10.4 id-aca-group
1.3.6.1.5.5.7.10.5 id-aca-role
1.3.6.1.5.5.7.10.6 id-aca-encAttrs
1.3.6.1.5.5.7.11 id-qcs
1.3.6.1.5.5.7.11.1 id-qcs-pkixQCSyntax-v1
1.3.6.1.5.5.7.12 id-cct
1.3.6.1.5.5.7.12.1 id-cct-crs
1.3.6.1.5.5.7.12.2 id-cct-PKIData
1.3.6.1.5.5.7.12.3 id-cct-PKIResponse
1.3.6.1.5.5.7.2 id-qt
1.3.6.1.5.5.7.2.1 id-qt-cps
1.3.6.1.5.5.7.2.2 id-qt-unotice
1.3.6.1.5.5.7.2.3 textNotice
1.3.6.1.5.5.7.21 id-ppl
1.3.6.1.5.5.7.21.0 id-ppl-anyLanguage
1.3.6.1.5.5.7.21.1 id-ppl-inheritAll
1.3.6.1.5.5.7.21.2 id-ppl-independent
1.3.6.1.5.5.7.3 id-kp
1.3.6.1.5.5.7.3.1 serverAuth
1.3.6.1.5.5.7.3.10 DVCS
1.3.6.1.5.5.7.3.17 ipsecIKE
1.3.6.1.5.5.7.3.18 capwapAC
1.3.6.1.5.5.7.3.19 capwapWTP
1.3.6.1.5.5.7.3.2 clientAuth
1.3.6.1.5.5.7.3.21 secureShellClient
1.3.6.1.5.5.7.3.22 secureShellServer
1.3.6.1.5.5.7.3.23 sendRouter
1.3.6.1.5.5.7.3.24 sendProxiedRouter
1.3.6.1.5.5.7.3.25 sendOwner
1.3.6.1.5.5.7.3.26 sendProxiedOwner
1.3.6.1.5.5.7.3.3 codeSigning
1.3.6.1.5.5.7.3.4 emailProtection
1.3.6.1.5.5.7.3.5 ipsecEndSystem
1.3.6.1.5.5.7.3.6 ipsecTunnel
1.3.6.1.5.5.7.3.7 ipsecUser
1.3.6.1.5.5.7.3.8 timeStamping
1.3.6.1.5.5.7.3.9 OCSPSigning
1.3.6.1.5.5.7.4 id-it
1.3.6.1.5.5.7.4.1 id-it-caProtEncCert
1.3.6.1.5.5.7.4.10 id-it-keyPairParamReq
1.3.6.1.5.5.7.4.11 id-it-keyPairParamRep
1.3.6.1.5.5.7.4.12 id-it-revPassphrase
1.3.6.1.5.5.7.4.13 id-it-implicitConfirm
1.3.6.1.5.5.7.4.14 id-it-confirmWaitTime
1.3.6.1.5.5.7.4.15 id-it-origPKIMessage
1.3.6.1.5.5.7.4.16 id-it-suppLangTags
1.3.6.1.5.5.7.4.2 id-it-signKeyPairTypes
1.3.6.1.5.5.7.4.3 id-it-encKeyPairTypes
1.3.6.1.5.5.7.4.4 id-it-preferredSymmAlg
1.3.6.1.5.5.7.4.5 id-it-caKeyUpdateInfo
1.3.6.1.5.5.7.4.6 id-it-currentCRL
1.3.6.1.5.5.7.4.7 id-it-unsupportedOIDs
1.3.6.1.5.5.7.4.8 id-it-subscriptionRequest
1.3.6.1.5.5.7.4.9 id-it-subscriptionResponse
1.3.6.1.5.5.7.48 id-ad
1.3.6.1.5.5.7.48.1 OCSP
1.3.6.1.5.5.7.48.1.1 basicOCSPResponse
1.3.6.1.5.5.7.48.1.10 path
1.3.6.1.5.5.7.48.1.11 trustRoot
1.3.6.1.5.5.7.48.1.2 Nonce
1.3.6.1.5.5.7.48.1.3 CrlID
1.3.6.1.5.5.7.48.1.4 acceptableResponses
1.3.6.1.5.5.7.48.1.5 noCheck
1.3.6.1.5.5.7.48.1.6 archiveCutoff
1.3.6.1.5.5.7.48.1.7 serviceLocator
1.3.6.1.5.5.7.48.1.8 extendedStatus
1.3.6.1.5.5.7.48.1.9 valid
1.3.6.1.5.5.7.48.2 caIssuers
1.3.6.1.5.5.7.48.3 ad_timestamping
1.3.6.1.5.5.7.48.4 AD_DVCS
1.3.6.1.5.5.7.48.5 caRepository
1.3.6.1.5.5.7.5 id-pkip
1.3.6.1.5.5.7.5.1 id-regCtrl
1.3.6.1.5.5.7.5.1.1 id-regCtrl-regToken
1.3.6.1.5.5.7.5.1.2 id-regCtrl-authenticator
1.3.6.1.5.5.7.5.1.3 id-regCtrl-pkiPublicationInfo
1.3.6.1.5.5.7.5.1.4 id-regCtrl-pkiArchiveOptions
1.3.6.1.5.5.7.5.1.5 id-regCtrl-oldCertID
1.3.6.1.5.5.7.5.1.6 id-regCtrl-protocolEncrKey
1.3.6.1.5.5.7.5.2 id-regInfo
1.3.6.1.5.5.7.5.2.1 id-regInfo-utf8Pairs
1.3.6.1.5.5.7.5.2.2 id-regInfo-certReq
1.3.6.1.5.5.7.6 id-alg
1.3.6.1.5.5.7.6.1 id-alg-des40
1.3.6.1.5.5.7.6.2 id-alg-noSignature
1.3.6.1.5.5.7.6.3 id-alg-dh-sig-hmac-sha1
1.3.6.1.5.5.7.6.4 id-alg-dh-pop
1.3.6.1.5.5.7.7 id-cmc
1.3.6.1.5.5.7.7.1 id-cmc-statusInfo
1.3.6.1.5.5.7.7.10 id-cmc-decryptedPOP
1.3.6.1.5.5.7.7.11 id-cmc-lraPOPWitness
1.3.6.1.5.5.7.7.15 id-cmc-getCert
1.3.6.1.5.5.7.7.16 id-cmc-getCRL
1.3.6.1.5.5.7.7.17 id-cmc-revokeRequest
1.3.6.1.5.5.7.7.18 id-cmc-regInfo
1.3.6.1.5.5.7.7.19 id-cmc-responseInfo
1.3.6.1.5.5.7.7.2 id-cmc-identification
1.3.6.1.5.5.7.7.21 id-cmc-queryPending
1.3.6.1.5.5.7.7.22 id-cmc-popLinkRandom
1.3.6.1.5.5.7.7.23 id-cmc-popLinkWitness
1.3.6.1.5.5.7.7.24 id-cmc-confirmCertAcceptance
1.3.6.1.5.5.7.7.3 id-cmc-identityProof
1.3.6.1.5.5.7.7.4 id-cmc-dataReturn
1.3.6.1.5.5.7.7.5 id-cmc-transactionId
1.3.6.1.5.5.7.7.6 id-cmc-senderNonce
1.3.6.1.5.5.7.7.7 id-cmc-recipientNonce
1.3.6.1.5.5.7.7.8 id-cmc-addExtensions
1.3.6.1.5.5.7.7.9 id-cmc-encryptedPOP
1.3.6.1.5.5.7.8 id-on
1.3.6.1.5.5.7.8.1 id-on-personalData
1.3.6.1.5.5.7.8.3 id-on-permanentIdentifier
1.3.6.1.5.5.7.9 id-pda
1.3.6.1.5.5.7.9.1 id-pda-dateOfBirth
1.3.6.1.5.5.7.9.2 (null)
1.3.6.1.5.5.7.9.2 id-pda-placeOfBirth
1.3.6.1.5.5.7.9.3 id-pda-gender
1.3.6.1.5.5.7.9.4 id-pda-countryOfCitizenship
1.3.6.1.5.5.7.9.5 id-pda-countryOfResidence
1.3.6.1.5.5.8.1.1 HMAC-MD5
1.3.6.1.5.5.8.1.2 HMAC-SHA1
1.3.6.1.6 snmpv2
1.3.6.1.7 Mail
1.3.6.1.7.1 mime-mhs
1.3.6.1.7.1.1 mime-mhs-headings
1.3.6.1.7.1.1.1 id-hex-partial-message
1.3.6.1.7.1.1.2 id-hex-multipart-message
1.3.6.1.7.1.2 mime-mhs-bodies
2.16.840.1.101.3.4.1.1 AES-128-ECB
2.16.840.1.101.3.4.1.2 AES-128-CBC
2.16.840.1.101.3.4.1.21 AES-192-ECB
2.16.840.1.101.3.4.1.22 AES-192-CBC
2.16.840.1.101.3.4.1.23 AES-192-OFB
2.16.840.1.101.3.4.1.24 AES-192-CFB
2.16.840.1.101.3.4.1.25 id-aes192-wrap
2.16.840.1.101.3.4.1.26 id-aes192-GCM
2.16.840.1.101.3.4.1.27 id-aes192-CCM
2.16.840.1.101.3.4.1.28 id-aes192-wrap-pad
2.16.840.1.101.3.4.1.3 AES-128-OFB
2.16.840.1.101.3.4.1.4 AES-128-CFB
2.16.840.1.101.3.4.1.41 AES-256-ECB
2.16.840.1.101.3.4.1.42 AES-256-CBC
2.16.840.1.101.3.4.1.43 AES-256-OFB
2.16.840.1.101.3.4.1.44 AES-256-CFB
2.16.840.1.101.3.4.1.45 id-aes256-wrap
2.16.840.1.101.3.4.1.46 id-aes256-GCM
2.16.840.1.101.3.4.1.47 id-aes256-CCM
2.16.840.1.101.3.4.1.48 id-aes256-wrap-pad
2.16.840.1.101.3.4.1.5 id-aes128-wrap
2.16.840.1.101.3.4.1.6 id-aes128-GCM
2.16.840.1.101.3.4.1.7 id-aes128-CCM
2.16.840.1.101.3.4.1.8 id-aes128-wrap-pad
2.16.840.1.101.3.4.2.1 SHA256
2.16.840.1.101.3.4.2.2 SHA384
2.16.840.1.101.3.4.2.3 SHA512
2.16.840.1.101.3.4.2.4 SHA224
2.16.840.1.101.3.4.3.1 dsa_with_SHA224
2.16.840.1.101.3.4.3.2 dsa_with_SHA256
2.16.840.1.113730 Netscape
2.16.840.1.113730.1 nsCertExt
2.16.840.1.113730.1.1 nsCertType
2.16.840.1.113730.1.12 nsSslServerName
2.16.840.1.113730.1.13 nsComment
2.16.840.1.113730.1.2 nsBaseUrl
2.16.840.1.113730.1.3 nsRevocationUrl
2.16.840.1.113730.1.4 nsCaRevocationUrl
2.16.840.1.113730.1.7 nsRenewalUrl
2.16.840.1.113730.1.8 nsCaPolicyUrl
2.16.840.1.113730.2 nsDataType
2.16.840.1.113730.2.5 nsCertSequence
2.16.840.1.113730.4.1 nsSGC
2.23 international-organizations
2.23.42 id-set
2.23.42.0 set-ctype
2.23.42.0.0 setct-PANData
2.23.42.0.1 setct-PANToken
2.23.42.0.10 setct-AuthRevResBaggage
2.23.42.0.11 setct-CapTokenSeq
2.23.42.0.12 setct-PInitResData
2.23.42.0.13 setct-PI-TBS
2.23.42.0.14 setct-PResData
2.23.42.0.16 setct-AuthReqTBS
2.23.42.0.17 setct-AuthResTBS
2.23.42.0.18 setct-AuthResTBSX
2.23.42.0.19 setct-AuthTokenTBS
2.23.42.0.2 setct-PANOnly
2.23.42.0.20 setct-CapTokenData
2.23.42.0.21 setct-CapTokenTBS
2.23.42.0.22 setct-AcqCardCodeMsg
2.23.42.0.23 setct-AuthRevReqTBS
2.23.42.0.24 setct-AuthRevResData
2.23.42.0.25 setct-AuthRevResTBS
2.23.42.0.26 setct-CapReqTBS
2.23.42.0.27 setct-CapReqTBSX
2.23.42.0.28 setct-CapResData
2.23.42.0.29 setct-CapRevReqTBS
2.23.42.0.3 setct-OIData
2.23.42.0.30 setct-CapRevReqTBSX
2.23.42.0.31 setct-CapRevResData
2.23.42.0.32 setct-CredReqTBS
2.23.42.0.33 setct-CredReqTBSX
2.23.42.0.34 setct-CredResData
2.23.42.0.35 setct-CredRevReqTBS
2.23.42.0.36 setct-CredRevReqTBSX
2.23.42.0.37 setct-CredRevResData
2.23.42.0.38 setct-PCertReqData
2.23.42.0.39 setct-PCertResTBS
2.23.42.0.4 setct-PI
2.23.42.0.40 setct-BatchAdminReqData
2.23.42.0.41 setct-BatchAdminResData
2.23.42.0.42 setct-CardCInitResTBS
2.23.42.0.43 setct-MeAqCInitResTBS
2.23.42.0.44 setct-RegFormResTBS
2.23.42.0.45 setct-CertReqData
2.23.42.0.46 setct-CertReqTBS
2.23.42.0.47 setct-CertResData
2.23.42.0.48 setct-CertInqReqTBS
2.23.42.0.49 setct-ErrorTBS
2.23.42.0.5 setct-PIData
2.23.42.0.50 setct-PIDualSignedTBE
2.23.42.0.51 setct-PIUnsignedTBE
2.23.42.0.52 setct-AuthReqTBE
2.23.42.0.53 setct-AuthResTBE
2.23.42.0.54 setct-AuthResTBEX
2.23.42.0.55 setct-AuthTokenTBE
2.23.42.0.56 setct-CapTokenTBE
2.23.42.0.57 setct-CapTokenTBEX
2.23.42.0.58 setct-AcqCardCodeMsgTBE
2.23.42.0.59 setct-AuthRevReqTBE
2.23.42.0.6 setct-PIDataUnsigned
2.23.42.0.60 setct-AuthRevResTBE
2.23.42.0.61 setct-AuthRevResTBEB
2.23.42.0.62 setct-CapReqTBE
2.23.42.0.63 setct-CapReqTBEX
2.23.42.0.64 setct-CapResTBE
2.23.42.0.65 setct-CapRevReqTBE
2.23.42.0.66 setct-CapRevReqTBEX
2.23.42.0.67 setct-CapRevResTBE
2.23.42.0.68 setct-CredReqTBE
2.23.42.0.69 setct-CredReqTBEX
2.23.42.0.7 setct-HODInput
2.23.42.0.70 setct-CredResTBE
2.23.42.0.71 setct-CredRevReqTBE
2.23.42.0.72 setct-CredRevReqTBEX
2.23.42.0.73 setct-CredRevResTBE
2.23.42.0.74 setct-BatchAdminReqTBE
2.23.42.0.75 setct-BatchAdminResTBE
2.23.42.0.76 setct-RegFormReqTBE
2.23.42.0.77 setct-CertReqTBE
2.23.42.0.78 setct-CertReqTBEX
2.23.42.0.79 setct-CertResTBE
2.23.42.0.8 setct-AuthResBaggage
2.23.42.0.80 setct-CRLNotificationTBS
2.23.42.0.81 setct-CRLNotificationResTBS
2.23.42.0.82 setct-BCIDistributionTBS
2.23.42.0.9 setct-AuthRevReqBaggage
2.23.42.1 set-msgExt
2.23.42.1.1 setext-genCrypt
2.23.42.1.3 setext-miAuth
2.23.42.1.4 setext-pinSecure
2.23.42.1.5 setext-pinAny
2.23.42.1.7 setext-track2
2.23.42.1.8 setext-cv
2.23.42.3 set-attr
2.23.42.3.0 setAttr-Cert
2.23.42.3.0.0 set-rootKeyThumb
2.23.42.3.0.1 set-addPolicy
2.23.42.3.1 setAttr-PGWYcap
2.23.42.3.2 setAttr-TokenType
2.23.42.3.2.1 setAttr-Token-EMV
2.23.42.3.2.2 setAttr-Token-B0Prime
2.23.42.3.3 setAttr-IssCap
2.23.42.3.3.3 setAttr-IssCap-CVM
2.23.42.3.3.3.1 setAttr-GenCryptgrm
2.23.42.3.3.4 setAttr-IssCap-T2
2.23.42.3.3.4.1 setAttr-T2Enc
2.23.42.3.3.4.2 setAttr-T2cleartxt
2.23.42.3.3.5 setAttr-IssCap-Sig
2.23.42.3.3.5.1 setAttr-TokICCsig
2.23.42.3.3.5.2 setAttr-SecDevSig
2.23.42.5 set-policy
2.23.42.5.0 set-policy-root
2.23.42.7 set-certExt
2.23.42.7.0 setCext-hashedRoot
2.23.42.7.1 setCext-certType
2.23.42.7.10 setCext-TokenType
2.23.42.7.11 setCext-IssuerCapabilities
2.23.42.7.2 setCext-merchData
2.23.42.7.3 setCext-cCertRequired
2.23.42.7.4 setCext-tunneling
2.23.42.7.5 setCext-setExt
2.23.42.7.6 setCext-setQualf
2.23.42.7.7 setCext-PGWYcapabilities
2.23.42.7.8 setCext-TokenIdentifier
2.23.42.7.9 setCext-Track2Data
2.23.42.8 set-brand
2.23.42.8.1 set-brand-IATA-ATA
2.23.42.8.30 set-brand-Diners
2.23.42.8.34 set-brand-AmericanExpress
2.23.42.8.35 set-brand-JCB
2.23.42.8.4 set-brand-Visa
2.23.42.8.5 set-brand-MasterCard
2.23.42.8.6011 set-brand-Novus
2.23.43 wap
2.23.43.1 wap-wsg
2.23.43.1.4.1 wap-wsg-idm-ecid-wtls1
2.23.43.1.4.10 wap-wsg-idm-ecid-wtls10
2.23.43.1.4.11 wap-wsg-idm-ecid-wtls11
2.23.43.1.4.12 wap-wsg-idm-ecid-wtls12
2.23.43.1.4.3 wap-wsg-idm-ecid-wtls3
2.23.43.1.4.4 wap-wsg-idm-ecid-wtls4
2.23.43.1.4.5 wap-wsg-idm-ecid-wtls5
2.23.43.1.4.6 wap-wsg-idm-ecid-wtls6
2.23.43.1.4.7 wap-wsg-idm-ecid-wtls7
2.23.43.1.4.8 wap-wsg-idm-ecid-wtls8
2.23.43.1.4.9 wap-wsg-idm-ecid-wtls9
2.5 X500
2.5.1.5 selected-attribute-types
2.5.1.5.55 clearance
2.5.29 id-ce
2.5.29.14 subjectKeyIdentifier
2.5.29.15 keyUsage
2.5.29.16 privateKeyUsagePeriod
2.5.29.17 subjectAltName
2.5.29.18 issuerAltName
2.5.29.19 basicConstraints
2.5.29.20 crlNumber
2.5.29.21 CRLReason
2.5.29.23 holdInstructionCode
2.5.29.24 invalidityDate
2.5.29.27 deltaCRL
2.5.29.28 issuingDistributionPoint
2.5.29.29 (null)
2.5.29.29 certificateIssuer
2.5.29.30 nameConstraints
2.5.29.31 crlDistributionPoints
2.5.29.32 certificatePolicies
2.5.29.32.0 anyPolicy
2.5.29.33 policyMappings
2.5.29.35 authorityKeyIdentifier
2.5.29.36 policyConstraints
2.5.29.37 extendedKeyUsage
2.5.29.37.0 anyExtendedKeyUsage
2.5.29.46 freshestCRL
2.5.29.54 inhibitAnyPolicy
2.5.29.55 targetInformation
2.5.29.56 noRevAvail
2.5.29.9 subjectDirectoryAttributes
2.5.4 X509
2.5.4.10 O
2.5.4.11 OU
2.5.4.12 title
2.5.4.13 description
2.5.4.14 searchGuide
2.5.4.15 businessCategory
2.5.4.16 postalAddress
2.5.4.17 postalCode
2.5.4.18 postOfficeBox
2.5.4.19 physicalDeliveryOfficeName
2.5.4.20 telephoneNumber
2.5.4.21 telexNumber
2.5.4.22 teletexTerminalIdentifier
2.5.4.23 facsimileTelephoneNumber
2.5.4.24 x121Address
2.5.4.25 internationaliSDNNumber
2.5.4.26 registeredAddress
2.5.4.27 destinationIndicator
2.5.4.28 preferredDeliveryMethod
2.5.4.29 presentationAddress
2.5.4.3 CN
2.5.4.30 supportedApplicationContext
2.5.4.31 member
2.5.4.32 owner
2.5.4.33 roleOccupant
2.5.4.34 seeAlso
2.5.4.35 userPassword
2.5.4.36 userCertificate
2.5.4.37 cACertificate
2.5.4.38 authorityRevocationList
2.5.4.39 certificateRevocationList
2.5.4.4 SN
2.5.4.40 crossCertificatePair
2.5.4.41 name
2.5.4.42 GN
2.5.4.43 initials
2.5.4.44 generationQualifier
2.5.4.45 x500UniqueIdentifier
2.5.4.46 dnQualifier
2.5.4.47 enhancedSearchGuide
2.5.4.48 protocolInformation
2.5.4.49 distinguishedName
2.5.4.5 serialNumber
2.5.4.50 uniqueMember
2.5.4.51 houseIdentifier
2.5.4.52 supportedAlgorithms
2.5.4.53 deltaRevocationList
2.5.4.54 dmdName
2.5.4.6 C
2.5.4.65 (null)
2.5.4.65 pseudonym
2.5.4.7 L
2.5.4.72 role
2.5.4.8 ST
2.5.4.9 street
2.5.8 X500algorithms
2.5.8.1.1 RSA
2.5.8.3.100 RSA-MDC2
2.5.8.3.101 MDC2
```