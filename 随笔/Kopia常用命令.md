## 新建库

1. 使用WebDAV连接

   ```shell
   kopia repository create webdav --url="http://yan:123456@192.168.1.10:5005/myrepo"
   ```

   

2. 使用SFTP连接

   ```sh
   kopia repository create sftp  --path /home/yan/myrepo --host 192.168.1.10 --username yan --sftp-password 123456  --known-hosts C:\Users\Yan\.ssh\known_hosts
   ```



3. 在本地磁盘创建

   ```sh
   kopia repository create filesystem --path D:\KopiaRepository
   ```

   



## 连接库

1. 使用WebDAV连接

   ```sh
   kopia repository connect webdav --url="http://yan:123456@192.168.1.10:5005/myrepo"
   ```

   

2. 使用SFTP连接

   ```sh
   kopia repository connect sftp  --path /home/yan/myrepo --host 192.168.1.10 --username yan --sftp-password 123456  --known-hosts C:\Users\Yan\.ssh\known_hosts
   ```

   

3. 连接本地磁盘中的库

   ```sh
   kopia repository connect filesystem --path D:\KopiaRepository
   ```

   





## 查看库状态

```sh
kopia repository status
```





## 快照操作

### 创建快照

```sh
kopia snapshot create  D:\myproject
```



### 查看快照列表及文件

```sh
kopia snapshot list
```

![image-20241012131108449](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20241012131108449.png)



### 还原快照到本地磁盘

```sh
kopia snapshot restore k1ad6a861838e7ac4658930e4544d8fdb d:\test
```

![image-20241012145034486](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20241012145034486.png)



## 断开库连接

```sh
kopia repository disconnect
```

