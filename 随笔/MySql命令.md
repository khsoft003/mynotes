

## 登录

```sh
[root@host]# mysql -u root -p   
Enter password:******  # 登录后进入终端
```





## 数据库操作



### 显示所有数据库

```sh
mysql> show databases;
```





### 创建数据库

```sh
mysql> create DATABASE ;
```



### 



## 数据库表操作



### 显示数据库中所有表

```sh
mysql> show tables;
```



### 表查询操作

```sh
mysql> select * from user;
```









## 常见问题



### [is not allowed to connect](https://www.cnblogs.com/eternityz/p/12243427.html)

今天使用本地连接远程Mysql，一直报java.sql.SQLException:null,message from server:"Host '' is not allowed to connect.

原因是：远程服务器不允许你的java程序访问它的数据库。所以，我们要对远程服务器进行设置，使它允许你进行连接。

#### 打开mysql控制台，

```
mysql -u root -p

然后输入密码
```

#### 一、输入：use mysql;

#### 二、输入：show tables;

#### 三、输入：select host from user;

#### 四、修改host字段的值为：%

```mysql
update user set host ='%' where user ='root';
```

#### 五、输入重新启动mysql服务