## openssl用法详解

OpenSSL 是一个开源项目，其组成主要包括一下三个组件：

- openssl：多用途的命令行工具
- libcrypto：加密算法库
- libssl：加密模块应用库，实现了ssl及tls

openssl可以实现：秘钥证书管理、对称加密和非对称加密 。







### 1、对称加密

对称加密需要使用的标准命令为 enc ，用法如下：

```sh
openssl enc -ciphername [-in filename] [-out filename] [-pass arg] [-e] [-d] [-a/-base64]
       [-A] [-k password] [-kfile filename] [-K key] [-iv IV] [-S salt] [-salt] [-nosalt] [-z] [-md]
       [-p] [-P] [-bufsize number] [-nopad] [-debug] [-none] [-engine id]
```

常用选项有：

- -in filename：指定要加密的文件存放路径

- -out filename：指定加密后的文件存放路径

- -salt：自动插入一个随机数作为文件内容加密，默认选项
- -e：可以指明一种加密算法，若不指的话将使用默认加密算法

- -d：解密，解密时也可以指定算法，若不指定则使用默认算法，但一定要与加密时的算法一致

- -a/-base64：使用-base64位编码格式



**加密示例**

```sh
 openssl enc -e -des -a -salt -in test.txt -out jiami
```

**解密示例**

```sh
openssl enc -d -des -a -salt -in jiami -out test_dec.txt
```





### 2、单向加密

单向加密需要使用的标准命令为 dgst ，用法如下：

```sh
openssl dgst [-md5|-md4|-md2|-sha1|-sha|-mdc2|-ripemd160|-dss1] [-c] [-d] [-hex] [-binary]
       [-out filename] [-sign filename] [-keyform arg] [-passin arg] [-verify filename] [-prverify
       filename] [-signature filename] [-hmac key] [file...]
```

常用选项有：

- [-md5|-md4|-md2|-sha1|-sha|-mdc2|-ripemd160|-dss1] ：指定一种加密算法

- -out filename：将加密的内容保存到指定文件中

示例如下：

![image-20210304132902900](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304132902900.png)

单向加密除了 openssl dgst 工具还有： md5sum，sha1sum，sha224sum，sha256sum ，sha384sum，sha512sum

![image-20210304133015897](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304133015897.png)





### 3、生成密码

生成密码需要使用的标准命令为 passwd ，用法如下：

```
openssl passwd [-crypt] [-1] [-apr1] [-salt string] [-in file] [-stdin] [-noverify] [-quiet] [-table] {password}
```

常用选项有：

- -1：使用md5加密算法
- -salt string：加入随机数，最多8位随机数
- -in file：对输入的文件内容进行加密
- -stdion：对标准输入的内容进行加密

示例如下：

![image-20210304134318691](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304134318691.png)







### 4、生成随机数

生成随机数需要用到的标准命令为 rand ，用法如下：

```
openssl rand [-out file] [-rand file(s)] [-base64] [-hex] num
```

常用选项有：

- -out file：将生成的随机数保存至指定文件中
- -base64：使用base64 编码格式
- -hex：使用16进制编码格式

示例如下：

![image-20210304134415224](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304134415224.png)



### 5、生成秘钥

首先需要先使用 genrsa 标准命令生成私钥，然后再使用 rsa 标准命令从私钥中提取公钥。

genrsa 的用法如下：

```
openssl genrsa [-out filename] [-passout arg] [-des] [-des3] [-idea] [-f4] [-3] [-rand file(s)] [-engine id] [numbits]
```

常用选项有：

-out filename：将生成的私钥保存至指定的文件中

-des|-des3|-idea：不同的加密算法

numbits：指定生成私钥的大小，默认是2048

一般情况下秘钥文件的权限一定要控制好，只能自己读写，因此可以使用 umask 命令设置生成的私钥权限，示例如下：

![image-20210304134936648](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304134936648.png)

![image-20210304135458409](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304135458409.png)

![image-20210304134956744](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304134956744.png)



ras 的用法如下：

```
openssl rsa [-inform PEM|NET|DER] [-outform PEM|NET|DER] [-in filename] [-passin arg] [-out filename] [-passout arg]
       [-sgckey] [-des] [-des3] [-idea] [-text] [-noout] [-modulus] [-check] [-pubin] [-pubout] [-engine id]
```

常用选项：

- -in filename：指明私钥文件
- -out filename：指明将提取出的公钥保存至指定文件中 
- -pubout：根据私钥提取出公钥 

示例如下：

![image-20210304135410037](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210304135410037.png)







## OpenSSL命令行加密、签名

https://www.cnblogs.com/ziwuxian/articles/14481529.html



## OpenSSL的Base64编码解码

https://www.cnblogs.com/ziwuxian/articles/14481622.html



## 参考文章

**openssl系列文章：**  [http://www.cnblogs.com/f-ck-need-u/p/7048359.html](http://www.cnblogs.com/f-ck-need-u/p/7048359.html#blogopenssl)

**openssl用法详解：**https://www.cnblogs.com/yangxiaolan/p/6256838.html

