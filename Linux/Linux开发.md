### 动态库查看文件信息

```sh
readelf -a libxxx.so
```





### 动态库查看函数方法

**方法一：**

~~~shell
nm -D libxxx.so
~~~

但这样能看到所有的导出，乱七八糟的很多，筛选用：

~~~shell
nm **.so | grep XX
~~~



**方法二：**

~~~shell
objdump -tT **.so
~~~





### 静态库查看函数方法

~~~shell
nm -g --defined-only libxxx.a
~~~



**内核符号表类型**

| 符号类型 | 名称      | 说明                                                         |
| -------- | --------- | ------------------------------------------------------------ |
| A        | Absolute  | 符号的值是绝对值，并且在进一步链接过程中不会被改变           |
| B        | BSS       | 符号在未初始化数据区或区（section）中，即在BSS段中           |
| C        | Common    | 符号是公共的。公共符号是未初始化的数据。在链接时，多个公共符号可能具有同一名称。如果该符号定义在其他地方，则公共符号被看作是未定义的引用 |
| D        | Data      | 符号在已初始化数据区中                                       |
| G        | Global    | 符号是在小对象已初始化数据区中的符号。某些目标文件的格式允许对小数据对象（例如一个全局整型变量）可进行更有效的访问 |
| I        | Inderect  | 符号是对另一个符号的间接引用                                 |
| N        | Debugging | 符号是一个调试符号                                           |
| R        | Read only | 符号在一个只读数据区中                                       |
| S        | Small     | 符号是小对象未初始化数据区中的符号                           |
| T        | Text      | 符号是代码区中的符号                                         |
| U        | Undefined | 符号是外部的，并且其值为0（未定义）                          |
| -        | Stabs     | 符号是a.out目标文件中的一个stab符号，用于保存调试信息        |
| ?        | Unknown   | 符号的类型未知，或者与具体文件格式有关                       |









### 查看静态库中文件

参考链接：https://blog.csdn.net/weixin_29660305/article/details/112948417

#### 查看静态库中文件

~~~shell
ar -t libxxx.a
~~~



#### 从静态库中解出文件

```sh
ar -x libxxx.a  ftbase.o
```



#### 删除静态库中文件

```sh
ar -d libxxx.a  ftbase.o
```





#### 生成静态库文件

```sh
ar –rc test.a test.o
```



#### 添加文件到静态库

```sh
ar -r liba.a b.o
```







### 查看可执行文件 .so的编译器 版本信息

~~~shell
strings xxx.so |grep GLIB 
strings xxx.so |grep GCC
~~~





### 查看共享库so版本号

缺点：只能获取主版本号

~~~shell
readelf -a ***.so
~~~



### 查看某个so的依赖 

```sh
objdump -x ** | grep NEEDED 
```

![image-20220409092550705](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20220409092550.png)



### Linux 查看.so中导出函数

**方法一**

```sh
nm -D **.so
```

但这样能看到所有的导出，乱七八糟的很多，筛选用：

```sh
nm **.so | grep XX
```

发现符号属性有两个分别是U和T：

- U代表是未定义符号
- T表示的是符号定义在Text段

 

**方法二**

```sh
objdump -tT **.so
```







### pkg-config查看库版本号

```shell
pkg-config --modversion libcrypto
```



### pkg-config打印模块所需的链接标志

```sh
pkg-config --libs libcrypto
```

![image-20210308174004402](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210308174004402.png)



### elf文件显示文件头信息

```sh
readelf -h xxx.so  或 xxx.a
```

![image-20210305113743355](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210305113743355.png)



### elf文件显示符号表段中的项

```sh
readelf -s xxx.so
```

![image-20210305114119625](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210305114119625.png)





### elf文件显示CPU构架信息

```sh
readelf -A  xxx.so 
```



![image-20210305114403221](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210305114403221.png)









### 参考链接

#### readelf命令使用说明

​	https://blog.csdn.net/yfldyxl/article/details/81566279

#### pkg-config命令使用说明

​	https://blog.csdn.net/miner_k/article/details/77854956



















### 查看最后一次段错误

dmesg命令

```sh
panfeng@ubuntu:~/segfault$ dmesg
... ...
[17257.502808] segfault3[3320]: segfault at 80484e0 ip 0018506a sp bfc1cd6c error 7 in libc-2.10.1.so[110000+13e000]
```





### [Linux查看库依赖方法](https://www.cnblogs.com/klausage/p/14186138.html)

1. 查看依赖的库：

   ```sh
   objdump -x xxx.so | grep NEEDED
   ```

   



2. 查看可执行程序依赖的库：

   ```shell
   objdump -x 可执行程序名 | grep NEEDED
   ```

   



3. 查看缺少的库：

   ```shell
   ldd xxx.so
   ```

   如果某个依赖的库不存在，会打印类似“xxx.so not found”的提示





### objdump使用方法

```sh
$objdump -f test #显示test的文件头信息
$objdump -d test #反汇编test中的需要执行指令的那些section
$objdump -D test #与-d类似，但是执行时间长，因为反汇编test中的所有section
$objdump -h test #显示test的Section Header信息
$objdump -x test #显示test的全部Header信息
$objdump -s test #除了显示test的全部Header信息，还显示他们对应的十六进制文件代码
$objdump -t test #输出目标文件的符号表
```



















### ldconfig命令

ldconfig是一个动态链接库管理命令。

为了让动态链接库为系统所共享,还需运行动态链接库的管理命令--ldconfig 

ldconfig  命令的用途,主要是在默认搜寻目录(/lib和/usr/lib)以及动态库配置文件/etc/ld.so.conf内所列的目录下,搜索出可共享的动态 链接库(格式如前介绍,lib*.so*),进而创建出动态装入程序(ld.so)所需的连接和缓存文件.缓存文件默认为  /etc/ld.so.cache,此文件保存已排好序的动态链接库名字列表.







##### 打印当前缓存文件的所有共享库

```sh
ldconfig  -p
```









































### mupdf 编译错误与解决方法

#### error: X11/extensions/XInput.h: No such file or directory

```sh
$sudo apt install libxi-dev
```





#### X11/extensions/Xrandr.h: 没有那个文件或目录

 ```sh
$sudo apt install  libxrandr-dev
 ```





#### fatal error: GL/glu.h: 没有那个文件或目录

```sh
$sudo apt install freeglut3-dev
```





### 系统相关



#### 信号列表

```sh
kill -l
```

![image-20210618141445971](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210618141445971.png)

信号的意义，参考链接：https://www.cnblogs.com/xiao0913/p/11846212.html





#### 创建daemon进程

Linux系统还专门提供了一个用来创建daemon进程的系统函数：

```c
int daemon(int nochdir, int noclose);
```

下面是用这个api创建daemon进程的简单示例：

```c
#include <unistd.h>
#include <stdlib.h>

int main(void)
{
　　if(daemon(0,0) == -1)
　　exit(EXIT_FAILURE);

　　while(1)
　　{
　　　　sleep(60);
　　}
　　return 0;
}
```

参考链接：https://www.cnblogs.com/minico/p/7702020.html







#### .service文件

![image-20210618164840295](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210618164840295.png)

.service文件定义了一个服务，分为[Unit]，[Service]，[Install]三个小节

[Unit]

Description:描述，

After：在network.target,auditd.service启动后才启动

ConditionPathExists: 执行条件

 

[Service]

EnvironmentFile:变量所在文件

ExecStart: 执行启动脚本

Restart: fail时重启

 

[Install]

Alias:服务别名

WangtedBy: 多用户模式下需要的



原文链接：https://blog.csdn.net/skh2015java/article/details/94012643









### 其他



#### 添加证书到第三方证书库

```sh
sudo apt install libnss3-tools
certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n 127.0.0.1 -i /tmp/cert-127.0.0.1
```





#### 切换Linux系统使用的JDK版本

```sh
update-alternatives --config java
```







## 安装/卸载程序



### C++ 开发环境

1.先安装 ：sudo apt-get install build-essential

2.查看 gcc 版本 然后安装 统一版本的 g++

```sh
gcc --version

gcc (Ubuntu/Linaro 4.4.4-14ubuntu5) 4.4.5
Copyright (C) 2010 Free Software Foundation, Inc.
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

3 安装 g++

```sh
sudo apt-get install g++-4.4
```

4查看安装版本

```sh
g++ --version
g++ (Ubuntu/Linaro 4.4.4-14ubuntu5) 4.4.5
Copyright (C) 2010 Free Software Foundation, Inc.
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```





### python安装

这种方式，直接通过python安装，与脚本安装类似，但是这个安装的是当前python版本所以依赖的pip，可能版本较低，因为内置python版本并不高。

```sh
yum upgrade python-setuptools
yum install python-pip
```



### gdb-peda的安装

peda一款实用的GDB插件
增强gdb的显示：在调试过程中着色并显示反汇编代码，寄存器和内存信息。
添加命令以支持调试和利用开发（有关命令使用的完整列表peda help）

#### 方法一：

```sh
$ pip install peda
```



#### 方法二：

```sh
git clone https://github.com/longld/peda.git ~/peda
echo "source ~/peda/peda.py" >> ~/.gdbinit
```



#### 基础命令

```sh
file 路径　-　附加文件
break *0x400100 (b main) - 在 0x400100 处下断点
tb  - 一次性断点
info b - 查看断点信息
enable   -   激活断点
disable  -   禁用断点
delete [number]  -  删除断点
watch *(int *)0x08044530  -  在内存0x0804453处的数据改变时stop
p $eax - 输出eax的内容
set $eax=4 - 修改变量值

c - 继续运行
r - 开始运行
ni - 单步步过
si - 单步步入
fini - 运行至函数刚结束处
return expression - 将函数返回值指定为expression
bt - 查看当前栈帧
info f - 查看当前栈帧
context - 查看运行上下文
stack - 查看当前堆栈
call func - 强制函数调用
stack 100 - 插件提供的，显示栈中100项
find xxx　 - 快速查找，很实用

x/<n/f/u> <addr>     n、f、u是可选的参数。
x /4xg $ebp：查看ebp开始的4个8字节内容
x/wx $esp 　　以4字节16进制显示栈中内容
b表示单字节，h表示双字节，w表示四字 节，g表示八字节
s 按字符串输出
x 按十六进制格式显示变量。
d 按十进制格式显示变量。
u 按十六进制格式显示无符号整型。
o 按八进制格式显示变量。
t 按二进制格式显示变量。
a 按十六进制格式显示变量。
c 按字符格式显示变量。
f 按浮点数格式显示变量。
i：反汇编

但是实际的组合就那么几种：
x/s 地址　　查看字符串
x/wx 地址　　查看DWORD
x/c 地址　　单字节查看
x/16x $esp+12 查看寄存器偏移

set args  - 可指定运行时参数。（如：set args 10 20 30 40 50）
show args  - 命令可以查看设置好的运行参数。

layout // 用于分割窗口，可以一边查看代码，一边测试。
主要有下面几种用法：
layout src // 显示源代码窗口
layout asm // 显示汇编窗口
layout regs // 显示源代码/汇编和寄存器窗口
layout split // 显示源代码和汇编窗口
layout next // 显示下一个layout
layout prev // 显示上一个layout

Ctrl + L // 刷新窗口
Ctrl + x  再按1 // 单窗口模式，显示一个窗口
Ctrl + x  再按2 // 双窗口模式，显示两个窗口
Ctrl + x  再按a // 回到传统模式，即退出layout，回到执行layout之前的调试窗口。
```



#### 参考链接：

https://www.jianshu.com/p/2f41a2fc1d21

https://www.cnblogs.com/furzoom/p/7710301.html





## 开发异常处理



#### QT5提示can not find -lGL的解决方法

##### 安装建立基本编译环境

​	首先不可或缺的，就是编译器与基本的函式库，如果系统没有安装的话，请依照下面的方式安装：

```sh
$ sudo apt-get install build-essential
```



##### 安装OpenGL Library

​	接下来要把我们会用到的 Library 装上去，首先安装 OpenGL Library

```sh
$sudo apt-get install libgl1-mesa-dev
```

到以上这一步为止就可以运行 qt5.0 的工程了，之后的命令只是为了方便以后学习OpenGL而安装。



##### 安装OpenGL Utilities

​	OpenGL Utilities 是一组建构于 OpenGL Library 之上的工具组，提供许多很方便的函式，使 OpenGL 更强大且更容易使用。接下来我们安装OpenGL Utilities

```sh
$sudo apt-get install libglu1-mesa-dev
```



##### 安装OpenGL Utility Toolkit

OpenGL Utility Toolkit 是建立在 OpenGL Utilities 上面的工具箱，除了强化了 OpenGL Utilities 的不足之外，也增加了 OpenGL 对于视窗介面支援。

```sh
$sudo apt-get install libglut-dev
```







### undefined reference to symbol 'dlclose@@GLIBC_2.2.5'

解决办法：加个 -ldl 即可。







### /lib/x86_64-linux-gnu/libm.so.6: version `GLIBC_2.27' not found 

###### 问题现象：

![image-20210626195728685](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20210626195804.png)

###### 解决方法：

使用命令 strings /lib/x86_64-linux-gnu/libm.so.6 | grep GLIBC_ 可以查看 /lib/x86_64-linux-gnu/libm.so.6 这个文件支持的glibc的版本。发现当前并不支持 GLIBC_2.27 

![image-20210626195947744](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20210626200029.png)

更换一个支持2.27版本的库文件。







### GL/glu.h: No such file or directory

安装libglu-dev

```sh
sudo apt install libglu-dev
```







### ldd 提示not found

对某个可执行文件使用ldd命令查看库的链接情况，某些库后面显示not found，总结网上看到的和自己遇到的，可能有以下几种原因：

1. 目标文件与依赖的库文件位数（32/64）冲突，

2. 共享库路径没有指定

第二种情况，一般执行文件的时候会报错：

```sh
./test: error while loading shared libraries: libxxx.so: cannot open shared object file: No such file or directory
```

其实是在默认库搜索路径下找不到需要的库，只需要执行：

```sh
export LD_LIBRARY_PATH=...
```

ldd就能找到对应的库了







### C函数返回指针位数错误，截断问题



#### 问题描述

在C工程中，一个64位系统中如果一个文件中的某个函数A调用另外一个文件中的函数B，但是A文件中没有包含B的声明，gcc可以编译通过，但是如果B函数的返回类型为指针，在64位系统应该返回64bit地址，实际上函数A调用B得到的B的返回指针却是32bit，高32bit被截断。

 

#### 代码示例

https://github.com/musiclvme/ToolsPackages/tree/master/function_def

![image-20210629172548829](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210629172548829.png)





test.c 里面调用了def.c里面的def_test，但是没有包含def_test.h的声明， 最后ret1得到返回指针为0x13095260，是32bit，高32bit被丢弃了。这样就会导致后面使用ret1这个指针时候肯定会出现问题。

#### 总结

gcc编译器对本函数调用未声明的函数，都为强制将其返回值类型转为int类型，int在64bit系统中占4个字节，这样指针类型的返回值就会出现截断现象！

这类问题需要留意编译当中的警示：warning: implicit declaration of function 'def_test' [-Wimplicit-function-declaration]
————————————————
版权声明：本文为CSDN博主「musiclvme」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/musiclvme/article/details/105230955





### Compile error: X11/extensions/Xrandr.h: No such file or directory

```sh
sudo apt install libxrandr-dev
```







### /usr/bin/ld: 找不到 -lxxx的解决办法

出现这种问题一般是没有找到libxxx.so的库，需要配置系统环境变量LIBRARY_PATH或者LD_LIBRARY_PATH

```sh
export LIBRARY_PATH=/usr/local/protobuf/lib
export LD_LIBRARY_PATH=/usr/local/protobuf/lib
```

或者拷贝libxxx.so文件到**/usr/local/lib**目录下







### error: X11/extensions/XInput.h: No such file or directory

```sh
$sudo apt install libxi-dev
```

