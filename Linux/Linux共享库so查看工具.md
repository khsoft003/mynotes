# objdump

objdump可用来反汇编so库，查看文件可能带有的附加信息。 详细使用时通过 –help 了解，常用参数有：

```sh
-d, --disassemble          Display assembler contents of executable sections
-D, --disassemble-all      Display assembler contents of all sections
-S, --source               Intermix source code with disassembly， 这个比较重要
-s, --full-contents        Display the full contents of all sections requested
```





# nm

nm用来列出目标文件的符号清单。详细使用时通过 –help 了解，常用参数有：

```sh
-a, --debug-syms       Display debugger-only symbols
-l, --line-numbers     Use debugging information to find a filename and line number for each symbol
```







# addr2line

addr2line 将指令的地址和可执行映像转换成文件名、函数名和源代码行数的工具。详细使用时通过 –help 了解。

```sh
-a --addresses         Show addresses
-b --target=<bfdname>  Set the binary file format
-e --exe=<executable>  Set the input file name (default is a.out）
-f --functions         Show function names
```

