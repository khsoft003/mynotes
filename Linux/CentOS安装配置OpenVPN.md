## OpenVPN官网

下载地址：https://openvpn.net/community-downloads/

官网：https://openvpn.net/

## OpenVPN的功能

OpenVPN是一种开源的虚拟私人网络（VPN）软件，它提供了一个安全的、加密的通信通道，可以用于连接远程计算机和网络。以下是OpenVPN提供的主要功能：

1. 安全的通信通道：OpenVPN提供了一个安全的、加密的通信通道，可以在互联网上连接远程计算机和网络。
2. 多种加密算法：OpenVPN支持多种加密算法，包括AES、Blowfish、3DES等，可以根据需要进行选择。
3. 灵活的配置选项：OpenVPN提供了灵活的配置选项，可以根据需要进行定制，包括IP地址、DNS服务器、路由等。
4. 支持多平台：OpenVPN可以在多种操作系统上运行，包括Windows、Mac、Linux等。
5. 客户端支持：OpenVPN提供了客户端软件，可以方便地在本地计算机上连接到远程网络。
6. 可扩展性：OpenVPN可以与其他软件集成，例如Radius服务器、LDAP、Active Directory等。
7. 支持多种协议：OpenVPN支持多种协议，包括TCP、UDP等，可以根据需要进行选择。

总之，OpenVPN是一款功能强大、灵活、安全可靠的VPN软件，被广泛应用于企业、教育、政府等领域。



## 安装配置OpenVPN服务器端



1. 安装OpenVPN：CentOS 7.8安装的是OpenVPN 2.4.12

```
sudo yum update
sudo yum install -y openvpn
```

2. 创建OpenVPN配置文件：

```
sudo vim /etc/openvpn/server.conf
```

在server.conf文件中，添加以下内容：

```
port 1194
proto udp
dev tun
ca /etc/openvpn/server.crt
cert /etc/openvpn/server.crt
key /etc/openvpn/server.key
dh /etc/openvpn/dh.pem
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 8.8.8.8"
push "dhcp-option DNS 8.8.4.4"
keepalive 10 120
tls-auth /etc/openvpn/ta.key 0
cipher AES-256-CBC
user nobody
group nobody
persist-key
persist-tun
status openvpn-status.log
verb 3
client-to-client
```

3. 生成证书和密钥：

```
cd /etc/openvpn
sudo openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/CN=server" -keyout server.key -out server.crt
sudo openssl dhparam -out dh.pem 2048
sudo openvpn --genkey --secret ta.key
```

4. 启动OpenVPN：

```
sudo systemctl start openvpn@server
```



## 给客户端生成证书

```bash
openssl genrsa -out cli.key 2048
openssl req -new -key cli.key -out cli.csr
openssl x509 -req -days 3650 -in cli.csr -CA server.crt -CAkey server.key -CAcreateserial -out cli.crt
```





## 安装配置OpenVPN客户端

#### Windows环境

1. 下载安装（2.4.12）：https://openvpn.net/community-downloads/

2. 将以下文件复制到客户端配置文件目录：C:\Users\Johnny\OpenVPN\config

- /etc/openvpn/server.crt
- /etc/openvpn/cli.key
- /etc/openvpn/cli.crt
- /etc/openvpn/ta.key

3. 在客户端配置文件目录中创建OpenVPN配置文件：config.ovpn

```
client
dev tun
proto udp
remote <server-ip> 1194
resolv-retry infinite
nobind
persist-key
persist-tun
ca server.crt
cert cli.crt
key cli.key
tls-auth ta.key 1
cipher AES-256-CBC
route-nopull
route 10.8.0.0 255.255.255.0 vpn_gateway
verb 3
```

4. 启动OpenVPN客户端。



#### Linux环境

1. 下载tar.gz包（2.4.12）：https://openvpn.net/community-downloads/

2. 编译安装OpenVPN客户端：

   ```bash
   wget https://swupdate.openvpn.org/community/releases/openvpn-2.4.12.tar.gz
   tar zxvf openvpn-2.4.12.tar.gz
   cd openvpn-2.4.12
   ./configure
   make
   make install
   ```

3. 创建OpenVPN的配置文件目录：/root/openvpn/conf

4. 拷贝以下文件到配置文件目录：

   - /root/openvpn/conf/server.crt
   - /root/openvpn/conf/cli.key
   - /root/openvpn/conf/cli.crt
   - /root/openvpn/conf/ta.key

5. 创建配置文件：config.ovpn

   ```
   client
   dev tun
   proto udp
   remote <server-ip> 1194
   resolv-retry infinite
   nobind
   persist-key
   persist-tun
   ca server.crt
   cert cli.crt
   key cli.key
   tls-auth ta.key 1
   cipher AES-256-CBC
   route-nopull
   route 10.8.0.0 255.255.255.0 vpn_gateway
   verb 3
   ```

   

6. 启动OpenVPN客户端

   ```bash
   openvpn --config /root/openvpn/conf/config.ovpn --daemon
   ```

   