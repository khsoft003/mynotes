



## 代码片段





##### 通过进程名结束进程

```shell
#!/bin/bash


#    查找   pid       
pid=`ps -ef | grep $1 | grep -v "grep" | awk '{print $2}'`

echo "pid=$pid"		# 打印一下查找到的 pid

if [ -z "$pid" ]	# -z 长度是否为0
then
	echo "not find sleep" 
	exit 0
fi

kill "$pid"			# 使用系统kill命令，结束对应pid的进程

echo "kill $1 --OK"		# 打印成功提示

```

