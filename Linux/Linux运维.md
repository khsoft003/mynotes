



## 系统目录说明



#### /etc/init.d

此目录下的脚本在系统启动时运行



#### /etc/profile.d

此目录下的脚本在用户登录时运行





## 常用系统操作



### 系统相关



#### 查看系统版本

```sh
lsb_release -a
uname -a
cat /etc/redhat-release
cat /proc/version
```



#### 查看系统位数（32/64）

```sh
uname -a
uname -m
arch
file /sbin/init
getconf LONG_BIT 
```



#### 查看系统内核版本

```sh
uname -r
```





#### 查看系统运行信息

查看系统CPU、内存、网络等动态信息

```shell
yum install nmon
nmon
```

![image-20220222092621704](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20220222092621704.png)







#### 查看所有的用户和组信息

- cat /etc/passwd
- cat /etc/group

参考链接：https://jingyan.baidu.com/article/a681b0de159b093b184346a7.html





#### 查看磁盘剩余空间

```sh
df -hl  		#查看磁盘剩余空间
du -sh [目录名]  #返回该目录的大小
du -sm [文件夹]  #返回该文件夹总M数
du -h [目录名]   #查看指定文件夹下的所有文件大小（包含子文件夹）
du -h --max-depth=1  #查看当前目录下所有文件夹大小（不包含子文件夹）

# 按byte查看
du -b  <dirname|filename>
# 按kb查看
du -k  <dirname|filename>
# 按mb查看
du -m  <dirname|filename>
```









#### 查看统计文件目录的个数

```sh
$ ls -l | grep "^-" | wc -l		#统计当前目录下文件的个数（不包括目录）
$ ls -lR| grep "^-" | wc -l		#统计当前目录下文件的个数（包括子目录）
$ ls -lR | grep "^d" | wc -l	#查看某目录下文件夹(目录)的个数（包括子目录）
```



#### 挂载光盘

下面的两个命令都可以

```sh
mount /dev/cdrom  /mnt/cdrom/
mount /dev/sr0    /mnt/cdrom/
```





#### 查看系统资源

1、vmstat [刷新延时 刷新次数]：监控系统资源，如vmstat 3 5，监听5此，每次间隔3秒。

2、dmesg：显示开机时内核检测信息。如dmesg|grep CPU。

3、free：查看内存使用状态。如free -m，以mb为单位，也可以是-b,-k,-g。

4、缓冲和缓存的区别：（没有被实际的进程使用，只被内核使用）
内存的运行速度要比硬盘快得多。缓存（cache）是用来加速数据从硬盘中“读取”，而缓冲（buffer）是用来加速数据“写入”硬盘的。

5、查看cpu信息：（因为/proc是内存里的，所以一旦断电里面的数据会小时，因此该数据是每次开机产生的。）
cat /proc/cpuinfo

6、uptime：显示系统的启动时间和平均负载，即top命令的第一行。w命令第一行也可以看到这个数据。

7、uname：查看系统和内核相关信息。

> -a：查看系统所有相关信息。
> -r：查看内核版本。
> -s：查看内核名称。

8、file /bin/ls ：判断当前系统的位数（注意要看操作系统的外部命令，如/bin/cp等内部命令就不行）

9、lsb_release -a：查看当前linux的发行版本。









#### 注意事项

- linux的配置文件大多数都是有严格的格式限制的，例如：
  - ​	注释#要从行首开始

​    行尾不要加注释，甚至空格









#### 安装字体

1、进入linux字体库

```sh
cd /usr/share/Fonts
```



2、新建windows文件夹

```sh
mkdir windows
```



3、将准备好的字体库导入到新建的windows文件夹

```sh
sudo cp *.ttc /usr/share/fonts/windows/  
sudo cp *.ttf /usr/share/fonts/windows/
```



4、更改字体库的权限（将当前windows字体库内文件权限也要进行修改，至少要有执行权限否则字体库不生效）

```sh
sudo chmod 755 /usr/share/fonts/windows/* 
```



5、然后进入linux字体库

```sh
cd /usr/share/fonts/windows/
```



6、接着根据当前目录下的字体建立scale文件

```sh
sudo mkfontscale
```



7、接着建立dir文件

```sh
sudo mkfontdir 
```



8、然后运行

```sh
sudo fc-cache -fv
```





### 网络相关

##### 配置ip地址

```sh
ifconfig eth0  192.168.1.123  #临时修改IP ，系统重启后失效
```



##### nmtui配置IP地址

```shell
nmtui
```





##### 重启网络服务

```sh
service network restart
```



#### Netcat 

一款简单的Unix工具，使用UDP和TCP协议。 它是一个可靠的容易被其他程序所启用的后台操作工具，同时它也被用作网络的测试工具或黑客工具。 使用它你可以轻易的建立任何连接。内建有很多实用的工具。

参考链接：https://www.runoob.com/linux/linux-comm-nc.html



##### TCP端口扫描

```shell
# nc -v -z -w2 192.168.0.3 1-100 
192.168.0.3: inverse host lookup failed: Unknown host
(UNKNOWN) [192.168.0.3] 80 (http) open
(UNKNOWN) [192.168.0.3] 23 (telnet) open
(UNKNOWN) [192.168.0.3] 22 (ssh) open
```







#### 防火墙配置firewall-cmd

###### 开放端口

```shell
firewall-cmd --add-port=8080/tcp --permanent  #开放8080端口
firewall-cmd --reload
```



###### 查看所有开放端口

```shell
firewall-cmd --list-all
```



###### 查看防火墙状态

```shell
firewall-cmd --state
```



参考地址：https://www.linuxcool.com/firewall-cmd







### 文件相关

#### 创建文件

```sh
touch file.txt
```



#### 查看证书

```sh
gcr-viewer /root/test.cer
```





#### 系统默认程序打开文件

xdg-open用法非常简单，就直接参数传入要打开的文件，等效于鼠标双击打开，系统会根据文件类型自动调用对应的程序

```sh
xdg-open  /root/test.pdf
xdg-open  /root/test.doc
xdg-open  /root/test.cer
xdg-open  http://www.baidu.com	#调用浏览器
xdg-open  .						#调用资源管理器
xdg-open  ~/Downloads			#调用资源管理器
```









### 目录相关



#### 递归创建

```sh
mkdir -p [目录名]
mkdir -p /tmp/Japan/boduo
```





#### 查看目录文件夹大小

```shell
du -sh [目录名]  #返回该目录的大小
du -sm [文件夹]  #返回该文件夹总M数
du -h [目录名]   #查看指定文件夹下的所有文件大小（包含子文件夹）
du -h --max-depth=1  #查看当前目录下所有文件夹大小（不包含子文件夹）

```



##### 安装ncdu

```shell
yum install ncdu
ncdu # 查看当前目录下各个文件夹大小
```





#### 拷贝目录

##### 	cp 命令

```sh
cp dir1/a.doc dir2 	#表示将dir1下的a.doc文件复制到dir2目录下
cp -r dir1 dir2 	#表示将dir1及其dir1下所包含的文件复制到dir2下
cp -r dir1/. dir2 	#表示将dir1下的文件复制到dir2,不包括dir1目录
#说明：cp参数 -i：询问，如果目标文件已经存在，则会询问是否覆盖；
```

##### 带属性拷贝

```sh
cp -rp [原文件或目录] [目标目录]
```





​		

#### 拷贝覆盖

​	拷贝aa目录下的所有文件及子目录下的文件到bb目录下，并强制覆盖。

```sh
cp -rf aa/*  bb
```





#### 移动文件夹

​	移动文件夹到指定目录下

```sh
mv aaa bbb/  #拷贝aaa文件夹到bbb目录下
```













### 进程相关

​	进程管理的内容：

- 判断服务器的健康状态
- 查看系统中的所有进程
- 杀死进程



#### 查看所有进程



##### top命令

```sh
top [选项] 
```

调整排序的列：在进程信息界面按**shift+f**，进入可以作为排序的列的列表页面。键入字母即可选择按哪列排序
改变排序方法：默认降序排列。如果需要改成升序，则在进程信息界面按R

**选项：**

　　-d 秒数： 指定top命令每隔几秒更新。默认值：3秒

**在top命令的交互模式可以执行的命令：**

　　?或h：　　显示交互模式帮助

　　P：　　以CPU使用率排序，默认。

　　M：　　以内存使用率排序

　　N：　　以PID排序

　　q：　　退出top

load average：1分钟，5分钟，15分钟　　系统的平均负载：小于1，负载较小；大于1，系统已经超出负荷

![image-20210515182357577](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210515182357577.png)



![image-20210515182421379](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210515182421379.png)



##### pstree命令

```
pstree -p　　　　　　查看进程树
```





##### ps命令

```
　ps aux 　　使用BSD操作系统格式
　ps -le 　　使用Linux操作系统格式
```

![image-20210515182152293](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210515182152293.png)

![image-20210515182208774](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210515182208774.png)







#### 终止进程



##### kill命令

```
kill -l 进程ID   :  查看可用的进程信号，重要信号：1 重启,9 强制杀死,15
```

![image-20210515182923175](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210515182923175.png)





##### killall命令

```
killall [选项] [信号]  　　　　#根据进程名杀死进程
```

- -i：　　交互式，询问是否杀死进程
- -I：　　忽略进程名的大小写



##### pkill命令

```
pkill [选项] [信号]  进程名　　　　#根据进程名杀死进程
```

-  -t　　终端号：　　按照终端号踢出用户，例如：pkill -t -9 pts/1

```
w            #使用w命令查询本机已经登录的用户
```





#### 列出进程打开的文件信息

##### lsof命令

```
lsof  [选项]
```

- -c 字符串：　　只列出以字符串开头的进程打开的文件
- -u 用户名：　　只列出某个用户的进程打开的文件
- -p pid：　　　　列出某个PID进程打开的文件

![image-20210515183324565](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210515183324565.png)





#### 后台不挂断地运行命令

```sh
nohup command &  #在后台运行command
```

nohup 英文全称 no hang up（不挂起），用于在系统后台不挂断地运行命令，退出终端不会影响程序的运行。



```sh
nohup unbuffer command > file.out 2>&1 &
```

上面的unbuffer命令需要额外安装expect，用来实时刷新。安装命令：apt install expect

nohup命令用来忽略所有挂断（SIGHUP）信号，让你的程序即使在用户注销后依然继续运行。

- command是任何一段你想要执行的shell命令。

- file.out 代表将command运行结果重定向到当前目录下的file.out文件中（如果要每次运行的结果追加到file.out后面，可以用>>而不是>）。
- 2 >&1表示将标准错误输出cerr的所有输出也都重定向到标准输出cout中，这样file.out中就会记录command命令运行过程中所有标准输出。



#### 查看运行的后台进程

```sh
jobs -l
```

![image-20211013110937941](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20211013111046.png)

jobs命令只看当前终端生效的，关闭终端后，在另一个终端jobs已经无法看到后台跑得程序了







### 服务相关



系统的第一个进程是`init` ，init 是一个守护进程，它将持续运行直至关机，其它所有的进程都是它的子进程。**init system** 规定了开机应该如何(何时)启动各种进程，当然其中就包括各种服务。

**主要的init system有以下几个：**

1.System V 是更老的初始化系统。

- Debian 6 and earlier
- Ubuntu 9.04 and earlier
- CentOS 5 and earlier

2.Upstart 是一个基于事件的传统的初始化系统的替代品。

- Ubuntu 9.10 to 14.10, including Ubuntu 14.04
- CentOS 6

3.systemd 是新的初始化系统，它已经被大多数最新的 Linux 发行版所采用。

- Debian 7 and 8
- Ubuntu 15.04 and newer
- CentOS 7









#### 常用命令

```sh
$ systemctl disable|enable SERVICE
$ systemctl start|stop|restart|status SERVICE
$ initctl start|stop|restart|status SERVICE
$ service SERVICE start|stop|restart|status
$ update-rc.d SERVICE enable|disable
```







**什么是 System V（SysV）**

SysV（意即 System V） 初始化系统是早期传统的初始化系统和系统管理器。由于 sysVinit 系统上一些长期悬而未决的问题，大多数最新的发行版都适用于 systemd 系统。

 

**什么是 Upstart 初始化系统**

Upstart 是一个基于事件的 /sbin/init 的替代品，它控制在启动时的任务和服务的开始，在关机时停止它们，并在系统运行时监控它们。

它最初是为 Ubuntu 发行版开发的，但其是以适合所有 Linux 发行版的开发为目标的，以替换过时的 System-V 初始化系统。

 

**什么是 systemd**

systemd 是一个新的初始化系统以及系统管理器，它已成为大多数 Linux 发行版中非常流行且广泛适应的新的标准初始化系统。systemctl 是一个 systemd 管理工具，它可以帮助我们管理 systemd 系统。



#### 查看所有正在运行的服务

```shell
service --status-all
service --status-all | more
service --status-all | less
```

```sh
systemctl 
systemctl list-units --type service
```

```shell
pstree   #输出来自 systemd 系统
```









#### 查看随系统启动的服务

```sh
# chkconfig --list
```



#### RPM服务的管理

##### RPM包安装的默认位置主要如下：

> /etc/init.d/ 启动脚本位置
> /etc/sysconfig/ 初始化环境配置文件位置
> /etc/ 配置文件位置
> /etc/xinetd.conf xinetd配置文件
> /etc/xinetd.d/ 基于xinetd服务的启动脚本
> /var/lib/ 服务产生的数据放在这里
> /var/log/ 日志



##### 独立服务的启动：

- 第一种：/etc/init.d/独立服务名 start|stop|restart|status
  如/etc/init.d/httpd restart
- 第二种：service 独立服务名 start|stop|restart|status
  如service httpd restart
  注：该方式是红帽子系列独有，其余版本不一定可用。
- 第三种（自启动）：chkconfig --level 2345 httpd on或者chkconfig httpd on(2345是默认级别，所以不用写也可以)
  注：该方式表示开启Apache的自启动，表示下次会自己启动。（一般2345是同开同关）on改成off表示关闭。
- 第四种（自启动，推荐）：在/etc/rc.d/rc.local里添加/etc/init.d/httpd restart即可。
- 第五种（自启动，红帽专用）：ntsysv命令，进入界面设置。它可以管理独立的服务，也可以管理基于xinetd的，但不能管理源码包安装的服务。







#### 常见服务

> acpid 电源管理接口，笔记本用户建议开启，可以监听内核层的相关电源事件。
> anacron 系统的定时任务程序。cron的一个子系统，如果定时任务错过了执行时间，可以通过anacron继续唤醒执行。
> alsasound alsa声卡驱动，如果使用alsa声卡，开启。
> apmd 电源管理模块。如果支持acpid，就不需要apmd。
> atd 制定系统在特定时间执行某个任务，只能执行一次。需要时开启，不过一般我们用crond来进行循环定时任务。
> auditd 审核子系统。如果开启了此服务，selinux的审核信息会写入/var/log/audit/audit.log文件；如果不开启，则记录在syslog中。
> autofs 让服务器可以自动挂载网络中的其他服务器的共享数据，一般用来自动挂载NFS服务，如果没有NFS服务建议关闭。
> avahi-daemon avahi是zeroconf协议的实现。它可以在没有dns服务的局域网发现基于zeroconf协议的设备和服务。除非有兼容设备或使用zeroconf协议，否则关闭。
> bluetooth 蓝牙支持设备。一般可以关闭。
> capi 仅对使用ISND设备的用户有用。
> chargen-dgram 使用UDP协议的chargen server，主要功能是提供类似远程打字的功能。
> chargen-stream 同上。
> cpuspeed 可以用来调整CPU频率，闲置时可以自动降低CPU频率来节省电量。
> crond 系统的定时任务，一般的linux服务器都需要定时任务帮助维护系统。建议开启。
> cvs 一个版本控制系统。
> daytime-dgram daytime使用tcp协议的daytime守护进程，该协议为客户机实现从远程服务器获取日期和实践的功能。
> daytime-stream 同上
> dovecot 邮件服务中POP3/IMAP服务的守护进程，主要用来接收信件，如果启动了邮件服务则开启，否则关闭。
> echo-dgram 服务器回显客户服务的进程。
> echo-stream 同上。
> firstboot 系统安装完成之后，有个欢迎界面，需要对系统进程初始设定，就是这个进程的作用。既然不是第一次启动了，就关闭吧。
> gpm 在字符终端(tty1-tty6)中可以使用鼠标复制和粘贴，就是这个服务的功能。
> haldaemon 检测和支持USB设备。如果是服务器可以关闭，个人机建议开启。
> hidd 蓝牙鼠标、键盘等蓝牙设备检测。必须启动bluetooth服务。
> hplip HP打印机支持，如果没有HP打印机可以关闭。
> httpd apache服务的守护进程，如果需要启动apache，就开启。
> ip6tables IPV6的防火墙，目前IPV6协议并没有使用，可以关闭。
> irda irda提供红外线设备间的通讯支持，可关闭。
> irqbalance 支持多核处理器，让cpu可以自动分配系统中断IRQ，提高系统性能。目前服务器多是多核CPU，请开启。
> isdn 使用isdn设备连接网络。目前主流的联网方式是光纤接入和adsl，isdn已经很少见了。
> kudzu 该服务可以在开机时进行硬件检测，并会调用相关的设置软件。建议关闭，仅在需要时开启。
> lvm2-monitor 该服务可以让系统支持lvm逻辑卷组，如果分区采用的是lvm方式，那么应该开启。
> mcstrans selinux的支持服务。
> mdmonitor 该服务用来检测software raid或lvm的信息。不是必须服务，可以关闭。
> mdmpd 该服务用来检测multi-path设备，不是必须服务。
> messagebus 这是linux的IPC（进程间通讯），用来在各个软件中交换信息，个人建议关闭。
> microcode_ctl intel系列的cpu可以通过这个服务支持额外的微指令集。
> mysqld mysql数据库服务器，需要就开启否则关闭。
> named dns服务的守护进程，用来进行域名解析。如果是dns服务器就开启，否则关闭。
> netfs 该服务用于在系统启动时自动挂在网络中的共享文件空间，比如nfs,samba等等，需要开启不需要关就闭。
> network 提供网络设置功能，通过这个服务来管理网络，所以开启。
> nfs NFS服务，linux与linux之间的文件共享服务。需要时开启，否则关闭。
> nfslock 在linux中如果使用了nfs服务，为了避免同一个文件被不同的用户同时编辑，所有有这个锁服务。有nfs时开启，否则关闭。
> ntpd 该服务可以通过互联网自动更新系统时间，使系统时间永远都准确。需要则开启，否则关闭。
> pcscd 智能卡检测服务，可以关闭。
> portmap 用在远程过程调用的服务，如果没有任何RPC服务时，可以关闭。主要是NFS和nis服务需要。
> psacct 该守护进程支持几个监控进程活动的工具。
> rdisc 客户端ICMP路由协议。
> readahead_early 在系统开机的时候，先将某些进程加载内存整理，可以加快一点启动速度。
> readahead_later 同上。
> restorecond 用于给selinux检测和重新加载正确的文件上下文。如果开启selinux则需要开启。
> rpcgssd 与NFS有关的客户端功能，没有nfs就关闭吧。
> rpcidmapd  同上。
> rsync 远程数据备份守护进程。
> sendmail sendmail邮件服务的守护进程。如果有邮件服务就开启，否则关闭。
> setroubleshoot 该服务用于将selinux相关信息记录在日志/var/log/messages中。建议开启。
> smartd 该服务用于自动检测硬盘状态，建议开启。
> smb 网络服务samba的守护进程。可以让linux和windows之间共享数据。如果需要则开启。
> squid 代理服务的守护进程。如果需要则开启。
> sshd ssh加密远程登录管理的服务。服务器的远程管理必须使用此服务，不要关闭。
> syslog 日志的守护进程。
> vsftpd vsftp服务的守护进程。如果需要ftp服务则开启，否则关闭。
> xfs 这个是x windos的字体守护进程。为图形界面提供字体服务，如果不启动图形界面，则关闭。
> xinetd 超级守护进程。如果有依赖于xinetd的服务就必须开启。
> ypbind 为NIS客户机激活ypbind服务的进程。
> yum-updatesd yum的在线升级服务。









### [工作管理](https://www.cnblogs.com/ziwuxian/p/12574959.html)

#### 1 把进程放入后台

- 放到后台运行

```
tar -zcf etc.tar.gc /etc  &　　　　
```

- 放到后台暂停

```
top   然后 ctrl+z　
```

#### 2 查看后台的工作

```
jobs -l
```

　　-l：显示工作的PID

![image-20210517101848016](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210517101848016.png)

 

 　　+ 号代表最近一个放入到后台的工作，也是恢复工作时默认恢复的工作

　　 - 号代表倒数第二个放入后台的工作

#### 3 恢复后台工作

```
fg %工作号　　　　　　　　#把工作放到前台运行
```

　　%工作号：　　%可以省略

```
bg  %工作号　　　　　　　　#把工作放到后台运行
```





### 安装卸载



#### yum命令

```sh
yum install 软件包名	#安装
yum update 软件包名		#升级软件包
yum update #升级所有软件包。谨慎操作，升级后可以系统无法启动
yum search 关键字  #查找软件包
yum remove 软件包名 #卸载软件包，及相关依赖包。谨慎操作
yum list #显示所有可以安装的软件包列表
```

##### 注意事项：

- 不要执行 yum update升级所有软件包
- 尽量不要执行yum remove卸载任何软件

```sh
yum grouplist 	#显示软件组列表
yum groupinstall 软件组名 #安装软件组
yum groupremove 软件组名 #卸载软件组
```







#### rpm命令

参考链接：http://math.ecnu.edu.cn/~jypan/Teaching/Linux/command/rpm.htm

https://blog.csdn.net/demonson/article/details/79379437



##### 安装一个包 

```sh
$rpm -ivh 包全名
```





##### 升级一个包 

```sh
$rpm -Uvh  包全名
```







##### 查看已经安装的RPM包

```sh
rpm -qa | grep [package]
```

```sh
rpm -q 软件名
rpm -qi 软件包名  #查看软件信息（软件已安装）
rpm -qip 软件包全名  #查看软件信息（软件未安装）
```



##### 查询一个已经安装的文件属于哪个软件包

```sh
# rpm -qf 文件名
rpm -qf /usr/lib/libacl.la
```



##### 查询已安装软件包都安装到何处

```sh
#rpm -ql 软件包名  
rpm -ql 软件包名 #查看软件包安装位置
rpm -qlp 软件包全名 #查看如果软件包安装的话，会安装到哪些位置
rpm -ql mplayer
```



##### 查看RPM包依赖性

```sh
rpm -qR 软件包名
rpm -qRp 软件包全名
```

模块依赖查询网站：http://www.rpmfind.net



##### RPM包校验

```sh
rpm -V 软件包名 #查看软件包安装之后是否做了改动
```

![image-20211117093036310](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/20211117093054.png)

验证内容中的8个信息的具体内容如下：

- S  文件大小是否改变
- M 文件的类型或文件的权限（rwx）是否被改变
- 5  文件MD5校验和是否改变（可以看成文件内容是否改变）
- D  设备的中，从代码是否改变
- L  文件路径是否改变
- U  文件的属主（所有者）是否改变
- G  文件的属组是否改变
- T  文件的修改时间是否改变

文件类型：

- c  配置文件（config file）
- d  普通文档（documentation）
- g  “鬼”文件（ghost file），很少见，就是该文件不应该被这个RPM包包含
- l  授权文件（license file）
- r  描述文件（read me）



##### 删除一个rpm包

```sh
#rpm -e 软件包名
rpm -e mplayer
```



##### 参考链接:兄弟连Linux学习笔记

  https://blog.csdn.net/qq_32809093/article/details/92379368









#### dpkg命令

##### 查看deb包中文件(不安装)

```sh
$ dpkg -c xxx.deb // 安装前根据deb文件查看
$ dpkg -L debname // 安装后根据包名查看
```

![image-20210702094315471](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210702094315471.png)



##### 安装deb包

```sh
$ dpkg -i xxx.deb
```

注意，如果提示错误，可以加参数—force-all强制安装，但不推荐这样做





##### 查看某个文件属于哪个deb包

```sh
$ dpkg -S filepath
```

![image-20210702094150293](https://bucket1018.oss-cn-beijing.aliyuncs.com/images/image-20210702094150293.png)



##### 查看是否安装了某个包

```sh
$ dpkg -s pageoffice
```

![image-20210702094224822](C:%5CUsers%5CJohnny%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20210702094224822.png)





##### 查看系统安装了哪些包

```sh
$ dpkg -l
```





##### 移除deb包

```sh
$ dpkg -r debname
```







#### snap常用命令

```shell
# 查看版本
snap version

# 查找软件
snap find "media player"

# 查看软件信息
snap info vlc

# 安装软件
snap install vlc                    # 默认使用stable channel
snap install vlc --channel=edge     # 指定 channel
snap switch  vlc --channel=stable   # 切换channel

# snap 安装软件目录在 `/snap/bin`，该目录已加入 `$PATH`，因此可以直接在命令行运行相应的程序。

# 列出所有已安装的软件
snap list
snap list --all vlc # 列出所有已安装的版本

# 更新已安装的软件
snap refresh vlc

# 回滚软件版本，snap默认会保留2各版本
snap revert vlc

# 启用/禁用软件
snap enable/disable vlc

# 删除软件
snap remove vlc         # 有些系统会缓存31天
snap remove vlc --purge # 彻底清除

# 后台服务列表
snap services

# 启动、停止、重启后台服务
snap restart lxd
snap restart lxd.daemon

snap start lxd.daemon
snap start --enable lxd.daemon

snap stop lxd.daemon
snap stop --disable lxd.daemon

# 查看后台服务日志
snap logs lxd
snap logs lxd -f # 持续监控
```

##### snap隔离级别

有三种隔离级别：

- Strict - 严格

  默认级别，大多数snap软件都是用该级别。该级别可以认为是安全的，因为它仅提供了最小的访问权限。在没有特殊授权的的情况下，软件不能访问文件、网路、进程等系统资源。

- Classic - 传统

  和传统安装的软件一样，可以访问系统资源。因此，需要手动授权，或在安装时带上`--classic`参数。

- Devmode - 开发模式

  为开发者提供的特殊模式。软件可以完全访问系统资源，并会输出调试信息。安装时需带上`--devmode`参数



#### apt命令

```sh
$sudo apt install xxx  #安装
$sudo apt purge xxx    #可以将包以及软件的配置文件全部删除
$sudo apt remove xxx   #仅可以删除包，但不会删除配置文件
$sudo apt list 				#显示所有软件包 
$sudo apt list --installed  #查看已安装软件
$sudo search xxx			#搜索软件包
$sudo apt update            #只检查，不更新（已安装的软件包是否有可用的更新，给出汇总报告）
$sudo apt upgrade xxx       #更新已安装的软件包
```





#### aptitude降级安装

- 安装aptitude，使用aptitude来进行降级。　　

```shell
sudo apt-get install aptitude
```

- 使用aptitude命令进行降级

```shell
aptitude install 包名=包的版本号 
# aptitude install libncurses5=5.7+20100313-51
```





### 压缩解压



#### .xz 文件的解压方法

```sh
xz -d filename.tar.xz
#得到filename.tar
tar -zxvf filename.tar 
#完成解压.
```





#### tar 命令

```sh
tar zxvf filename.tar.gz #解压gz文件.
tar xvf filename.tar #解压tar文件.
tar zxvf filename.tar.gc -C  /home/downloads # 解压到指定目录下
```















### awk



#### 显示指定列

```sh
root@petrusxu-ThinkPad-T430:/home/petrusxu/test# cat test.ini 
id name wwn desc
1 hi 1234 fdsf
2 nj 2345 fdss
3 lo 3456 kjlh
4 mk 4567 kjiu
```

```sh
root@petrusxu-ThinkPad-T430:/home/petrusxu/test# cat test.ini |awk '{ for(i=1; i<=NF; i++){if($i~/wwn/) haha=i };print $haha}' 
wwn
1234
2345
3456
4567
```







## 软件安装



### yum 扩展源

```sh
yum -y install epel-release
```







### 安装gcc

```sh
#安装gcc、c++编译器以及内核文件
yum -y install gcc gcc-c++ kernel-devel 
```





### vim安装NERDTree插件

1. 下载NERDTree

   ```sh
   git clone https://github.com/preservim/nerdtree
   ```

   

2.  创建~/.vim/目录

3. 拷贝nerdtree目录下的所有文件到 ~/.vim/目录下

   ```sh
   cp -r nerdtree/.  ~/.vim/
   ```





### 安装lazydocker

下载lazydocker发布包，解压即用。

```sh
wget https://github.com/jesseduffield/lazydocker/releases/download/v0.12/lazydocker_0.12_Linux_x86_64.tar.gz
tar zxvf lazydocker_0.12_Linux_x86_64.tar.gz
./lazydocker
```









### 安装pkg-config

Linux上，到pkg-config官网https://www.freedesktop.org/wiki/Software/pkg-config/，下载最新安装包
目前，最新版是2017年3月20日发布的0.29.2，下载地址https://pkg-config.freedesktop.org/releases/pkg-config-0.29.2.tar.gz。
命令wget https://pkg-config.freedesktop.org/releases/pkg-config-0.29.2.tar.gz，下载完成后解压

```sh
tar -zxvf pkg-config-0.29.2.tar.gz 
cd pkg-config-0.29.2/
./configure 
make
make check
sudo make install
pkg-config --version
0.29.2
```

#### 遇到"glib-2.0>=2.16"的错

*Either a previously installed pkg-config or "glib-2.0 >= 2.16" could not be found*

```sh
./configure --with-internal-glib
```





### 安装 DNF 包管理器 

参考链接：https://www.cnblogs.com/jpfss/p/6628736.html

**DNF**是新一代的[rpm](http://man.linuxde.net/rpm)软件包管理器。他首先出现在 Fedora 18 这个发行版中。而最近，它取代了[yum](http://man.linuxde.net/yum)，正式成为 Fedora 22 的包管理器。

DNF包管理器克服了YUM包管理器的一些瓶颈，提升了包括用户体验，内存占用，依赖分析，运行速度等多方面的内容。DNF使用 RPM, libsolv 和 hawkey 库进行包管理操作。尽管它没有预装在 CentOS 和 RHEL 7 中，但你可以在使用 YUM 的同时使用 DNF 。你可以在这里获得关于 DNF 的更多知识：[《 DNF 代替 YUM ，你所不知道的缘由》](http://www.tecmint.com/dnf-next-generation-package-management-utility-for-linux/)

DNF 的最新稳定发行版版本号是 1.0，发行日期是2015年5月11日。 这一版本的额 DNF 包管理器（包括在他之前的所有版本） 都大部分采用 Python 编写，发行许可为GPL v2.

DNF 并未默认安装在 RHEL 或 CentOS 7系统中，但是 Fedora 22 已经默认使用 DNF .

1、为了安装 DNF ，您必须先安装并启用 epel-release 依赖。

在系统中执行以下命令：

```
# yum install epel-release
```

或者

```
# yum install epel-release -y
```

2、使用 epel-release 依赖中的 YUM 命令来安装 DNF 包。在系统中执行以下命令：

```
# yum install dnf
```

然后， DNF 包管理器就被成功的安装到你的系统中了。接下来，是时候开始我们的教程了！在这个教程中，您将会学到27个用于 DNF 包管理器的命令。使用这些命令，你可以方便有效的管理您系统中的 RPM 软件包。现在，让我们开始学习 DNF 包管理器的27条常用命令吧！



**查看 DNF 包管理器版本**

用处：该命令用于查看安装在您系统中的 DNF 包管理器的版本

```
# dnf –version
```





**查看系统中可用的 DNF 软件库**

用处：该命令用于显示系统中可用的 DNF 软件库

```
# dnf repolist
```





**列出所有 RPM 包**

用处：该命令用于列出用户系统上的所有来自软件库的可用软件包和所有已经安装在系统上的软件包

```
# dnf list
```





**列出所有安装了的 RPM 包**

用处：该命令用于列出所有安装了的 RPM 包

```
# dnf list installed
```





**列出所有可供安装的 RPM 包**

用处：该命令用于列出来自所有可用软件库的可供安装的软件包

```
# dnf list available
```





**搜索软件库中的 RPM 包**

用处：当你不知道你想要安装的软件的准确名称时，你可以用该命令来搜索软件包。你需要在”search”参数后面键入软件的部分名称来搜索。（在本例中我们使用”[nano](http://man.linuxde.net/nano)”）

```
# dnf search nano
```





**查看软件包详情**

用处：当你想在安装某一个软件包之前查看它的详细信息时，这条命令可以帮到你。（在本例中，我们将查看”nano”这一软件包的详细信息）

```
# dnf info nano
```





**安装软件包**

用处：使用该命令，系统将会自动安装对应的软件及其所需的所有依赖（在本例中，我们将用该命令安装nano软件）

```
# dnf install nano
```



**升级软件包**

用处：该命令用于升级制定软件包（在本例中，我们将用命令升级”systemd”这一软件包）

```
# dnf update systemd
```





**删除软件包**

用处：删除系统中指定的软件包（在本例中我们将使用命令删除”nano”这一软件包）

```
# dnf remove nano 或 # dnf erase nano
```







### 安装ranger

参考链接：https://blog.csdn.net/qq_35515661/article/details/110055989

```sh
git clone https://github.com/hut/ranger.git
cd ranger
yum install python36 -y
ln -s /usr/bin/python3.6 /usr/bin/python3
curl -O https://bootstrap.pypa.io/get-pip.py
sudo /usr/bin/python3.6 get-pip.py
sudo make install
```

或，如果你有python3 > python3.5 直接：

```sh
pip3 install ranger-fm
```



##### 配置ranger

参考链接：https://blog.csdn.net/lxyoucan/article/details/115671189

编辑～/.config/ranger/rc.conf，添加如下配置

```sh
#显示边框线
set draw_borders both

# 只预览2MB以内的文件
set preview_max_size 2048000

```





##### 拷贝配置命令

1，选项设置

```sh
ranger --copy-config=options #获取~/.config/ranger/options.py
```

2，按键绑定

```sh
ranger --copy-config=rc #获取 ~/.config/ranger/rc.conf
```

3，文件关联

```sh
ranger --copy-config=apps #获取 ~/.config/ranger/apps.py
```

4，预览设置

```sh
ranger --copy-config=scope #获取 ~/.config/ranger/scope.sh
```









##### 快捷键

参考链接：https://blog.51cto.com/martint/1564139

```sh
yy      复制
dd      剪切
pp      粘贴
delete  删除
cw      重命名
zh      显示隐藏文件
空格    单选
v       反选
V       进入/退出多选模式

#任务管理
w: 打开/关闭任务视图
dd: 终止一个任务
J: 降低当前任务的优先级
K: 提升当前任务的优先级

S: 把父目录作为当前目录，返回到shell
[: 父目录向上
]: 父目录向下
```





### 安装tmux

```sh
yum install tmux
```

​	tmux 的概念：

	- Session
	- Window
	- Pane

  常用Window和Pane





#### 查看版本号

```sh
tmux -V
```



#### 窗口操作

```sh
ctrl+b+c	#新建窗口
ctrl+b+&	#关闭窗口
ctrl+b+l	#切换窗口
ctrl+b+w	#窗口列表

ctrl+b+p	#上一个窗口
ctrl+b+n	#下一个窗口
```



#### 窗格操作

```sh
ctrl+b+%	#水平分屏
ctrl+b+"	#垂直分屏

ctrl+b+x	#关闭窗格
ctrl+b+;	#切换窗格

ctrl+b+ctrl+o   #旋转窗格位置
```





#### 命令模式

```sh
ctrl+b+:	#进入命令模式
```







#### 配置鼠标操作

编辑~/.tmux.conf，添加如下配置

```sh
set -g  mode-mouse on
set -g  mouse-resize-pane on
set -g  mouse-select-pane on
set -g  mouse-select-window on
```





### 安装img2txt

```sh
pip3 install img2txt.py
yum install caca-utils
```



##### 图片转txt

```sh
img2txt -W 50 ./logo.png
```





### 安装PHP7

在centos7通过yum安装PHP7，首先在终端运行：

```sh
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
```

提示错误：
error: Failed dependencies:
epel-release >= 7 is needed by webtatic-release-7-3.noarch
需要先安装epel-release。



##### 1.安装epel-release

通过命令：
yum -y install epel-release
成功安装。



##### 2.安装PHP7

终端再次运行如下命令：

```sh
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
```

成功获取PHP7的yum源，然后再执行：

```sh
yum install php70w
```

这样就大功告成了。



##### 3.验证安装

显示当前PHP版本

```sh
php -v
```





### 安装Shadowsocks客户端

> 系统环境：CentOS7

#### 安装 pip
Pip 是 Python 的包管理工具，这里我们用 pip 安装 shadowsocks。
```shell
yum install python-pip
pip install shadowsocks
```

#### 配置 shadowsocks
新建配置文件：
```shell
vi /etc/shadowsocks.json
```
填写以下内容:
```json
{
    "server":"your_server_ip",      #ss服务器IP
    "server_port":your_server_port, #端口
    "local_address": "127.0.0.1",   #本地ip
    "local_port":1080,              #本地端口
    "password":"your_server_passwd",#连接ss密码
    "timeout":300,                  #等待超时
    "method":"rc4-md5",             #加密方式
    "fast_open": false,             # true 或 false。如果你的服务器 Linux 内核在3.7+，可以开启 fast_open 以降低延迟。开启方法： echo 3 > /proc/sys/net/ipv4/tcp_fastopen 开启之后，将 fast_open 的配置设置为 true 即可
    "workers": 1                    # 工作线程数
}
```

Demo参考：
```json
{
    "server":"23.105.222.129",
    "server_port":5800,
    "local_address": "127.0.0.1",
    "local_port":1080,
    "password":"*******",
    "timeout":300,
    "method":"aes-256-cfb",
    "fast_open": false,
    "workers": 1
}
```

#### 启动shadowsocks服务
```shell
sslocal -c /etc/shadowsocks.json
```
或
```shell
sudo sslocal -c /etc/shadowsocks.json -d start
```

#### 测试
运行
```shell
curl --socks5 127.0.0.1:1080 http://httpbin.org/ip
```
如果返回你的 ss 服务器 ip 则测试成功：
```json
{
  "origin": "23.105.222.129"
}
```

#### 安装 Privoxy
Shadowsocks 是一个 socket5 服务，因此我们需要使用 Privoxy 把流量转到 http/https 上。
直接使用yum安装即可：
> yum install privoxy

安装好后，修改一下配置：
> vim /etc/privoxy/config

搜索forward-socks5t将
> forward-socks5t / 127.0.0.1:9050 .

取消注释并修改为：
> forward-socks5t / 127.0.0.1:1080 .

#### 启动 privoxy
> privoxy /etc/privoxy/config


#### 配置/etc/profile
执行vim /etc/profile,添加如下代码：

```shell
export http_proxy=http://127.0.0.1:8118
export https_proxy=http://127.0.0.1:8118
```

修改后使配置生效：
```shell
source /etc/profile
```

测试生效：
```shell
curl www.google.com
```

返回一大堆 HTML 则说明 shadowsocks 正常工作了。
备注：如果不需要用代理了，把 /etc/profile 里的配置注释即可。





### 安装tldr

为了舍弃man手册，于是程序员大神们搞了很多个工具，比如TLDR、Bro、Cheat ，这些都是很简洁的命令手册。

TLDR区别于man手册的区别：

- 简洁，清晰的排版+言简意赅的说明
- 只收集高频使用的实际例子
- 缺点是没有man手册全面，但目前一直在更新



TLDR 安装也很简单，支持 `node` 和 `python` 安装

Node:

```shell
npm install -g tldr
```

Python:

```shell
pip3 install tldr
```







## nginx

### nginx常用命令

###### 查看版本号

```shell
nginx -v
```



###### 启动

```shell
nginx
```



###### 停止

```shell
nginx -s stop
```



###### 重新加载

```shell
nginx -s reload
```





### nginx配置文件结构

文件名：nginx.conf，一般存储位置：/etc/nginx/nginx.conf



#### 全局块

配置服务器整体运行的配置指令。
比如处理并发数的配置。

> worker processes 1;



#### events块

影响nginx服务器与用户的网络连接。

比如支持最大连接数的配置

> worker_connections  1024;





#### http块

http配置块包括http-全局块、http-server块、upstream 块儿。可以嵌套多个server，配置代理，缓存，日志定义等绝大多数功能和第三方模块的配置。



##### http全局块

mime-type定义，日志自定义，是否使用sendfile传输文件，连接超时时间，单连接请求数等

```nginx
http {
      #设定mime类型,类型由mime.type文件定义
      include    mime.types;
      #默认文件类型
      default_type application/octet-stream; 

      #设定日志格式(自定义)
      log_format ownformat '$remote_addr - $remote_user [$time_local] "$request" '
                   '$status $body_bytes_sent "$http_referer" '
                   '"$http_user_agent" "$http_x_forwarded_for"';
      access_log  /home/nginx/log/http_access.log ownformat;
                   
      #对于普通应用，设为on可以减少文件拷贝次数，如果图片显示不正常把这个改成off。
      sendfile     on;	      #off模式拷贝路径：硬盘—>内核fs缓冲区—>用户空间—>内核socket缓冲区—>协议引擎
			      # on模式拷贝路径1： 硬盘—>内核fs缓冲区—>内核socket缓冲区—>协议引擎
			      # on模式拷贝路径2： 硬盘—>内核fs缓冲区—>协议引擎
						
      tcp_nopush on	      #在linux/Unix系统中优化tcp数据传输，仅在sendfile开启时有效
      #tcp_nodelay on;	      #TCP_NODELAY选项来禁用Nagle的缓冲算法，并在数据可用时立即发送与tcp_nopush互斥

	
      keepalive_timeout 120; 		#长连接超时时间，单位是秒
      send_timeout 30;			#指定客户端的响应超时时间
	
	
      gzip on;			      #开启gzip压缩输出
      gzip_http_version 1.0; 	      #压缩版本
      gzip_comp_level 2;	      #压缩级别，1-10，数字越大压缩的越好
      gzip_min_length 1k;	      #设置允许压缩的页面最小字节数
      gzip_types text/plain application/x-javascript text/css application/xml;  #压缩类型
      gzip_disable "MSIE [1-6].";      #IE6及以下禁止压缩
}

#日志格式(自定义)
        #$remote_addr与$http_x_forwarded_for用以记录客户端的ip地址；
        #$remote_user：用来记录客户端用户名称；
        #$time_local： 用来记录访问时间与时区；
        #$request： 用来记录请求的url与http协议；
        #$status： 用来记录请求状态；成功是200，
        #$body_bytes_sent ：记录发送给客户端文件主体内容大小；
        #$http_referer：用来记录从那个页面链接访问过来的；
        #$http_user_agent：记录客户浏览器的相关信息；
 #通常web服务器放在反向代理的后面，这样就不能获取到客户的IP地址了，通过$remote_add拿到的IP地址是反向代理服务器的iP地址。反向代理服务器在转发请求的http头信息中，可以增加x_forwarded_for信息，用以记录原有客户端的IP地址和原来客户端的请求的服务器地址。
```







##### server块

server块包含server全局块、location块。

###### server全局块

```nginx
server {
    listen 80;			#描述虚拟主机接受连接的地址和端口
    server_name localhost;          #内网：主机名，外网：域名 可以是多个域名，用空格隔开
    access_log   /home/nginx/log/server_localhost_access.log ownformat;
    index index.html index.htm index.jsp;         #默认访问主页
	root /nginx/www/webapps; 	              #主目录
}
	

server {
    listen      192.168.1.101:8080;		# 监听具体IP和具体端口上的连接
    ...
}

server {
    listen      80;						
    server_name example.com www.example.com;	#基于域名的虚拟主机（server_name）
}

server {
    listen      192.168.1.1:80;
    server_name example.net www.example.net;	#基于域名和IP混合的虚拟主机
}

#nginx支持三种类型的虚拟主机配置
#- 基于ip的虚拟主机（一块物理服务器绑定多个ip地址）
#- 基于域名的虚拟主机（server_name）
#- 基于端口的虚拟主机（listen不写ip，端口模式）
```



###### location块

为具体的请求URI配置请求路由，可以实现默认主页、错误提示页面、静态文件处理、反向代理等功能

> location的优先级与location配置的位置无关。

```nginx
location = / {			#注意URL最好为具体路径。 uri严格匹配指定的路径，不包括子路径
    [ configuration A ]
}

location / {			#对当前路径及子路径下的所有对象都生效；
    [ configuration B ]
}

location ~ URI {} {		#~区分字符大小写
    [ configuration C ]
}

location ^~ /images/ {	#禁用正则表达式
    [ configuration D ]
}

location ~* \.(gif|jpg|jpeg)$ {	#~*不区分字符大小写
    [ configuration E ]
}

#默认请求
location / {
    #定义首页索引文件的名称
    index index.php index.html index.htm;   
}

location ~ /crm/ {
    proxy_pass http://127.0.0.1:8081;
}

location ~ /doc/ {
    proxy_pass http://127.0.0.1:8082;
}

# 定义错误提示页面
error_page   500 502 503 504 /50x.html;
  location = /50x.html {
}
error_page   400 402 403 404 /40x.html;
  location = /40x.html {
}

#静态文件，nginx自己处理
location ~ ^/(images|javascript|js|css|flash|media|static)/ {
    #过期30天，静态文件不怎么更新，过期可以设大一点，
    expires 30d;
}

#所有jsp的页面均交由tomcat或weblogic处理
location ~ .(jsp|jspx|do)?$ {
	#指向weblogic服务器
 	proxy_pass http://127.0.0.1:700;
    #将主机跟真实IP写入HTTP头中
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}

```



##### upstream 块

```nginx
#upstream可以实现负载均衡
upstream httpds {
    server 127.0.0.1:8050       weight=10 down;
    server 127.0.0.1:8060       weight=1;
    server 127.0.0.1:8060       weight=1 backup;
}
#- down：表示当前的server暂时不参与负载 
#- weight：默认为1.weight越大，负载的权重就越大。 
#- backup： 其它所有的非backup机器down或者忙的时候，请求backup机器。

#负载均衡算法：
#1、轮询（默认）
#每个请求按时间顺序逐一分配到不同的后端服务器，如果后端服务器down掉，能自动剔除。

#2、weight
#指定轮询几率，weight和访问比率成正比，用于后端服务器性能不均的情况。

#3、ip_hash
#每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。

#4、fair（第三方）
#按后端服务器的响应时间来分配请求，响应时间短的优先分配。

#5、url_hash（第三方）
#按访问url的hash结果来分配请求，使每个url定向到同一个后端服务器，后端服务器为缓存时比较有效。

```



参考链接：https://www.cnblogs.com/elfcafe/p/13196881.html





### nginx常用配置

##### 反向代理一：

简单端口映射

```nginx
server {
    listen		80;
    server_name	192.168.17.129;
    location / {
        root	html;
        proxy_pass  http://127.0.0.1:8080;
        index	index.html index.htm;
    }
}
```

![image-20220303114941810](C:%5CUsers%5CJohnny%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20220303114941810.png)



##### 反向代理二：

路径不同，跳转到不同端口的服务中。

- nginx监听端口9001；
- 访问 http://192.168.17.129:9001/edu/ 直接跳转到 127.0.0.1:8080
- 访问 http://192.168.17.129:9001/vod/ 直接跳转到 127.0.0.1:8081

```nginx
server {
    listen	9001;
    server_name 192.168.17.129;
    location ~ /edu/ {
    	proxy_pass http://127.0.0.1:8080;
    }
    location ~ /vod/ {
    	proxy_pass http://127.0.0.1:8081;
    }
}

```





##### 负载均衡

```nginx
upstream myserver{
    server 	192.168.17.129:8080;
    server 	192.168.17.129:8081;
}

server {
    listen	80;
    server_name	192.168.17.129;
    location / {
        proxy_pass	http://myserver;
    }
}
```



**负载均衡分配策略：**

第一种轮询（默认)。
	每个请求按时间顺序逐一分配到不同的后端服务器,如果后端服务器down掉,能自动剔除。

第二种weight
	weight代表权重默认为1,权重越高被分配的客户端越多

    ```nginx
upstream httpds {
    server 127.0.0.1:8050       weight=10;
    server 127.0.0.1:8060       weight=1;
}
    ```



第三种ip_hash
	每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器

```nginx
upstream backend {
   server 127.0.0.1:8080 ;
   server 127.0.0.1:9090 ;
   ip_hash;
}
```



第四种fair（第三方)。
	按后端服务器的响应时间来分配请求，响应时间短的优先分配。



##### 动静分离

​    不能理解成只是单纯的把动态页面和静态页面物理分离，严格意义上说应该是动态请求跟静态请求分开，可以理解成使用Nginx，处理静态页面，Tomcat 处理动态页面。动静分离从目前实现角度来讲大致分为两种。

​    一种是纯粹把静态文件独立成单独的域名，放在独立的服务器上，也是目前主流推崇的方案；

​    另外一种方法就是动态跟静态文件混合在一起发布， 通过nginx 来分开。

```nginx
location /www/ {
    root	/data/;
    index	index.html;
}
location /image/ {
    root	/data/;
    autoindex	on; 
}
```



![image-20220303131730045](C:%5CUsers%5CJohnny%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20220303131730045.png)

##### 高可用

待续......













## 其他



#### 简单文件共享系统

使用一条命令就可以把当前文件夹下的内容通过网页的形式共享

##### Python2

```sh
python -m  SimpleHTTPServer 80
```



##### Python3

```sh
python -m http.server 80
```

